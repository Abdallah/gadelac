open Printf
open Gadelac

module CLI = CommandLine
module T = Transformations

let preprocess options = T.to_optimized options !(options.CLI.input_name)

let compile options =
  let outname = if !(options.CLI.output_name) = "" then "stdout" else !(options.CLI.output_name) in
  eprintf "compiling %s to %s\n%!" !(options.CLI.input_name) outname;
  T.to_instructed options !(options.CLI.input_name)

let count_roles options =
  let program = preprocess options in
  let is_role = function
    | (Predicate.Role, _), [] -> true
    | (Predicate.Role, _), _ :: _ -> assert false
    | _, _ -> false in
  let roles = List.length (List.filter is_role program) in
  sprintf "%d" roles

let main options =
  let { CLI.verbose; backend; output_name; _ } = options in
  if !verbose >= 2 then eprintf "%s\n" (CLI.print options);
  let process () =
    if !(options.CLI.count_roles)
    then count_roles options
    else
      match !backend with
      | CLI.ASP -> ASP.program (preprocess options)
      | CLI.C -> C.program () (compile options)
      | CLI.OCaml -> OCaml.program () (compile options)
      | CLI.CLIPS -> CLIPS.program (preprocess options)
      | CLI.FOL -> FirstOrder.Print.program (ClarksCompletion.program (preprocess options))
      | CLI.GDL -> Optimization.Print.program !(options.CLI.hsfc) (preprocess options)
      | CLI.Prolog -> Prolog.program (preprocess options)
      | CLI.Table -> Desugared.Print.program (Inlining.FullGround.program (preprocess options))
      | CLI.UAMBAT -> ActionTranslation.program (preprocess options) in
  let (result, time) = Utils.Time.measure process () in
  if !output_name = "" then printf "%s\n" result
  else Utils.Print.to_file !output_name result;
  if !verbose >= 1 then eprintf "time to compile: %.3f sec\n%!" time

let _ =
  let options = CLI.get () in
  try main options
  with | Invalid_argument str -> eprintf "Bad input: %s\n%!" str; exit 1
       | exn -> eprintf "%s\n%!" (Printexc.to_string exn); exit 2
