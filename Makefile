all: gadelac

.PHONY: clean install uninstall test gadelac

clean:
	rm -f *~ -r __pycache__
	dune clean

gadelac:
	dune build bin/Gadelac.exe


install:
	dune build @install
	dune install

#test:
#	dune runtest

uninstall:
	dune uninstall

compile:
	./compile_all.py
test:
	./compile_test.sh
