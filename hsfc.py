#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import os
import resource
import subprocess
import sys
import functools
import multiprocessing.dummy as mpd

hsfc_games = ["amazons", "asteroidsserial", "beatmania", "blocker", "breakthrough", "bunk_t", "chomp", "connect4", "doubletictactoe", "hanoi", "lightsout", "minichess", "nim1", "pancakes6", "peg_bugfixed", "roshambo2", "sheep_and_wolf", "tictactoe",  "tictactoex9"]

test_games = ["busy-beaver-3-2", "busy-beaver-3-2", "busy-beaver-4-2", "busy-beaver-5-2"]
test_input = "Games/Tests"

buggy_gdl = [ # Bugs in the GDL, not in the compiler
    "Nine_Mens_Morris_0.11_2p.gdl", # (<= (goal ?player 0) (jailed player)) -> typo making it not safe!
    "pointgrab_state.gdl", # (legal ?r grab1) is not safe!
    "themathematician_medium.gdl", # (add 0 ?n ?n) is not safe!
    "themathematician_easy.gdl", # (add 0 ?n ?n) is not safe!
    "tritactoe.gdl", # (<= (goal ?player1 0) (tritt ?player2) (distinct ?player1 ?player2)) is not safe!
    "3qbf-5cnf-20var-40cl.1.qdimacs.SAT.gdl", # uses "not" as a symbol
    "3qbf-5cnf-20var-40cl.1.qdimacs.viz.SAT.gdl", # uses "not" as a symbol
    "sat_test_20v_91c_visualisation.gdl", # uses "not" as a symbol
    "satlike_20v_91c.gdl", # uses "not" as a symbol
    "uf20-01.cnf.SAT.gdl", # uses "not" as a symbol
    ]
forbidden = buggy_gdl + [
    "laikLee_hex.gdl", # really takes too much time. Perhaps (probably) because it does not satisfy the GRR.
    "kono.gdl" # loop, why?
    ]

forbidden_opti = [
    "checkers-mustjump-torus.gdl", # loop?
    "wargame02.gdl", # high memory cost
    "golden_rectangle.gdl", #high memory cost
    ]

def compile_directory(in_dir, out_dir, opti):
    #print(forbidden)
    correct_games = [game for game in os.listdir(in_dir) if game not in forbidden]
    opti_games = [game for game in correct_games if game not in forbidden_opti]
    games = correct_games if not opti else opti_games
    #print(games)
    pool = mpd.Pool(1) # two concurrent commands at a time
    pool.map(functools.partial(compile_game, in_dir, out_dir, opti), games)

def compile_game(in_dir, out_dir, opti, game):
    sys.stderr.write("Starting " + in_dir + "/" + game + "\n")
    sys.stderr.flush()
    input_file = in_dir + "/" + game
    options = ["--backend", "gdl"] + ["-o", out_dir + "/" + game] + [input_file]
    no_optimizations = ["--reachability", "false", "--inlining", "-1", "--compute_rigids", "false", "--merge_rigids", "false", "--ground_all", "false"]
    do_optimizations = ["--reachability", "false", "--inlining", "-1", "--compute_rigids",  "true", "--merge_rigids",  "true", "--ground_all", "false"]
    old_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    if opti: subprocess.check_output(["./gadelac"] + options + do_optimizations)
    else: subprocess.check_output(["./gadelac"] + options + no_optimizations)
    g_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    old_time = old_info.ru_utime + old_info.ru_stime
    pddl_time = g_info.ru_utime + g_info.ru_stime - old_time
    sys.stderr.write(in_dir + "/" + game + " time: {0:.3f}\n".format(pddl_time))
    sys.stderr.flush()

if __name__ == "__main__":
    games = [("Games/dresden", game + ".gdl") for game in hsfc_games]
#    games = [("Games/twoplayers", "tictactoe")]
    for (directory, game) in games:
        sys.stderr.write(game + "    ")
        compile_game(directory, "HSFC_domains_opt", True, game)
    # compile_directory("Games/dresden", "HSFC_domains/dresden", False)
    # compile_directory("Games/base", "HSFC_domains/base", False)
    # compile_directory("Games/stanford", "HSFC_domains/stanford", False)
#    compile_directory("Games/dresden", "HSFC_domains_opt/dresden", True)
#    compile_directory("Games/base", "HSFC_domains_opt/base", True)
#    compile_directory("Games/stanford", "HSFC_domains_opt/stanford", True)
