module Naive (D : Domain.S) :
sig
  val substitutions : D.t -> 'a Desugared.T.clause -> D.substitutions
  val forward : bool -> 'a Desugared.T.program -> D.t
  val backward : bool -> 'a Desugared.T.program -> D.t
end

val add_frames : 'a Desugared.T.program -> Ast.T.free Desugared.T.program

type param = Trivial | Graph | Paths | Precise
val print : param -> string

(** Performs a reachability analysis to prune useless rules and rules that cannot fire. *)
val make : param -> 'a Desugared.T.program -> Ast.T.free Desugared.T.program

