open Printf

type param = T0 | T1 | T2

let deprintf format = if false then eprintf format else ifprintf stderr format

let print_param = function
  | T0 -> "t0"
  | T1 -> "t1"
  | T2 -> "t2"

let read_param s = match String.lowercase_ascii s with
  | "t0" -> T0
  | "t1" -> T1
  | "t2" -> T2
  | _ -> failwith (sprintf "unknown frequency parameter \"%s\"" s)

type t = Permanent | Persistent | Ephemeral

let print = function
  | Permanent -> "perm"
  | Persistent -> "pers"
  | Ephemeral -> "ephe"

(*let update_permanent old lits = 
  let max_freq x y = match x, y with
    | Ephemeral, _ | _, Ephemeral -> Ephemeral
    | Persistent, _ | _, Persistent -> Persistent
    | Permanent, Permanent -> Permanent in
  let cmp x y = match x, y with
    | None, _ | _, None -> None
    | Some x, Some y -> Some (max_freq x y) in
  match old, List.fold_left cmp (Some Permanent) (List.map snd lits) with
  | None, x -> x
  | Some x, None -> Some x
  | Some x, Some y -> Some (max_freq x y)*)
let update_permanent old poss negs = 
  let max_freq x y = match x, y with
    | Ephemeral, _ | _, Ephemeral -> Ephemeral
    | Persistent, _ | _, Persistent -> Persistent
    | Permanent, Permanent -> Permanent in
  let cmp x y = match x, y with
    | None, _ | _, None -> None
    | Some x, Some y -> Some (max_freq x y) in
  match old, List.fold_left cmp (Some Permanent) (negs @ poss) with
  | None, x -> x
  | Some x, None -> Some x
  | Some x, Some y -> Some (max_freq x y)

let one_persistent = function
  | _, [] -> None
  | (Predicate.Next, [Ast.T.Fact atom]), Desugared.T.Okay (Predicate.True, [Ast.T.Fact atom2]) :: [] when atom = atom2 ->
(*    assert false;  (* all variables should be distinct *)*)
    if Desugared.only_free_arguments atom then Some (Predicate.Constant (fst atom)) else None
  | _, _ :: _ -> None
let is_persistent rules = List.filter_map one_persistent rules
let rec one_ephemeral = function
  | ((Predicate.Next, _), _) -> Some Predicate.Next
  | (_, []) -> None
  | ((pred, _), Desugared.T.Negation _ :: _) -> Some pred
  | (head, _ :: rest) -> one_ephemeral (head, rest)
let is_ephemeral rules = List.filter_map one_ephemeral rules

let frequency_gen param rules =
  let persists = is_persistent rules in
  let ephemeral = is_ephemeral rules in
  let starting_freq fct = match param with
    | T0 -> Some Ephemeral
    | T1 -> if List.mem fct ephemeral then Some Ephemeral else None
    | T2 ->
      if List.mem fct ephemeral then Some Ephemeral
      else if List.mem fct persists then Some Persistent else None in
  let start = Analysis.start_map rules starting_freq in
  let final = Analysis.fixpoint rules start update_permanent in
  let simplify value = Some (Utils.Option.default (fun x -> x) Ephemeral value) in
  let simplified = Predicate.Map.map simplify final in
  let final2 = Analysis.fixpoint rules simplified update_permanent in
  let simplify2 value = Utils.Option.default (fun x -> x) Ephemeral value in
  Predicate.Map.map simplify2 final2

let frequency param prog = frequency_gen param prog

let get_permanents prog =
  let dep = Analysis.dep_graph prog in
  let dep = Predicate.Map.mapi (fun key set -> Predicate.Set.remove key set) dep in
  let preds = Predicate.Set.of_list (Predicate.Map.keys dep) in
  let is_perm non_perms pred = Predicate.Set.is_empty (Predicate.Set.inter (Predicate.Map.find pred dep) non_perms) in
  let rec fixpoint perms non_perms =
    let new_perms = Predicate.Set.filter (is_perm non_perms) non_perms in
    if Predicate.Set.is_empty new_perms then perms
    else fixpoint (Predicate.Set.union perms new_perms) (Predicate.Set.diff non_perms new_perms) in
  fixpoint Predicate.Set.empty preds

let get_perms prog =
  let dep_graph = Analysis.transitive_dep_graph prog in
  let not_perms = [Predicate.True; Predicate.Init; Predicate.Does; Predicate.Next] in
  let is_permanent key set = not (List.mem key not_perms) && not (Predicate.Set.mem Predicate.True set) && not (Predicate.Set.mem Predicate.Does set) in
  deprintf "frequency: %s\n%!" (Predicate.Map.print Predicate.Set.print dep_graph);
  Predicate.Map.keys (Predicate.Map.filter is_permanent dep_graph)

let permanents prog =
  let map = frequency T1 prog in
  let aux pred freq set = if freq = Permanent then Predicate.Set.add pred set else set in
  Predicate.Map.fold aux map Predicate.Set.empty

