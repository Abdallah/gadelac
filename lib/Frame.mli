type parameter = string

val make : parameter -> 'a Desugared.T.program -> Ast.T.free Desugared.T.program
