open Printf

let clean_vars clause =
  let vars = Desugared.get_variables_clause clause in
  let upcs = Utils.PList.map String.capitalize_ascii vars in
  Utils.PList.intersect vars upcs = []

let escape = Utils.MString.escape ['_'; '+'; '-'; '<'; '>'; '='; '/'; '\''; '\\'] '_'

let variable capitalize v = let v = escape v in if capitalize then String.capitalize_ascii v else "V" ^ v
(*let symbol fct = if fst fct = "+" then "+" else try string_of_int (int_of_string (fst fct)) with Failure _ -> "s_" ^ escape (Ast.Print.symbol fct)*)
let symbol ((name, _, _) as fct) = if name = "__gad_zero" then "0" else "s_" ^ escape (Ast.Print.symbol fct)
let predicate fct = "p_" ^ escape (Ast.Print.predicate fct)

let rec term capitalize = function
  | Ast.T.Variable var -> variable capitalize var
  | Ast.T.Fact (symb, []) -> symbol symb
(*  | Ast.T.Fact (("+", _), l) -> sprintf "%s" (Utils.Print.list' "" "+" "" (term capitalize) l)*)
  | Ast.T.Fact (("__gad_incr", 1, 1), [time]) -> sprintf "1+%s" (term capitalize time)
  | Ast.T.Fact (symb, l) -> sprintf "%s%s" (symbol symb) (Utils.Print.list' "(" "," ")" (term capitalize) l)
let atomic capitalize (pred, terms) = match terms with
  | [] -> predicate pred
  | _ :: _ -> sprintf "%s%s" (predicate pred) (Utils.Print.list' "(" "," ")" (term capitalize) terms)

let literal capitalize = function
  | Desugared.T.Okay     atom -> atomic capitalize atom
  | Desugared.T.Negation atom -> sprintf "not %s" (atomic capitalize atom)
  | Desugared.T.Distinct (var, t) -> sprintf "%s != %s" (variable capitalize var) (term capitalize t)

let clause (head, body) =
  let capitalize = clean_vars (head, body) in
  let head = atomic capitalize head in
  if body = [] then head ^ "."
  else sprintf "%s :- %s." head (Utils.Print.list' "" ", " "" (literal capitalize) body)

let program prog =
  let prog = Temporize.program prog in
  Utils.Print.unlines clause prog
