open Printf
let _ = ignore printf

type t = (Ast.T.predicate * int, Ast.T.symbol * int) Either.t
let print = Either.fold ~left:(Utils.Print.couple' "(" " " ")" Ast.Print.predicate Utils.Print.int) ~right:(Utils.Print.couple' "(" " " ")" Ast.Print.symbol Utils.Print.int)

let associated f =
  let arity = Ast.arity f in
  assert (arity >= 0);
  List.map (fun i -> Either.Right (f, i)) (Utils.PList.range 0 arity)

let associated_pred f =
  let arity = Ast.arity_pred f in
  assert (arity >= 0);
  List.map (fun i -> Either.Left (f, i)) (Utils.PList.range 0 arity)

let non_variables (f, terms) =
  let f : type a. 'b -> a Ast.T.term -> 'c = fun (accu, i) -> function
    | Ast.T.Variable _ -> (accu, i + 1)
    | Ast.T.Fact _ -> (Either.Right (f, i) :: accu, i + 1) in
  let (contexts, _) = List.fold_left f ([], 0) terms in
  List.rev contexts

let rec from_variable_term : type a. string -> Ast.T.symbol -> int -> a Ast.T.term -> t list = fun var fct i -> function
  | Ast.T.Fact (fct2, terms) -> Utils.PList.flatten (Utils.PList.mapi (from_variable_term var fct2) 0 terms)
  | Ast.T.Variable var2 -> if var = var2 then [Either.Right (fct, i)] else []
let from_variable_atom var (pred, terms) =
  let find : type a. int -> a Ast.T.term -> t list = fun i -> function
    | Ast.T.Fact (fct, terms) -> Utils.PList.flatten (Utils.PList.mapi (from_variable_term var fct) 0 terms)
    | Ast.T.Variable var2 -> if var = var2 then [Either.Left (pred, i)] else [] in
  let results = Utils.PList.mapi find 0 terms in
  Utils.PList.flatten results
let from_variable_lit : type a. string -> a Desugared.T.literal -> t list = fun var -> function
  | Desugared.T.Okay atom -> from_variable_atom var atom
  | Desugared.T.Negation _ -> []
  | Desugared.T.Distinct _ -> []
let from_variable var (head, body) =
  let head = from_variable_atom var head in
  let body = List.concat_map (from_variable_lit var) body in
  let results = Utils.PList.uniques (head @ body) in
  assert (List.for_all (function | Either.Right (f, i) -> Ast.arity f > i | Either.Left (f, i) -> Ast.arity_pred f > i) results);
  results

(*Ast.get_variables*)
let get_var : type a. a Ast.T.term -> Variable.t list = function
  | Ast.T.Fact _ -> []
  | Ast.T.Variable v -> [v]
let rec to_variable_term : type a. (Ast.T.symbol * int) -> a Ast.T.term -> Variable.Set.t = fun (f, i) -> function
  | Ast.T.Variable _ -> Variable.Set.empty
  | Ast.T.Fact (symbol, terms) ->
    if f = symbol then Variable.Set.of_list (Ast.get_variables (Utils.PList.nth i terms))
    else Variable.Set.maps_union (to_variable_term (f, i)) terms
let to_variable_atomic context (pred, terms) = match context with
  | Either.Left (pred', i) -> if pred <> pred' then Variable.Set.empty else Variable.Set.of_list (get_var (Utils.PList.nth i terms))
  | Either.Right context -> Variable.Set.maps_union (to_variable_term context) terms
let to_variable_lit : type a. t -> a Desugared.T.literal -> Variable.Set.t = fun con -> function
  | Desugared.T.Okay atom | Desugared.T.Negation atom -> to_variable_atomic con atom
  | Desugared.T.Distinct (_, term) -> Utils.PEither.get_right Variable.Set.empty (Either.map_right (fun con -> to_variable_term con term) con)
let to_variable con (head, body) =
  let head = to_variable_atomic con head
  and body = Variable.Set.maps_union (to_variable_lit con) body in
  Variable.Set.union head body
