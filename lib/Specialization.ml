open Printf

type context = Ast.T.symbol * int list

let rec get_and_remove_ith i l = match l with
  | [] -> assert false
  | a :: r when i <= 0 -> a, r
  | a :: r ->
    let (a', r') = get_and_remove_ith (i - 1) r in
    (a', a :: r')

let rec get_and_remove_aux is l = match is with
  | [] -> ([], l)
  | i :: r ->
    let (a, l') = get_and_remove_ith i l in
    let r' = List.map (fun i' -> i' - 1) r in
    let (ass, l'') = get_and_remove_aux r' l' in
    (a :: ass, l'')

(** [get_and_remove is l] returns a pair [(selected, other)] where [selected] is a list of elements of [l] at index [is] and [other] is the remaining list. *)
let get_and_remove is l =
  assert (is = List.sort compare is);
  get_and_remove_aux is l

(* Bug in the following line if the gterm is complex? *)
let fun_cst_one (fct_name, arity, old_arity) (_, gterm) = (sprintf "%s_%s" fct_name (Ast.Print.term gterm), arity - 1, old_arity)
let fun_cst (fct, is) gterms =
  assert (List.length is = List.length gterms);
  let igs = List.combine is gterms in
  List.fold_left fun_cst_one fct igs

let rec term_aux : type a. context -> Ast.T.symbol * a Ast.T.term list -> Ast.T.symbol * a Ast.T.term list = fun context (f, ts) ->
  let (fct, is) = context in
  if f <> fct then (f, List.map (term context) ts)
  else
    (assert (is <> []); assert (List.length ts > (Utils.PList.max compare is));
     let (ass, ts') = get_and_remove is ts in
     let ground_as = List.filter_map Desugared.ground ass in
     if List.length ground_as <> List.length ass
     then (eprintf "%s\n%s\n%s\n%s\n" (Utils.Print.list Ast.Print.term ass) (Utils.Print.list Ast.Print.term ground_as)
             (Utils.Print.list Utils.Print.int is) (Ast.Print.term (Ast.T.Fact (f, ts))); assert false);
     let new_fct = fun_cst context ground_as in
     (new_fct, List.map (term context) ts'))
and term : type a. context -> a Ast.T.term -> a Ast.T.term = fun context -> function
  | Ast.T.Variable _ as t -> t
  | Ast.T.Fact at -> Ast.T.Fact (term_aux context at)

let atomic context (pred, ts) = (pred, List.map (term context) ts)

let literal : type a. context -> a Desugared.T.literal -> a Desugared.T.literal = fun context -> function
  | Desugared.T.Distinct (s, t) -> Desugared.T.Distinct (s, term context t)
  | Desugared.T.Negation atom   -> Desugared.T.Negation (atomic context atom)
  | Desugared.T.Okay     atom   -> Desugared.T.Okay     (atomic context atom)

let clause context (head, body) = (atomic context head, List.map (literal context) body)

let program_con key is rules = List.map (clause (key, is)) rules

let regroup_contexts l =
  let add (pmap, smap) = function
    | Either.Left  (f, i) -> (Predicate.Map.update f (Utils.Int.Set.add i) Utils.Int.Set.empty pmap, smap)
    | Either.Right (f, i) -> (pmap, Symbol.Map.update f (Utils.Int.Set.add i) Utils.Int.Set.empty smap) in
  List.fold_left add (Predicate.Map.empty, Symbol.Map.empty) l

let make contexts rules =
  let (_, contexts) = regroup_contexts contexts in
  let remove_double_contexts is = List.sort compare (Utils.Int.Set.elements is) in
  let contexts = Symbol.Map.map remove_double_contexts contexts in
  let rules = Symbol.Map.fold program_con contexts rules in
  rules

let rec flatten_term : type a. a Ast.T.term -> a Ast.T.term list = function
  | Ast.T.Variable _ as term -> [term]
  | Ast.T.Fact ((name, _, old_arity), args) -> Ast.T.Fact ((name, 0, old_arity), []) :: List.concat_map flatten_term args

let flatten_atomic (pred, args) = (pred, List.concat_map flatten_term args)
let flatten_literal : type a. a Desugared.T.literal -> a Desugared.T.literal = function
  | Desugared.T.Okay atom -> Desugared.T.Okay (flatten_atomic atom)
  | Desugared.T.Negation atom -> Desugared.T.Negation (flatten_atomic atom)
  | Desugared.T.Distinct (var, term) -> assert (List.length (flatten_term term) = 1); Desugared.T.Distinct (var, term)
let flatten_clause (head, body) = (flatten_atomic head, Utils.PList.map flatten_literal body)
let flatten_program prog = Utils.PList.map flatten_clause prog


let rec print_ground_term : type a. a Ast.T.term -> string = function
  | Ast.T.Variable _ -> assert false
  | Ast.T.Fact (n, []) -> Ast.Print.symbol n
  | Ast.T.Fact (n, ts) -> sprintf "%s_%s" (Ast.Print.symbol n) (Utils.Print.list' "" "_" "" print_ground_term ts)
let specialize_predicate pred rest grounds =
  let ng = List.length grounds
  and nr = List.length rest in
  assert (Ast.arity_pred pred = ng + nr);
  if ng > 0 then
    (let pr = Ast.Print.predicate pred in
     let gr = Utils.Print.list' "_" "_" "" print_ground_term grounds in
     let npred = sprintf "%s%s" pr gr in
     (Predicate.Constant (npred, nr, nr), rest))
  else (pred, rest)

let specialize_atomic contexts (pred, args) =
  let indexes = List.filter_map (fun (p, i) -> if p = pred then Some i else None) contexts in
  let aux (i, left, right) arg = if List.mem i indexes then (i + 1, left, arg :: right) else (i + 1, arg :: left, right) in
  let (_, rest, grounds) = List.fold_left aux (0, [], []) args in
  specialize_predicate pred (List.rev rest) (List.rev grounds)

let specialize_literal : type a. (Ast.T.predicate * int) list -> a Desugared.T.literal -> a Desugared.T.literal = fun contexts -> function
  | Desugared.T.Distinct _ as d -> d
  | Desugared.T.Negation atom -> Desugared.T.Negation (specialize_atomic contexts atom)
  | Desugared.T.Okay atom -> Desugared.T.Okay (specialize_atomic contexts atom)

let specialize_clause contexts (head, body) = (specialize_atomic contexts head, List.map (specialize_literal contexts) body)

let specialize_program contexts prog = Utils.PList.map (specialize_clause contexts) prog
