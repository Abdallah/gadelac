open Printf

let deprintf format = if false then eprintf format else ifprintf stderr format

let no_double_variable_term term = Utils.PList.no_doubles (Ast.get_variables term)

let rec des_unify : type a. a Ast.T.term -> a Ast.T.term -> ((string * a Ast.T.term) list * (string * a Ast.T.term) list) option = fun term1 term2 -> match term1, term2 with
  | Ast.T.Variable v1, Ast.T.Variable v2 when (v1 = v2) -> Some ([], [])
  | Ast.T.Variable s1, _ -> Some ([(s1, term2)], [])
  | _, Ast.T.Variable s2 -> Some ([], [(s2, term1)])
  | Ast.T.Fact (f1, _), Ast.T.Fact (f2, _) when f1 <> f2 -> None
  | Ast.T.Fact (f1, l1), Ast.T.Fact (f2, l2) ->
    assert (f1 = f2);
    assert (List.length l1 = List.length l2);
    let args = List.map2 des_unify l1 l2 in
    let aux (accu1, accu2) arg = match arg with
      | None -> raise Exit
      | Some (x1, x2) -> List.rev_append x1 accu1, List.rev_append x2 accu2 in
    try let (res1, res2) = List.fold_left aux ([], []) args in
        Some (List.rev res1, List.rev res2) with Exit -> None

let is_refinement terms term =
  let is_dominated fluent1 fluent2 = match des_unify fluent2 fluent1 with
    | None -> false
    | Some (_, []) -> true
    | Some (_, _ :: _) -> false in
  List.exists (is_dominated term) terms

type parameter = string

module D = Domain.Exact
module NaiveD = Reachability.Naive (D)

module ChoosePartition =
struct
  let get_possible_actions program =
    let domains = NaiveD.forward false program in
    let actions = D.unify (Predicate.Legal, [Ast.T.Variable "role"; Ast.T.Variable "name"]) domains in
    assert (List.for_all (fun sub -> Variable.Map.mem "role" sub && Variable.Map.mem "name" sub) actions);
    let get_action_name sub = match Variable.Map.find "name" sub with
      | Ast.T.Variable _ -> assert false
      | Ast.T.Fact (name, _) -> name in
    let roles = Utils.PList.uniques (Utils.PList.map (Variable.Map.find "role") actions)
    and actis = Utils.PList.uniques (Utils.PList.map get_action_name actions) in
    (roles, actis)

  let naive_basis program =
    let (roles, _) = get_possible_actions program in
    let make_action i role = (role, Ast.T.Variable (sprintf "_action_%d" i)) in
    let actions = Utils.PList.mapi make_action 0 roles in
    let fluents = Ast.T.Variable (sprintf "_fluent") in
    [(fluents, actions)]

  let partial_ground name depth subs =
    assert (List.for_all (Variable.Map.mem name) subs);
    let terms = List.map (Variable.Map.find name) subs in
    let aux term = Ast.abstract (Generate.variable name) depth term in
    Utils.PList.uniques (Utils.PList.map aux terms)

  let filter_noop noop actions move_tuples =
    let noop_action = Ast.T.Fact ((noop, 0, 0), []) in
    let is_sequential list = Utils.PList.count (fun (_, a) -> a <> noop_action) list = 1 in
    if List.mem noop_action actions then List.filter is_sequential move_tuples
    else move_tuples

  let depth_basis noop flu_d act_d program =
    let domains = NaiveD.backward false program in
    let fluents = D.unify (Predicate.True,  [Ast.T.Variable "fluent"]) domains in
    let fluents = partial_ground "fluent" flu_d fluents in
    let roles = D.unify (Predicate.Role,  [Ast.T.Variable "role"]) domains in
    assert (List.for_all (Variable.Map.mem "role") roles);
    let roles = List.map (Variable.Map.find "role") roles in
    let actions role = 
      let acts = D.unify (Predicate.Legal, [role; Ast.T.Variable "act"]) domains in
      let acts = partial_ground "act" act_d acts in
      List.map (fun act -> (role, act)) acts in
    let doeses = Utils.PList.cross_products (List.map actions roles) in
    let act_names = D.unify (Predicate.Legal, [Ast.T.Variable "role"; Ast.T.Variable "act"]) domains in
    let act_names = List.map (Variable.Map.find "act") act_names in
    let doeses = filter_noop noop act_names doeses in
    Utils.PList.cross_product fluents doeses

  let action_partition prog =
    let (roles, actions) = get_possible_actions prog in
    let role_actions = List.rev_map (fun role -> List.rev_map (fun act -> (role, act)) actions) roles in
    let l = Utils.PList.cross_products role_actions in
    (* eprintf "to pddl\n%s\n%!" (Utils.Print.unlines (Utils.Print.list (Utils.Print.couple Ast.Print.symbol Ast.Print.symbol)) l);*)
    l

  let get_possible_fluents program =
    let domains = NaiveD.forward false program in
    let fluents = D.unify (Predicate.True, [Ast.T.Variable "fluent"]) domains in
    let fluents = Utils.PList.map (function Ast.T.Fact (flu, _) -> flu | Ast.T.Variable _ -> assert false) (Utils.PList.map (Variable.Map.find "fluent") fluents) in
    Symbol.Set.of_list fluents

  let make_action_partition program =
    let (roles, actions) = get_possible_actions program in
    let role_vars = List.map (fun role_id -> Ast.T.Variable (sprintf "_r%d" role_id)) (Utils.PList.range 0 (List.length roles)) in
    let action_vars role_id a = List.map (fun action_id -> Ast.T.Variable (sprintf "_a%d_%d" role_id action_id)) (Utils.PList.range 0 (Ast.arity a)) in
    let all_actions role_id role = List.map (fun a -> (role, a, action_vars role_id a)) actions in
    let legal_actions = Utils.PList.mapi all_actions 0 role_vars in
    let move_tuples = Utils.PList.cross_products legal_actions in
    List.map (List.map (fun (role, a, vars) -> (role, Ast.T.Fact (a, vars)))) move_tuples

  let filter_noop noop actions move_tuples =
    let noop_action = (noop, 0, 0) in
    let is_sequential list = Utils.PList.count (fun (_, a, _) -> a <> noop_action) list = 1 in
    if List.mem noop_action actions then List.filter is_sequential move_tuples
    else move_tuples

  let make_action_partition2 noop program =
    let (roles, actions) = get_possible_actions program in
    let action_vars role_id a = List.map (fun action_id -> Ast.T.Variable (sprintf "_a%d_%d" role_id action_id)) (Utils.PList.range 0 (Ast.arity a)) in
    let all_actions role_id role = List.map (fun a -> (role, a, action_vars role_id a)) actions in
    let legal_actions = Utils.PList.mapi all_actions 0 roles in
    let move_tuples = Utils.PList.cross_products legal_actions in
    let move_tuples = filter_noop noop actions move_tuples in
    let move_tuples = List.map (List.map (fun (role, a, vars) -> (role, Ast.T.Fact (a, vars)))) move_tuples in
    move_tuples

(*  let make_action_covering program fluent =
    let find_does = function | Desugared.T.Okay (Predicate.Does atom) | Desugared.T.Negation (Predicate.Does atom) -> Some atom | _ -> None in
    let is_frame_rule (head, body) = match head with
      | Predicate.Next, [flu] -> if Unification.unify flu fluent = None then [] else Utils.PList.filter_map find_does body
      | Predicate.Next, _ -> assert false
      | _ -> [] in
    let frame_rules_doeses = List.concat_map is_frame_rule program*)

  (** Return all fluents with any arguments *)
  let make_fluent_partition prog =
    let fluents = get_possible_fluents prog in
    let fluent_vars f = List.map (fun fluent_id -> Ast.T.Variable (sprintf "_f%d" fluent_id)) (Utils.PList.range 0 (Ast.arity f)) in
    let make_term f = Ast.T.Fact (f, fluent_vars f) in
    Symbol.Set.mapl make_term fluents

  let abstract_variables fluent =
    assert (no_double_variable_term fluent);
    let new_var = 
      let var_id = ref 0 in
      fun () -> incr var_id; sprintf "_f%d" !var_id  in
    let rec aux_term = function | Ast.T.Variable _ -> Ast.T.Variable (new_var ()) | Ast.T.Fact (fct, terms) -> Ast.T.Fact (fct, List.map aux_term terms) in
    aux_term fluent

  (** Return all fluents with arguments making them "frame fluents"*)
  let make_fluent_partition2 prog =
    let from_rule (head, body) = match head with
      | Predicate.Next, [flu] -> 
        let true_lit = Desugared.T.Okay (Predicate.True, [flu]) in
        if List.mem true_lit body then Some flu else None
      | Predicate.Next, _ -> assert false
      | _, _ -> None in
    let fluents = List.filter_map from_rule prog in
    let fluents = List.map abstract_variables fluents in
    let isnt_subsumed fluents flu = not (is_refinement (List.filter ((<>) flu) fluents) flu) in
    let fluents = List.filter (isnt_subsumed fluents) fluents in
    Utils.PList.uniques fluents

  let get_effects noop program =
    let act_partition = make_action_partition2 noop program in
    let fluents = make_fluent_partition program in
    Utils.PList.cross_product fluents act_partition

  let is_frame_fluent program fluent =
    let is_frame_rule (head, body) = match head with
      | Predicate.Next, [flu] -> List.mem (Desugared.T.Okay (Predicate.True, [flu])) body && des_unify fluent flu <> None
      | Predicate.Next, _ -> assert false
      | _, _ -> false in
    List.exists is_frame_rule program

  let get_frame_effects noop program =
    let act_partition = make_action_partition2 noop program in
    let fluents = make_fluent_partition2 program in
    let frame, non_frame = List.partition (is_frame_fluent program) fluents in
    Utils.PList.cross_product frame act_partition

  let get_frame_effects2 noop program =
    let act_partition = make_action_partition2 noop program in
    let fluents = make_fluent_partition program in
    let frame, non_frame = List.partition (is_frame_fluent program) fluents in
    let cross_frame = Utils.PList.cross_product frame act_partition in
    cross_frame @ (List.map (fun fluent -> (fluent, [])) non_frame)

  let no_effects _ = []

end

module SplitRules =
struct
  let is_does_literal : type a. a Desugared.T.literal -> (a Desugared.T.literal, (a Ast.T.term * a Ast.T.term, a Ast.T.term * a Ast.T.term) Either.t) Either.t = function
    | Desugared.T.Okay (Predicate.Does, [role; action]) -> Either.Right (Either.Right (role, action))
    | Desugared.T.Negation (Predicate.Does, [role; action]) -> Either.Right (Either.Left (role, action))
    | Desugared.T.Okay (Predicate.Does, _) | Desugared.T.Negation (Predicate.Does, _) -> assert false
    | Desugared.T.Okay _ 
    | Desugared.T.Negation _ as lit -> Either.Left lit
    | Desugared.T.Distinct _ as lit -> Either.Left lit

  let (inert_init, inert_true) = if false then (Predicate.Constant ("inertinit", 1, 1), Predicate.Constant ("inerttrue", 1, 1)) else (Predicate.Init, Predicate.True)

  type 'a fr_rule = { fluent : 'a Ast.T.term;
                      doeses : ('a Ast.T.term * 'a Ast.T.term) list;
                      not_doeses : ('a Ast.T.term * 'a Ast.T.term) list;
                      body : 'a Desugared.T.literal list }
  let print_fr_rule { fluent; doeses; not_doeses; body } =
    assert (not_doeses = []);
    sprintf "fluent:%s doeses:%s body:%s"
      (Ast.Print.term fluent) (Utils.Print.list (Utils.Print.couple Ast.Print.term Ast.Print.term) doeses) (Utils.Print.unspaces Desugared.Print.literal body)

  type 'a rule =
    | Frame of 'a fr_rule
    | Next  of 'a fr_rule
    | Other of 'a Desugared.T.clause
                  
  let partition f l =
    let aux (accu_frame, accu_next, accu_other) rule = match f rule with
      | Frame ru -> (ru :: accu_frame, accu_next, accu_other)
      | Next  ru -> (accu_frame, ru :: accu_next, accu_other)
      | Other ru -> (accu_frame, accu_next, ru :: accu_other) in
    let frame, next, other = List.fold_left aux ([], [], []) l in
    (List.rev frame, List.rev next, List.rev other)

  let is_frame_fluent inert_fluents = function
    | Predicate.Next, [fluent] -> if is_refinement inert_fluents fluent then Some fluent else None
    | Predicate.Next, _ -> assert false
    | _, _ -> None

  let alter_init inert_fluents head = match head with
    | Predicate.Init, [fluent] -> if is_refinement inert_fluents fluent then (inert_init, [fluent]) else head
    | Predicate.Init, _ -> assert false
    | _, _ -> head
  let alter_true inert_fluents body = 
    let is_inert = is_refinement inert_fluents in
    let alter_lit lit = match lit with
      | Desugared.T.Okay (Predicate.True, [fluent]) -> if is_inert fluent then Desugared.T.Okay (inert_true, [fluent]) else lit
      | Desugared.T.Negation (Predicate.True, [fluent]) -> if is_inert fluent then Desugared.T.Negation (inert_true, [fluent]) else lit
      | Desugared.T.Okay (Predicate.True, _) | Desugared.T.Negation (Predicate.True, _) -> assert false
      | _ -> lit in
    List.map alter_lit body

  let split_rule inert_fluents (head, hyps) =
    match is_frame_fluent inert_fluents head with
    | Some fluent -> 
      let lit = Desugared.T.Okay (Predicate.True, [fluent]) in
      let (no_doeses, actions) = List.partition_map is_does_literal hyps in
      let body = alter_true inert_fluents (List.filter ((<>) lit) no_doeses) in
(*      eprintf "hyps: %s\nbody: %s\n\n%!" (Utils.Print.unspaces Desugared.Print.literal hyps) (Utils.Print.unspaces Desugared.Print.literal body);*)
      let not_doeses, doeses = List.partition_map (fun x -> x) actions in
      let fr = { fluent; doeses; not_doeses; body } in
      if List.mem lit hyps then Frame fr else Next fr
    | None -> Other (alter_init inert_fluents head, alter_true inert_fluents hyps)

  let split_rules inert_fluents program =
(*    eprintf "inerts: %s\n%!" (Utils.Print.unspaces Desugared.Print.term inert_fluents);*)
    partition (split_rule inert_fluents) program

  module ArgBody = Utils.DataStructure.F (struct type t = Ast.T.free Ast.T.term list * Ast.T.free Desugared.T.literal list let compare = compare let print _ = "" end)
  let no_dup_new_pred =
    let generator = Generate.predicate "pddl" in
    let map = ref ArgBody.Map.empty in
    let new_predicate args_body = 
      let pred = generator (List.length (fst args_body)) in
      map := ArgBody.Map.add args_body pred !map;
      pred in
    (fun args_body ->
      match ArgBody.Map.find_opt args_body !map with
      | None -> new_predicate args_body
      | Some pred -> pred)

end

let make_context (fluent, actions) =
  let make_does (role, action) = (Predicate.Does, [role; action]) in
  List.map make_does actions @ [(SplitRules.inert_true, [fluent])]

let introduce_predicate context (sub, body) =
  let context = make_context context in
  let is_needed atom =
    let atom2 = Unification.apply_atomic sub atom in
    if atom2 = atom then None else Some (Desugared.T.Okay atom2) in
  let needed_context = List.filter_map is_needed context in
  let body = needed_context @ body in
  let fram_vars = Utils.PList.uniques (List.concat_map Ast.get_variables_atomic context) in
  let args = Variable.Map.from_list (Unification.apply_var sub) fram_vars in
  let body_vars = List.concat_map Ast.get_variables (Variable.Map.values args) @ List.concat_map Desugared.get_variables_lit body in
  let constrained _ = function
    | Ast.T.Fact _ -> true
    | Ast.T.Variable var -> Utils.PList.count ((=) var) body_vars <> 1 in
  let args = Variable.Map.filter constrained args in
  let carried_vars, carried_args = Utils.PList.split (Variable.Map.bindings args) in
  let new_pred = SplitRules.no_dup_new_pred (carried_args, body) in
  let new_term = (new_pred, Utils.PList.map Ast.variable carried_vars) in
  let new_rule = ((new_pred, carried_args), body) in
  (new_term, new_rule)
    
let to_substitution conjunction =
  let sub, _ = Utils.PList.fold_stop' Unification.add_equality Unification.empty conjunction in
  sub
    
let apply_sub distincts sub =
  let aux accu dist =
    let dists = Desugared.simplify_distinct sub dist in
    Utils.PList.cross_product_map (@) dists accu in
  (sub, List.fold_left aux [[]] distincts)

let partition_body body =
  let aux = function
    | Desugared.T.Okay     atom   -> Either.Right (Either.Right atom)
    | Desugared.T.Negation atom   -> Either.Right (Either.Left  atom)
    | Desugared.T.Distinct (v, t) -> Either.Left  (Ast.T.Variable v, t) in
  List.partition_map aux body

let make_hypotheses hyps sub =
  let aux = function
    | Either.Right atom -> Desugared.T.Okay     (Unification.apply_atomic sub atom)
    | Either.Left  atom -> Desugared.T.Negation (Unification.apply_atomic sub atom) in
  List.map aux hyps

let add_equalities (fluent, actions) rule =
  let doeses = List.map (fun (role, action) -> Ast.T.Fact (("does", 2, 2), [role; action])) rule.SplitRules.doeses
  and not_do = List.map (fun (role, action) -> Ast.T.Fact (("does", 2, 2), [role; action])) rule.SplitRules.not_doeses
  and actios = List.map (fun (role, action) -> Ast.T.Fact (("does", 2, 2), [role; action])) actions in
  let flu_eq = (fluent, rule.SplitRules.fluent) in
  let action_equ   does = Utils.PList.map (fun action -> (action,   does)) actios in (** takes a does hypothesis and returns a disjunction of constraints. *)
  let action_neq nodoes = Utils.PList.map (fun action -> (action, nodoes)) actios in (** a conjunction of constraints *)
  let equality_cnf = [flu_eq] :: List.map action_equ doeses in (** a cnf of constraints. *)
  let equality_dnf = Utils.PList.cross_products equality_cnf in (** a dnf of constraints. *)
  let equality_subs = List.filter_map to_substitution equality_dnf in
  let inequali_conj = List.concat_map action_neq not_do in (** a conjunction of constraints. *)
  assert (inequali_conj = []); (* as long as we have no not_doeses! *)
  let dists, hyps = partition_body rule.SplitRules.body in
  let distincts = dists @ inequali_conj in
  let constraint_sets = Utils.PList.map (apply_sub distincts) equality_subs in
  let bodies = List.concat_map (fun (sub, dnf) -> (List.map (fun conj -> sub, make_hypotheses hyps sub @ conj) dnf)) constraint_sets in
  (* eprintf "%s\n%!" (Utils.Print.unlines (Utils.Print.list
     Desugared.Print.literal) (List.map snd bodies));*)
  bodies

let group_frame_rules frames context =
  let bodies = List.concat_map (add_equalities context) frames in
  let bodies, rules = Utils.PList.split_map (introduce_predicate context) bodies in
  (* eprintf "frames:\n%s\ncontext: %s\ndnfs\n%s\n%!"
     (Utils.Print.unlines SplitRules.print_fr_rule frames)
     (Utils.Print.couple Ast.Print.term (Utils.Print.list
     (Utils.Print.couple Ast.Print.term Ast.Print.term)) context)
     (Utils.Print.unlines (Utils.Print.list Ast.Print.literal)
     bodies);*)
  (context, bodies), rules

let transpose_rule (context, body) =
  let head = (Predicate.Negative, [fst context]) in
  let ast_body = make_context context in
  let ast_body = Utils.PList.map (fun fact -> Desugared.T.Okay fact) ast_body in
  let negate fact = Desugared.T.Negation fact in
  let neg_body = Utils.PList.map negate body in
  let neg_body = Utils.PList.uniques neg_body in
  (head, ast_body @ neg_body)

let positive_frame_rules effects frames =
  let make_ast (fluent, actions) =
    let sub_bodies = List.concat_map (add_equalities (fluent, actions)) frames in
    let head = (Predicate.Positive, [fluent]) in
    let acts = Utils.PList.map (fun (r, a) -> (Predicate.Does, [r; a])) actions in
    let clause (sub, body) = Unification.apply_atomic sub head, Utils.PList.map (fun act -> Desugared.T.Okay (Unification.apply_atomic sub act)) acts @ body in
    Utils.PList.map clause sub_bodies in
  List.concat_map make_ast effects

let negative_frame_rules effects frames =
  let rules, add = Utils.PList.split_map (group_frame_rules frames) effects in
  let rulestr = Utils.PList.map transpose_rule rules in
  (rulestr, Utils.PList.uniques (Utils.PList.flatten add))

let make noop program =
(*  let fr_fluents = ChoosePartition.naive_basis program in*)
  let fr_fluents = ChoosePartition.depth_basis noop 0 0 program in
(*  let fr_fluents = ChoosePartition.get_frame_effects2 noop program in*)
  let inert_fluents = Utils.PList.uniques (List.map fst fr_fluents) in
  let frames, nexts, others = SplitRules.split_rules inert_fluents (Desugared.cast_program program) in
  let negs, rest = negative_frame_rules fr_fluents frames in
  eprintf "Frame: before inlining\n%s\n%!" (Desugared.Print.program (negs @ rest));
  let negs_in = Inlining.inline_aux max_int rest negs in
  let poss = positive_frame_rules fr_fluents nexts in
  let full = negs_in @ poss @ rest @ others in
  full
