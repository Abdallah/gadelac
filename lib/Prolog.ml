open Printf

(*  let escape = Utils.MString.escape ['_'; '+'; '-'; '<'; '>'; '='; '/'; '\''; '\\'] '_'*)
let escape = Utils.MString.escape ['+'; '-'; '<'; '>'; '='; '/'; '\''; '\\'] '_'

let variable capitalize v = let v = escape v in if capitalize then String.capitalize_ascii v else "V" ^ v
(*let symbol fct = escape (Ast.Print.symbol fct)*)
let symbol ((name, _, _) as fct) = if name = "__gad_zero" then "0" else escape (Ast.Print.symbol fct)
let predicate fct = "d_" ^ escape (Ast.Print.predicate fct)

let rec term : type a. 'b -> a Ast.T.term -> string = fun capitalize -> function
  | Ast.T.Variable var -> variable capitalize var
  | Ast.T.Fact (symb, []) -> symbol symb
  | Ast.T.Fact (("__gad_incr", 1, 1), [time]) -> sprintf "1+%s" (term capitalize time)
  | Ast.T.Fact (symb, l) -> sprintf "%s%s" (symbol symb) (Utils.Print.list' "(" "," ")" (term capitalize) l)
let atomic capitalize (pred, terms) = match terms with
  | [] -> predicate pred
  | _ :: _ -> sprintf "%s%s" (predicate pred) (Utils.Print.list' "(" "," ")" (term capitalize) terms)

let literal : type a. 'b -> a Desugared.T.literal -> string = fun capitalize -> function
  | Desugared.T.Okay     atom -> atomic capitalize atom
  | Desugared.T.Negation atom -> sprintf "\\+ %s" (atomic capitalize atom)
  | Desugared.T.Distinct (var, t) -> sprintf "%s \\= %s" (variable capitalize var) (term capitalize t)

let clause (head, body) =
  let capitalize = ASP.clean_vars (head, body) in
  let head = atomic capitalize head in
  if body = [] then head ^ "."
  else sprintf "%s :- %s." head (Utils.Print.list' "" ", " "" (literal capitalize) body)

let program prog = Utils.Print.unlines clause prog
