open Printf

include Types.SCOPE
open T

type term_value = Ast.T.symbol * string

let read_variable v = v

module Print =
struct
  let var_type = function | `TTerm -> "atom" | `TFTerm -> "fatom" | `TArray -> "array" | `TFArray -> "farray"
  let variable = sprintf "%s"
  let affectation = function
    | Cons (var, fct) -> sprintf "%s := %s" (variable var) (Ast.Print.symbol fct)
    | Term (var, fct, arg) -> sprintf "%s := %s ?%s"  (variable var) (Ast.Print.symbol fct) (variable arg)
    | Array (var, args) -> sprintf "%s := %s"  (variable var) (Utils.Print.list (fun x -> "?" ^ variable x) args)
end

module TV = Utils.DataStructure.F (struct type t = term_value let compare = compare let print = Utils.Print.couple Ast.Print.symbol Utils.Print.string end)
module R = Utils.DataStructure.F (struct type t = variable list let compare = compare let print = Utils.Print.list Print.variable end)

type t =
  { counter : int;
    affectations : affectation list;
    strings : Variable.Set.t;
    pat : string;
    conss : Ast.T.symbol Variable.Map.t;
    cons_map : (int * string) Symbol.Map.t;
    terms : term_value Variable.Map.t;
    term_map : (int * string) TV.Map.t;
    arrays : variable list Variable.Map.t;
    array_map : (int * string) R.Map.t }

let empty pat =
  { counter = 0;
    affectations = [];
    strings = Variable.Set.empty;
    pat = pat;
    conss = Variable.Map.empty;
    cons_map = Symbol.Map.empty;
    terms = Variable.Map.empty;
    term_map = TV.Map.empty;
    arrays = Variable.Map.empty;
    array_map = R.Map.empty }

let get_cons_binding t term = Option.map snd (Symbol.Map.find_opt term t.cons_map)
let get_term_binding t term = Option.map snd (TV.Map.find_opt term t.term_map)
let get_array_binding t array = Option.map snd (R.Map.find_opt array t.array_map)

let add_cons_binding t value =
  let new_var = sprintf "__d%s%d" t.pat t.counter in
  assert (not (Symbol.Map.mem value t.cons_map));
  assert (not (Variable.Set.mem new_var t.strings));
  let new_t =
    { t with
      counter = t.counter + 1;
      affectations = Cons (new_var, value) :: t.affectations;
      strings = Variable.Set.add new_var t.strings;
      conss = Variable.Map.add new_var value t.conss;
      cons_map = Symbol.Map.add value (t.counter, new_var) t.cons_map } in
  (new_var, new_t)

let add_term_binding t value =
  let new_var = sprintf "__a%s%d" t.pat t.counter in
  assert (not (TV.Map.mem value t.term_map));
  assert (not (Variable.Set.mem new_var t.strings));
  let new_t =
    { t with
      counter = t.counter + 1;
      affectations = Term (new_var, fst value, snd value) :: t.affectations;
      strings = Variable.Set.add new_var t.strings;
      terms = Variable.Map.add new_var value t.terms;
      term_map = TV.Map.add value (t.counter, new_var) t.term_map } in
  (new_var, new_t)

let add_array_binding t value =
  let new_var = sprintf "__r%s%d" t.pat t.counter in
  assert (not (R.Map.mem value t.array_map));
  assert (not (Variable.Set.mem new_var t.strings));
  let new_t =
    { t with
      counter = t.counter + 1;
      affectations = Array (new_var, value) :: t.affectations;
      strings = Variable.Set.add new_var t.strings;
      arrays = Variable.Map.add new_var value t.arrays;
      array_map = R.Map.add value (t.counter, new_var) t.array_map } in
  (new_var, new_t)

let force_array_binding t array name =
  { t with
    strings = Variable.Set.add name t.strings;
    array_map = R.Map.add array (t.counter, name) t.array_map;
    arrays = Variable.Map.add name array t.arrays; }

let add_existing_binding t name = { t with strings = Variable.Set.add name t.strings }
let union_strings ts = List.fold_left Variable.Set.union Variable.Set.empty (List.map (fun t -> t.strings) ts)
let term_present ts term = Variable.Set.mem term (union_strings ts)
let array_present ts array = List.for_all (Fun.flip Variable.Set.mem (union_strings ts)) array

let get_all t = List.rev t.affectations
(*  let all_terms = TMap.to_list t.term_map in
  let all_terms = List.rev_map (fun ((fun_cst, var), (c, string)) -> c, Term (string, fun_cst, var)) all_terms in
  let all_arrays = RMap.to_list t.array_map in
  let all_arrays = List.rev_map (fun (vars, (c, string)) -> c, Array (string, vars)) all_arrays in
  let all_rest = AMap.to_list t.map in
  let all_rest = List.rev_map (fun (atom, (c, string)) -> c, Rest (string, atom)) all_rest in
  let all = List.sort compare (Utils.PList.flatten [all_rest; all_terms; all_arrays]) in
  List.map snd all*)
