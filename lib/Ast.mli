include module type of Types.AST with module T = Types.AST.T

module Print :
sig
  val symbol : T.symbol -> string
  val predicate : T.predicate -> string
  val term : 'a T.term -> string
  val atomic : 'a T.atomic -> string
  val literal : 'a T.literal -> string
  val program : 'a T.program -> string
end

val variable : string -> T.free T.term
val arity : T.symbol -> int
val arity_pred : T.predicate -> int
val get_variables : 'a T.term -> string list
val get_variables_atomic : 'a T.atomic -> string list
val get_variables_lit : 'a T.literal -> string list

val replace_variable_term : (string * 'a T.term) -> 'b T.term -> 'b T.term
val replace_variable_atomic : (string * 'a T.term) -> 'b T.atomic -> 'b T.atomic
val replace_variable_lit : (string * 'a T.term) -> 'b T.literal -> 'b T.literal

val ground : 'a T.atomic -> bool
val negate : 'a T.literal -> 'a T.literal

val depth_term : 'a T.term -> int
val depth : 'a T.atomic -> int

(** [abstract gen d t] returns an abstract version of [t] of depth less or equal to [d]. Assumes there is no variable at depth less than [d]. *)
val abstract : (unit -> string) -> int -> 'a T.term -> T.free T.term
val cast_term : 'a T.term -> T.free T.term
val cast_atomic : 'a T.atomic -> T.free T.atomic
val assert_ground_atomic : 'a T.atomic -> T.ground T.atomic

module S :
sig
  type term = Fact of (string * term list) | Variable of string
  type atomic = (string * term list)
  type literal = Positive of atomic | Negative of atomic
  type rule = (atomic * literal Logic.t)
  type program = rule list
end

val program : S.program -> T.free T.program
