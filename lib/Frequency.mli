(** [T0] no temporal properties is detected. 
    [T1] Permanent facts are detected. 
    [T2] Permanent and Persistent facts are detected. *)
type param = T0 | T1 | T2
val print_param : param -> string
val read_param : string -> param
type t = Permanent | Persistent | Ephemeral
val print : t -> string
val frequency : param -> 'a Desugared.T.program -> t Predicate.Map.t
val permanents : 'a Desugared.T.program -> Predicate.Set.t
