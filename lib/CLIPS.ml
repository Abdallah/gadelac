open Printf

let print_arg = function
  | Ast.T.Variable var -> sprintf "?%s" var
  | Ast.T.Fact (symb, []) -> Ast.Print.symbol symb
  | Ast.T.Fact (symb, a :: r) -> eprintf "%s %s %s\n%!" (Ast.Print.symbol symb) (Ast.Print.term a) (Utils.Print.list Ast.Print.term r); assert false
let print_term = function
  | Ast.T.Variable var -> sprintf "?%s" var
  | Ast.T.Fact (("+", _, _), args) -> sprintf "(+ %s)" (Utils.Print.unspaces print_arg args)
  | Ast.T.Fact (symb, []) -> Ast.Print.symbol symb
  | Ast.T.Fact (symb, args) -> sprintf "%s %s" (Ast.Print.symbol symb) (Utils.Print.unspaces print_arg args)

let print_atomic (head, args) =
  let args = Utils.PList.mapi (fun i term -> sprintf "(arg%d %s)" i (print_term term)) 0 args in
  sprintf "(%s %s)" (Ast.Print.predicate head) (Utils.Print.unspaces Utils.Print.string args)

let print_literal = function
  | Desugared.T.Okay     atom -> sprintf "  (logical %s)"       (print_atomic atom)
  | Desugared.T.Negation atom -> sprintf "  (logical (not %s))" (print_atomic atom)
  | Desugared.T.Distinct (var, term) -> sprintf "  (logical (test (neq ?%s %s)))" var (print_arg term)

let def_template perms pred =
  let arity = Ast.arity_pred pred + if Predicate.Set.mem pred perms then 0 else 1 in
  let args = Utils.Print.unspaces (fun i -> sprintf "(multislot arg%d)" i) (Utils.PList.range 0 arity) in
  sprintf "(deftemplate %s %s)" (Ast.Print.predicate pred) args
let def_templates perms prog =
  let predicates = Analysis.predicates prog in
  Predicate.Set.unlines (def_template perms) predicates

let def_rule strats i (head, hyp1, body) =
  assert (Predicate.Map.mem (fst head) strats);
  let strat = Predicate.Map.find (fst head) strats in
  let strat = sprintf "  (declare (salience %d))" strat in
  let head  = sprintf "  (assert %s)" (print_atomic head)
  and body  = Utils.Print.unlines print_literal (hyp1 :: body) in
  sprintf "(defrule gdl-rule%d\n%s\n%s\n  =>\n%s)" i strat body head

let def_facts facts =
  let print_fact atom = "  " ^ print_atomic atom in
  sprintf "(deffacts static-inits\n%s)" (Utils.Print.unlines print_fact facts)

let program prog =
  let prog = Temporize.program prog in
  let perms = Frequency.permanents prog in
  let strats = Stratify.strat_terms prog in
  let deftemps = def_templates perms prog in
  let has_body (head, body) = match body with [] -> Either.Left head | a :: r -> Either.Right (head, a, r) in
  let facts, rules = List.partition_map has_body prog in
  let defrules = Utils.PList.mapi (def_rule strats) 0 rules in
  let deffacts = def_facts facts in
  sprintf "%s\n\n%s\n\n%s" deftemps (Utils.Print.unlines Utils.Print.string defrules) deffacts
