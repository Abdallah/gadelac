val default : ('a -> 'b) -> 'b -> 'a option -> 'b
val combine : ('a -> 'b -> 'c) -> 'a option -> 'b option -> 'c option

val lift : 'a list option -> 'a list
