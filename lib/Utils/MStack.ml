include Stack

let mem x s =
  try iter (fun y -> if x = y then raise Exit) s; false
  with Exit -> true

let pop_while f s =
  let l = ref [] in
  while not (is_empty s) && f (top s) do
    l := pop s
  done;
  !l

let pop_upto f s =
  let rec aux accu =
    if is_empty s then accu
    else (let x = pop s in
          if f x then x :: accu
          else aux (x :: accu)) in
  aux []
