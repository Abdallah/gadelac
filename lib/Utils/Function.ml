let rec iterate stop f a =
  if stop a then a else iterate stop f (f a)

let fixpoint cmp f a =
  let stop (old_a, new_a) = cmp old_a new_a in
  let f' (_, new_a) = (new_a, f new_a) in
  fst (iterate stop f' (a, f a))

let curry f a b = f (a, b)
let uncurry f (a, b) = f a b
