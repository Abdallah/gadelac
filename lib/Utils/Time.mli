val minute : float
val hour : float

type timer

val start : float -> timer
val elapsed : timer -> bool

type chrono

val chrono_make : unit -> chrono
val chrono_restart : chrono -> unit
val chrono_elapsed : chrono -> float

val measure : ('a -> 'b) -> 'a -> ('b * float)
val measure_several : int -> ('a -> 'b) -> 'a -> ('b * float)

val children_chrono_make : unit -> Unix.process_times
val children_chrono_elapsed : Unix.process_times -> float
