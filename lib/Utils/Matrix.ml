let size m =
  let width = Array.length m in
  if width > 0 then (width, Array.length m.(0)) else (0, 0)
let copy m = 
  let (width, height) = size m in
  assert (width * height > 0);
  let m' = Array.make width [||] in
  for i = 1 to width do
    m'.(i - 1) <- Array.copy m.(i - 1)
  done;
  m'

let transpose m =
  let (width, height) = size m in
  assert (width * height > 0);
  let m' = Array.make height [||] in
  for i = 1 to height do
    m'.(i - 1) <- Array.make width m.(0).(0)
  done;
  for i = 1 to height do
    for j = 1 to width do
      m'.(i - 1).(j - 1) <- m.(j - 1).(i - 1)
    done
  done;
  m'

let vert_sym m =
  let (width, height) = size m in
  assert (width * height > 0);
  let m' = Array.make width [||] in
  for i = 1 to width do
    m'.(i - 1) <- Array.make height m.(0).(0)
  done;
  for i = 1 to width do
    for j = 1 to height do
      m'.(width - i).(j - 1) <- m.(i - 1).(j - 1)
    done
  done;
  m'

let hori_sym m =
  let (width, height) = size m in
  assert (width * height > 0);
  let m' = Array.make width [||] in
  for i = 1 to width do
    m'.(i - 1) <- Array.make height m.(0).(0)
  done;
  for i = 1 to width do
    for j = 1 to height do
      m'.(i - 1).(height - j) <- m.(i - 1).(j - 1)
    done
  done;
  m'
