val iterate : ('a -> bool) -> ('a -> 'a) -> 'a -> 'a
val fixpoint : ('a -> 'a -> bool) -> ('a -> 'a) -> 'a -> 'a
val curry : (('a * 'b) ->  'c) -> 'a -> 'b -> 'c
val uncurry : ('a -> 'b -> 'c) -> ('a * 'b) -> 'c
