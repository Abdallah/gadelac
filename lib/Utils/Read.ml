let bool s = match String.lowercase_ascii s with
  | "yes" | "true" | "y" | "t" -> Some true
  | "no" | "false" | "n" | "f" -> Some false
  | _ -> None

let int s = int_of_string_opt s
