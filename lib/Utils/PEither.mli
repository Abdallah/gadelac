val partition_id : ('a, 'b) Either.t list -> 'a list * 'b list
val get : ('a, 'a) Either.t -> 'a
val get_left  : 'a -> ('a, 'b) Either.t -> 'a
val get_right : 'b -> ('a, 'b) Either.t -> 'b
