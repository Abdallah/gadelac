let get = function
  | Either.Left a -> a
  | Either.Right b -> b

let get_left b = function
  | Either.Left  a -> a
  | Either.Right _ -> b

let get_right a = function
  | Either.Left  _ -> a
  | Either.Right b -> b

let partition_id l = List.partition_map (fun x -> x) l
