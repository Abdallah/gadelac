let default f b = function
  | Some a -> f a
  | None -> b

let combine f a b = match a, b with
  | None, _ | _, None -> None
  | Some aa, Some bb -> Some (f aa bb)

let lift = function
  | None -> []
  | Some l -> l
