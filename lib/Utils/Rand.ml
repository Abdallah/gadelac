let element l =
  let n = List.length l in 
  assert (n > 0);
  let i = Random.int n in
  PList.nth i l
