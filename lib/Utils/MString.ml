open Printf

type t = String.t

let to_list string =
  let l = ref [] in
  String.iter (fun char -> l := char :: !l) string;
  List.rev !l

let separate original number =
  let before = String.sub original 0 number
  and after = String.sub original (number + 1) (String.length original - number - 1) in
  (before, after)

let rec replace original char rep =
  try let n = String.index original char in
      let (before, after) = separate original n in
      replace (before ^ rep ^ after) char rep
  with Not_found -> original

let maps f string =
  let chars = to_list string in
  String.concat "" (List.map f chars)

let replace_many original map =
  let f x = try List.assoc x map with Not_found -> String.make 1 x in
  maps f original

let repeat s nb =
  let rec aux accu n =
    if n <= 0 then accu
    else aux (s ^ accu) (n - 1) in
  aux "" nb

let starts_with main sub =
  String.length main >= String.length sub && String.sub main 0 (String.length sub) = sub

let ends_with main sub =
  String.length main >= String.length sub && String.sub main (String.length main - String.length sub) (String.length sub) = sub

let escape forbidden_chars replacement =
(*  assert (List.mem replacement forbidden_chars);*)
  assert (not (List.mem 'e' forbidden_chars));
  let rep = String.make 1 replacement in
  let f i forbidden = (forbidden, sprintf "e%d%s" i rep) in
  let map = PList.mapi f 0 forbidden_chars in
  (fun x -> replace_many x map)

