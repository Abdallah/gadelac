type 'a branch = { unive : Variable.t list;
                   exist : Variable.t list;
                   equal : 'a Ast.T.term list;
                   hyps : 'a Desugared.T.literal list }
type 'a program = 'a branch list Predicate.Map.t
(*type 'a branch = 'a Ast.T.term list * 'a Desugared.T.literal Logic.t*)

val program : 'a Desugared.T.program -> Ast.T.free FirstOrder.axiom list
