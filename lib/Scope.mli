include module type of Types.SCOPE with module T = Types.SCOPE.T

val read_variable : string -> T.variable

module Print :
sig
  val var_type : T.var_type -> string
  val variable : T.variable -> string
  val affectation : T.affectation -> string
end

type t

val empty : string -> t
val term_present  : t list -> T.variable -> bool
val array_present  : t list -> T.variable list -> bool
val get_all : t -> T.affectation list

val add_cons_binding : t -> Ast.T.symbol -> T.variable * t
val get_cons_binding : t -> Ast.T.symbol -> T.variable option
val add_term_binding : t -> (Ast.T.symbol * T.variable) -> T.variable * t
val get_term_binding : t -> (Ast.T.symbol * T.variable) -> T.variable option
val add_array_binding : t -> T.variable list -> T.variable * t
val get_array_binding : t -> T.variable list -> T.variable option
val force_array_binding : t -> T.variable list -> string -> t
val add_existing_binding : t -> string -> t
