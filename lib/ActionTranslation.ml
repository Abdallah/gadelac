open Printf

let atomic (pred, terms) = match pred with
  | Predicate.Legal | Predicate.Does | Predicate.True | Predicate.Next ->
     sprintf "%s%s" (Ast.Print.predicate pred) (Utils.Print.list' "('" " " "')" Ast.Print.term terms)
  | _ -> sprintf "'%s'" (Ast.Print.atomic (pred, terms))

let literal = function
  | Desugared.T.Okay     atom -> atomic atom
  | Desugared.T.Negation atom -> sprintf "not(%s)" (atomic atom)
  | Desugared.T.Distinct _ -> assert false (*Should have been grounded*)

let clause (head, body) =
  sprintf "Rule(%s, %s)" (atomic head) (Utils.Print.list' "[" ", " "]" literal body)

module SMap = Variable.Map

let getJointActions prog =
  assert (List.for_all (fun rule -> Desugared.get_variables_clause rule = []) prog);
  let add_rule map = function
    | ((Predicate.Legal, [role; action]), _) -> SMap.update (Ast.Print.term role) (fun l -> (role, action) :: l) [] map
    | ((Predicate.Legal, _), _) -> assert false
    | _ -> map in
  let actionMap = List.fold_left add_rule SMap.empty prog in
  Utils.PList.cross_products (SMap.values actionMap)

let program prog =
  let prog = Ground.full_program prog in
  let prog = Inlining.Grounded.program (1, 1) prog in
  let jmoves = getJointActions prog in
  let rules = Utils.Print.list' "[\n" ",\n" "\n]" clause prog
  and jmoves = Utils.Print.list' "[\n" ",\n" "\n]" (Utils.Print.list' "(" ", " ")" (Utils.Print.couple' "does('" " " "')" Ast.Print.term Ast.Print.term)) jmoves in
  sprintf "actions = %s\n\nrules = %s" jmoves rules

