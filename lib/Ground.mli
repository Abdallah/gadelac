val program : Context.t list -> 'a Desugared.T.program -> 'a Desugared.T.program
val full_program : 'a Desugared.T.program -> Ast.T.ground Desugared.T.program
val count_program : int -> 'a Desugared.T.program -> 'a Desugared.T.program
val hsfc : 'a Desugared.T.program -> 'a Desugared.T.program

(** Ensures variables only stand for symbols and not compound terms. *)
val measured : 'a Desugared.T.program -> 'a Desugared.T.program
