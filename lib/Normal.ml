module A = Annotations

type one_rule = | E1 of (Ast.T.free Ast.T.atomic * Ast.T.free Ast.T.atomic)
                | E2 of (Ast.T.free Ast.T.atomic * Ast.T.free Ast.T.atomic * Ast.T.free Desugared.T.literal)

type program = one_rule list * Annotations.t

module Print =
struct
  let one_rule = function
    | E1 (t, l) -> Desugared.Print.clause (t, [Desugared.T.Okay l])
    | E2 (t, l1, l2) -> Desugared.Print.clause (t, [Desugared.T.Okay l1; l2])
  let program (rules, info) = Utils.Print.unlines one_rule rules ^ Annotations.print info
end

module RuleD = Utils.DataStructure.F (struct type t = Ast.T.free Desugared.T.clause let compare = compare let print = Desugared.Print.clause end)

let reduce_rule set (ccl, hyp_a, hyp_b, hyp_c, rest) =
  let vars_rest = Ast.get_variables_atomic ccl @ List.concat_map Desugared.get_variables_lit (hyp_c :: rest) in
  let vars = Desugared.get_variables_lit hyp_a @ Desugared.get_variables_lit hyp_b in
  let comm_vars = Utils.PList.uniques (Utils.PList.intersect vars_rest vars) in
(*  let nset, new_t = Generate.add_rule !set (Utils.PList.uniques vars) [hyp_a; hyp_b] in*) (** this line can improve performance! *)
  let nset, new_t = Generate.add_rule set comm_vars [hyp_a; hyp_b] in
  (nset, (ccl, Desugared.T.Okay new_t, hyp_c, rest))

let new_pred = Generate.predicate "new"

let reduce prog =
  let set = ref (Generate.empty new_pred) in
  let others = ref RuleD.Set.empty in
  let bigs = Stack.create () in
  List.iter (fun x -> Stack.push x bigs) prog;
  while not (Stack.is_empty bigs) do
    let to_be_reduced = Stack.pop bigs in
    let (nset, (head, newh, hyp1, body)) = reduce_rule !set to_be_reduced in
    set := nset;
    match body with
    | [] -> others := RuleD.Set.add (head, [newh; hyp1]) !others
    | hyp2 :: rest -> Stack.push (head, newh, hyp1, hyp2, rest) bigs
  done;
  RuleD.Set.elements !others @ Generate.get !set

let remove_true atom = match atom with
  | Predicate.True, [Ast.T.Fact (fct, args)] -> (Predicate.Constant fct, args)
  | Predicate.True, [Ast.T.Variable _] -> assert false
  | _ -> atom
let remove_true_lit lit = match lit with
  | Desugared.T.Okay atom -> Desugared.T.Okay (remove_true atom)
  | Desugared.T.Negation atom -> Desugared.T.Negation (remove_true atom)
  | _ -> lit
let make_rule (head, body) = match body with
  | [] -> None
  | Desugared.T.Okay t2 :: [] -> Some (E1 (head, remove_true t2))
  | Desugared.T.Okay t2 :: a :: [] -> Some (E2 (head, remove_true t2, remove_true_lit a))
  | Desugared.T.Negation _ :: _ -> assert false
  | Desugared.T.Distinct _ :: _ -> assert false
  | _ -> assert false

let remove_frame (head, body) =
  let head = match head with 
    | (Predicate.Next, [Ast.T.Fact (fct, args)]) -> (Predicate.Constant fct, args)
    | (Predicate.Next, [Ast.T.Variable _]) -> assert false
    | _ -> head in
  (head, List.map remove_true_lit body)

let is_small = function
  | (head, []) -> assert (Desugared.get_variables_lit (Desugared.T.Okay head) = []); Either.Right (head, [])
  | (head, a :: []) -> Either.Right (head, [a])
  | (head, a :: b :: []) -> Either.Right (head, [a; b])
  | (head, a :: b :: c :: rest) -> Either.Left (head, a, b, c, rest)


let annot param rules strats =
  let perms, inits, roles = A.starting rules in
  let atoms = Analysis.symbols (List.map remove_frame rules) in
  let predicates = Predicate.Set.add Predicate.Next (Analysis.predicates (List.map remove_frame rules)) in
  let frequency = Frequency.frequency param rules in
  { A.perms;
    A.inits;
    A.strats;
    A.predicates;
    A.atoms;
    A.roles;
    A.frequency; }

let make param prog : program =
  let rules = Safety.sort (Desugared.cast_program prog.Stratify.rules) in (** Safety first! *)
  let bigs, smalls = List.partition_map is_small rules in
  let rules = smalls @ reduce bigs in
  let info = annot param rules prog.Stratify.strats in
  (List.filter_map make_rule rules, info)

(* Attention aux règles avec plusieurs négation, il ne faut pas produire de sous-règle avec à la fois que des négations et des variables*)
