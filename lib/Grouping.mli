(** This module allows for a form of common subexpression elimination. *)

type param = bool

type action = Inversion.hypothesis * Ast.T.free Ast.T.atomic list
type branch = Ast.T.free Ast.T.term list * action list
type matching = branch list list

type program = matching Predicate.Map.t * Annotations.t

module Print :
sig
  val action : action -> string
  val branch : branch -> string
  val matching : matching -> string
  val program : program -> string
end

(** set [param] to [true] to enable the grouping optimization. *)
val make : param -> Inversion.program -> program
