
(** Add a [time] parameter to non-rigid predicates: replace [(true ?f)] [(true ?t ?f)], [(init ?f)] with [(true 0 ?f)], and [(next ?f)] with [(true (+ 1 ?t) ?f)]. *)
val program : 'a Desugared.T.program -> Ast.T.free Desugared.T.program
