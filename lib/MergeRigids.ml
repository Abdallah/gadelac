open Printf

let deprintf format = if false then eprintf format else ifprintf stderr format
let _ = ignore deprintf

module VD = Variable.DisjointSet

let cluster_literals perms =
  let aux_disj disj lit = VD.unions (Desugared.get_variables_lit lit) disj in
  let disj = List.fold_left aux_disj VD.empty perms in
  let aux_map (map, other) lit = match Desugared.get_variables_lit lit with
    | [] -> (map, lit :: other)
    | v :: _ -> (Variable.Map.update (VD.find v disj) (Utils.PList.cons lit) [] map, other) in
  let (map, other) = List.fold_left aux_map (Variable.Map.empty, []) perms in
  assert (other = []);
  let result = Utils.PList.map List.rev (Variable.Map.values map) in
  Utils.PList.map Safety.sort_literals result

let create_auxiliary (new_rules, heads) body = match body with
  | [] -> assert false
  | a :: [] -> (new_rules, Desugared.cast_literal a :: heads)
  | _ ->
    let vars = Utils.PList.uniques (List.concat_map Desugared.get_variables_lit body) in
    let (new_rules, head) = Generate.add_rule new_rules vars body in
    (new_rules, Desugared.T.Okay head :: heads)

let safe_variable : type a. 'b -> a Desugared.T.literal -> Variable.t list = fun perms -> function
  | Desugared.T.Distinct _  -> []
  | Desugared.T.Negation _  -> []
  | Desugared.T.Okay atomic -> if Predicate.Set.mem (fst atomic) perms then Ast.get_variables_atomic atomic else []
let safe_rigid_literal : type a. 'b -> 'c -> a Desugared.T.literal -> 'd = fun perms safe_vars -> function
  | Desugared.T.Distinct (var, term) -> List.mem var safe_vars && List.for_all (Utils.PList.memr safe_vars) (Ast.get_variables term)
  | Desugared.T.Negation atom -> Predicate.Set.mem (fst atom) perms && List.for_all (Utils.PList.memr safe_vars) (Ast.get_variables_atomic atom)
  | Desugared.T.Okay atom -> Predicate.Set.mem (fst atom) perms

let permanents_in_clause perms new_rules (head, body) =
  let rigid_variables = List.concat_map (safe_variable perms) body in
  let perm_lits, light_body = List.partition (safe_rigid_literal perms rigid_variables) body in
  let clusters = cluster_literals perm_lits in
  let new_rules, heads = List.fold_left create_auxiliary (new_rules, []) clusters in
  ((Ast.cast_atomic head, Utils.PList.append heads (List.map Desugared.cast_literal light_body)), new_rules)

let new_pred = Generate.predicate "mergeperm"

let program prog =
  let perms = Frequency.permanents prog in
  let aux (rules, existing) rule =
    let nrule, auxi = permanents_in_clause perms existing rule in
    nrule :: rules, auxi in
  let (rules, new_rules) = List.fold_left aux ([], Generate.empty new_pred) prog in
  Utils.PList.append (Generate.get new_rules) (List.rev rules)
