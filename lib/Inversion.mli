type fact = Variable of string | Free | Fact of (Ast.T.symbol * fact list)

type unify =
  { bindings : (string * string) list;
    terms : Ast.T.free Ast.T.term list;
    argument : Ast.T.predicate }

type hypothesis =
  | Okay (* (? <= ?) *)
  | IfExists of Ast.T.free Ast.T.atomic (* (? <= ?, b) *)
  | IfUnify  of unify
  | IfNot    of Ast.T.free Ast.T.atomic (* (? <= ?, not b) *)
  | IfDist   of (string * Ast.T.free Ast.T.term)

type branch = Ast.T.free Ast.T.term list * hypothesis * Ast.T.free Ast.T.atomic
type matching = branch list

(** f -> (tmc)s, i   <=>      (c <= f(t) /\ m)s *)
type program = matching Predicate.Map.t * Annotations.t

module Print :
sig
  val fact : fact -> string
  val unify : unify -> string
  val hypothesis : hypothesis -> string
  val branch : branch -> string
  val matching : matching -> string
  val program : program -> string
end

val make : unit -> Normal.program -> program
