open Printf

type param = unit
let print_param () = ""
let read_param _ = ()

type unify =
  { bindings : (Scope.T.variable * Scope.T.variable) list;
    terms : Ast.T.free Ast.T.term list }

type hypothesis =
  | Okay
  | IfExists of (Ast.T.predicate * Scope.T.variable)
  | IfUnify  of (Ast.T.predicate * unify)
  | IfNot    of (Ast.T.predicate * Scope.T.variable)
  | IfDist   of (Scope.T.variable * Scope.T.variable)

type conclusion = FunCall of Ast.T.predicate * Scope.T.variable option | Next of Ast.T.predicate * Scope.T.variable
type action = hypothesis * Scope.T.affectation list * conclusion list
type branch = Ast.T.free Ast.T.term list * Scope.T.affectation list * action list
type matching = branch list list

type program =
  { global : Scope.T.affectation list;
    funs : matching Predicate.Map.t;
    inits : Scope.T.variable list option Predicate.Map.t;
    perms : Scope.T.variable list option Predicate.Map.t;
    annot : Annotations.t }

type scopes =
  { glbal : Scope.t;
    branc : Scope.t;
    local : Scope.t }

module Print =
struct
  let affectations afs = Utils.Print.list Scope.Print.affectation afs
  let hypothesis = function
    | Okay -> ""
    | IfExists (pred, s) -> sprintf "if exists %s %s then " (Ast.Print.predicate pred) (Scope.Print.variable s)
    | IfUnify _ -> ""
    | IfNot (pred, s) -> sprintf "if absent %s %s then " (Ast.Print.predicate pred) (Scope.Print.variable s)
    | IfDist (s1, s2) -> sprintf "if %s <> %s then " (Scope.Print.variable s1) (Scope.Print.variable s2)
  let conclusion = function
    | FunCall (fct, var) -> sprintf "%s %s" (Ast.Print.predicate fct) (Utils.Print.option Scope.Print.variable var)
    | Next (fct, var) -> sprintf "next %s %s" (Ast.Print.predicate fct) (Scope.Print.variable var)
  let action (hyp, afs, cncs) = sprintf "%s %s %s" (affectations afs) (hypothesis hyp) (Utils.Print.list conclusion cncs)

  let branch (terms, afs, acts) = sprintf "(%s -> %s; %s)" (Utils.Print.list Ast.Print.term terms) (affectations afs) (Utils.Print.list action acts)
  let matching = Utils.Print.list (Utils.Print.list branch)
  let program prog = sprintf "%s\n%s\n%s\n"
    (Utils.Print.unlines Scope.Print.affectation prog.global)
    (Predicate.Map.print' "" "\n" "" (Utils.Print.couple Ast.Print.predicate matching) prog.funs)
    (Annotations.print prog.annot)
end

let (<|>) opt1 opt2 = match opt1 with
  | None -> opt2
  | Some _ -> opt1

(** Returns whether the value is already attached to some global or local variable. *)
let get_bind get_style scopes value = get_style scopes.glbal value <|> get_style scopes.branc value <|> get_style scopes.local value
let get_cons_binding = get_bind Scope.get_cons_binding
let get_term_binding = get_bind Scope.get_term_binding
let get_array_binding = get_bind Scope.get_array_binding

let get_affect_cons scopes fct =
  match get_cons_binding scopes fct with
  | Some var -> scopes, var
  | None ->
    let (var, g) = Scope.add_cons_binding scopes.glbal fct in
    ({ scopes with glbal = g }, var)

let rec get_affect_array scopes terms =
  let scopes, array = List.fold_left_map get_affect_term scopes terms in
  match get_array_binding scopes array with
  | Some var -> scopes, var
  | None ->
    if      Scope.array_present [scopes.glbal]               array then let (var, g) = Scope.add_array_binding scopes.glbal array in ({ scopes with glbal = g }, var)
    else if Scope.array_present [scopes.glbal; scopes.branc] array then let (var, b) = Scope.add_array_binding scopes.branc array in ({ scopes with branc = b }, var)
    else let (var, l) = Scope.add_array_binding scopes.local array in ({ scopes with local = l }, var)

and get_affect_atom scopes (fct, terms) =
  let scopes, arg = get_affect_array scopes terms in
  let atom = (fct, arg) in
  match get_term_binding scopes atom with
  | Some var -> scopes, var
  | None ->
    if      Scope.term_present [scopes.glbal]               arg then let (var, g) = Scope.add_term_binding scopes.glbal atom in ({ scopes with glbal = g }, var)
    else if Scope.term_present [scopes.glbal; scopes.branc] arg then let (var, b) = Scope.add_term_binding scopes.branc atom in ({ scopes with branc = b }, var)
    else let (var, l) = Scope.add_term_binding scopes.local atom in ({ scopes with local = l }, var)

and get_affect_term scopes = function
  | Ast.T.Variable s -> scopes, Scope.read_variable s
  | Ast.T.Fact (fct, []) -> get_affect_cons scopes fct
  | Ast.T.Fact atom -> get_affect_atom scopes atom

let get_affect_atomic scopes (_pred, terms) = get_affect_array scopes terms

let get_array_variable terms =
  let f accu t = match accu, t with
    | None, _ -> None
    | Some accu, Ast.T.Variable v -> Some (Scope.read_variable v :: accu)
    | Some _, Ast.T.Fact _ -> None in
  List.fold_left f (Some []) (List.rev terms)

let get_affect_unify scopes terms =
  match get_array_variable terms with
  | None -> scopes
  | Some array ->
    let new_local = Scope.force_array_binding scopes.local array "instance" in
    { scopes with local = new_local }

let unify scopes u =
  let bindings = List.map (fun (v1, v2) -> Scope.read_variable v1, Scope.read_variable v2) u.Inversion.bindings
  and terms = u.Inversion.terms in
  let u' = { bindings; terms } in
  let scopes = get_affect_unify scopes terms in
  get_affect_unify scopes terms, u'

let hypothesis _annot scopes = function
  | Inversion.Okay -> scopes, Okay
  | Inversion.IfExists atom ->
    let scopes, v = get_affect_atomic scopes atom in
    scopes, IfExists (fst atom, v)
  | Inversion.IfUnify u ->
    let scopes, new_u = unify scopes u in
    let fct = u.Inversion.argument in
    scopes, IfUnify (fct, new_u)
  | Inversion.IfNot atom ->
    let scopes, v = get_affect_atomic scopes atom in
    scopes, IfNot (fst atom, v)
  | Inversion.IfDist (string, term) ->
    let scopes, v = get_affect_term scopes term in
    scopes, IfDist (Scope.read_variable string, v)

let conclusion scopes ((fct, terms) : Ast.T.free Ast.T.atomic) =
  match fct, terms with
  | Predicate.Next, Ast.T.Fact (symb, args) :: [] ->
    let scopes, var = get_affect_array scopes args in
    (scopes, Next (Predicate.Constant symb, var))
  | Predicate.Next, _ -> assert false
  | _, [] -> (scopes, FunCall (fct, None))
  | _, _ :: _ ->
    let scopes, var = get_affect_array scopes terms in
    (scopes, FunCall (fct, Some var))

let action annot (glbal, branc) (hyp, cncs) : (Scope.t * Scope.t) * action =
  let scopes = { glbal; branc; local = Scope.empty "local" } in
  let scopes, hypo = hypothesis annot scopes hyp in
  let scopes, new_cncs = List.fold_left_map conclusion scopes cncs in
  (scopes.glbal, scopes.branc), (hypo, Scope.get_all scopes.local, new_cncs)

let make_branch_scope branch = function
  | None -> branch
  | Some [] -> branch
  | Some array -> Scope.force_array_binding branch array "_arg"

let take_branch_affects vars = List.fold_left Scope.add_existing_binding (Scope.empty "branch") vars
let branch annot global (terms, acts) =
  let arg_shortcut = get_array_variable terms in
  let branch = take_branch_affects (List.concat_map Ast.get_variables terms) in
  let branch = make_branch_scope branch arg_shortcut in
  let ((global, branch), new_acts) = List.fold_left_map (action annot) (global, branch) acts in
  global, (terms, Scope.get_all branch, new_acts)

let matching annot global (mats : Grouping.matching) = List.fold_left_map (List.fold_left_map (branch annot)) global mats

let prog global (fs, annot) = Predicate.Map.fold_map (matching annot) global fs

let is_init : Ast.T.ground Ast.T.term -> (Ast.T.predicate * Ast.T.ground Ast.T.term list) = function | Ast.T.Fact (fct, args) -> (Predicate.Constant fct, args)

let init (scopes, map) gterm =
  let (fct, args) = Ast.cast_atomic gterm in
  match args with
  | [] -> assert (not (Predicate.Map.mem fct map)); (scopes, Predicate.Map.add fct None map)
  | _ :: _ ->
    let scopes, v = get_affect_array scopes args in
    let update = function
      | None -> assert false
      | Some vs -> assert (not (List.mem v vs)); Some (v :: vs) in
    (scopes, Predicate.Map.update fct update (Some []) map)

module A = Annotations

let inits_perms global annot =
  let scopes = { glbal = global;
                 branc = Scope.empty "never";
                 local = Scope.empty "never" } in
  let ginits = GTerm.Set.mapl is_init annot.A.inits in
  let (scopes, new_is) = List.fold_left        init (scopes, Predicate.Map.empty) ginits in
  let (scopes, new_ps) = GAtomic.Set.fold_left init (scopes, Predicate.Map.empty) annot.A.perms in
  scopes.glbal, new_is, new_ps

let make () (program : Grouping.program) =
  let global = Scope.empty "global" in
  let global, funs = prog global program in
  let global, inits, perms = inits_perms global (snd program) in
  let global_affects = Scope.get_all global in
  { global = global_affects;
    funs; inits; perms;
    annot = snd program }

