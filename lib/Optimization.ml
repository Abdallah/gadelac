open Printf

type param =
  { specialize_tempo : bool ref;
    specialize_bound : bool ref;
    ground_all   : bool ref ;
    ground     : (string * int) list ref;
    ground_count : int ref;
    measured : bool ref;
    flatten : bool ref;
    specialize : (string * int) list ref;
    reachability : Reachability.param ref;
    compute_rigids : bool ref;
    merge_rigids : bool ref;
    inlining : int ref;
    frame_translation : bool ref;
    noop_move : string ref }

let default_param () =
  { specialize_tempo = ref false;
    specialize_bound = ref false;
    ground_all = ref false;
    ground = ref [];
    ground_count = ref 0;
    measured = ref false;
    flatten = ref false;
    specialize = ref [];
    reachability = ref Reachability.Paths;
    compute_rigids = ref true;
    merge_rigids = ref false;
    inlining = ref (-1);
    frame_translation = ref false;
    noop_move = ref "" }

let set_zero param =
  param.specialize_tempo := false;
  param.specialize_bound := false;
  param.ground_all := false;
  param.ground := [];
  param.ground_count := 0;
  param.measured := false;
  param.flatten := false;
  param.specialize := [];
  param.reachability := Reachability.Trivial;
  param.compute_rigids := false;
  param.merge_rigids := false;
  param.inlining := -1;
  param.frame_translation := false;
  param.noop_move := ""

let set_full param =
  param.specialize_tempo := true;
  param.specialize_bound := true;
  param.ground_all := true;
  param.measured := true;
  param.flatten := true;
  param.reachability := Reachability.Precise;
  param.compute_rigids := true;
  param.merge_rigids := true;
  param.inlining := 10

let set_level param lvl =
  if lvl <= 0 then set_zero param
  else if lvl >= 2 then set_full param
  else ()

module Print =
struct

  let param { specialize_tempo; specialize_bound; ground_all; ground; ground_count; measured; flatten; specialize; reachability; compute_rigids; merge_rigids; inlining; frame_translation; noop_move } =
    (if !specialize_tempo then "specialize_tempo" else "")
    ^ (if !specialize_bound then "specialize_bound" else "")
    ^ (if !ground_all then "ground_all" else "")
    ^ (if !ground <> [] then "ground:" ^ Utils.Print.list (Utils.Print.couple Utils.Print.string Utils.Print.int) !ground else "")
    ^ (if !ground_count <> 0 then "ground count" ^ Utils.Print.int !ground_count else "")
    ^ (if !measured then "measured" else "")
    ^ (if !flatten then "flatten" else "")
    ^ (if !specialize <> [] then "specialize:" ^ Utils.Print.list (Utils.Print.couple Utils.Print.string Utils.Print.int) !specialize else "")
    ^ (if !reachability <> Reachability.Trivial then "reachability:" ^ Reachability.print !reachability else "")
    ^ (if !compute_rigids then "compute_rigids" else "")
    ^ (if !merge_rigids then "merge_rigids" else "")
    ^ (if !inlining >= 0 then sprintf "inlining %d" !inlining else "")
    ^ (if !frame_translation then sprintf "frame_translation" else "")
    ^ (if !noop_move <> "" then sprintf "noop_move %s" !noop_move else "")

  module NaiveP = Reachability.Naive (Domain.Path)

  let print_strats arity rules =
    let next_does = (Predicate.Next, []), [Desugared.T.Okay (Predicate.Does, [])]
    and does_legal = (Predicate.Does, []), [Desugared.T.Okay (Predicate.Legal, [])]
    and does_term = (Predicate.Does, []), [Desugared.T.Okay (Predicate.Terminal, [])] in
    let scc = Predicate.SCC.tarjan (Analysis.dep_graph (next_does :: does_legal :: does_term :: rules)) in
    let print_one i pred = if arity then sprintf "(strat %d %s/%d)" i (Predicate.print pred) (Ast.arity_pred pred) else sprintf "(strat %d %s)" i (Predicate.print pred) in
    let print_class i l = Predicate.Set.unlines (print_one i) l in
    String.concat "\n" (Utils.PList.mapi print_class 0 scc)

  let program hsfc prog =
    let rules = if hsfc then Ground.hsfc prog else prog in
    let paths = NaiveP.forward false rules in
    let result = ref "" in
    let append x = result := !result ^ x in
    append (sprintf ";;;; RULES  \n\n%s\n\n" (Desugared.Print.program rules));
    append (sprintf ";;;; STRATS \n\n%s\n\n" (print_strats true rules));
    append (sprintf ";;;; PATHS  \n\n%s" (Domain.Path.print paths));
    !result
end

module TemporizationFriendly =
struct
  let instant_persistent_variables_context rule = match rule with
    | ((Predicate.Next, [Ast.T.Fact atom]), Desugared.T.Okay (Predicate.True, [Ast.T.Fact atom2]) :: Desugared.T.Distinct (var, _) :: [])
    | ((Predicate.Next, [Ast.T.Fact atom]), Desugared.T.Distinct (var, _) :: Desugared.T.Okay (Predicate.True, [Ast.T.Fact atom2]) :: []) ->
      if atom = atom2 then Context.from_variable var rule else []
    | _ -> []

  let instant_extract = List.concat_map instant_persistent_variables_context

  let special_persistent_variables_context = function
    | ((Predicate.Next, [Ast.T.Fact atom]), Desugared.T.Okay (Predicate.True, [Ast.T.Fact atom2]) :: []) when atom = atom2 -> Context.non_variables atom
    | _ -> []

  let special_extract = List.concat_map special_persistent_variables_context
end

module AllContexts =
struct
  let extract rules = Symbol.Set.maps Context.associated (Analysis.symbols rules)
end

module BoundArgs =
struct
  let rec free_term context = function
    | Ast.T.Variable _ -> false
    | Ast.T.Fact (symb, terms) ->
      (fst context = symb && Desugared.is_variable (Utils.PList.nth (snd context) terms)) || List.exists (free_term context) terms

  let free_atomic (pred, terms) = function
    | Either.Left  (pred', i) -> pred' = pred && Desugared.is_variable (Utils.PList.nth i terms)
    | Either.Right context -> List.exists (free_term context) terms

  let free_lite context = function
    | Desugared.T.Distinct (_, term) -> Utils.PEither.get_right false (Either.map_right (fun context -> free_term context term) context)
    | Desugared.T.Okay atom | Desugared.T.Negation atom -> free_atomic atom context

  let free_rule context (head, body) = free_atomic head context || List.exists (free_lite context) body

  let bound_program rules context = not (List.exists (free_rule context) rules)

  let bound rules =
    let all_contexts = Symbol.Set.maps Context.associated (Analysis.symbols rules) in
    List.filter (bound_program rules) all_contexts
end

let grounding_contexts param prog =
  let tempo = if !(param.specialize_tempo) then TemporizationFriendly.instant_extract prog else [] in
  tempo

let specialization_contexts param prog =
  let tempo = if !(param.specialize_tempo) then TemporizationFriendly.special_extract prog else []
  and bound = if !(param.specialize_bound) then BoundArgs.bound prog else [] in
  tempo @ bound

let user_contexts param prog =
  let get_context_pred pred (req_name, req_index) =
    let (name, arity) = (Ast.Print.predicate pred, Ast.arity_pred pred) in
    if name = req_name && arity > req_index then Some (Either.Left  (pred, req_index)) else None in
  let get_context_symb ((name, arity, _) as symb) (req_name, req_index) =
    if name = req_name && arity > req_index then Some (Either.Right (symb, req_index)) else None in
  let from_pred symb = List.filter_map (get_context_pred symb) param in
  let from_symb symb = List.filter_map (get_context_symb symb) param in
  Predicate.Set.maps from_pred (Analysis.predicates prog) @ Symbol.Set.maps from_symb (Analysis.symbols prog)
let user_grounding param prog =
  let contexts = user_contexts !(param.ground) prog in
  Ground.program contexts prog
let user_specialization param prog =
  let contexts = user_contexts !(param.specialize) prog in
(*  eprintf "Optimization: contexts %s\n\n%s\n\n%!" (Utils.Print.list Context.print contexts) (Desugared.Print.program (Instantiation.program prog contexts));*)
  Specialization.make contexts (Ground.program contexts prog)

let optimize name switch f prog =
  if switch then
    (eprintf "%s %!" name;
     let (prog, time) = Utils.Time.measure f prog in
     eprintf "%f\n%!" time;
     Desugared.cast_program prog)
  else Desugared.cast_program prog

let make param prog =
  let prog = optimize "unwinding" true Unwind.program prog in
  let prog = optimize "compute rigids" !(param.compute_rigids) ComputeRigids.get prog in
  let prog = optimize "merge rigids" !(param.merge_rigids) MergeRigids.program prog in
  let prog = optimize "compute rigids" (!(param.compute_rigids) && !(param.merge_rigids)) ComputeRigids.get prog in
  let prog = optimize "frame" !(param.frame_translation) (Frame.make !(param.noop_move)) prog in
  let prog = optimize "reachability" true (Reachability.make !(param.reachability)) prog in
  let prog = optimize "ground" true (Ground.program (grounding_contexts param prog)) prog in
  let prog = optimize "ground count" (!(param.ground_count) > 0) (Ground.count_program !(param.ground_count)) prog in
  let prog = optimize "ground all" !(param.ground_all) Ground.full_program prog in
  let prog = optimize "measured" !(param.measured) Ground.measured prog in
  let prog = optimize "flatten" !(param.flatten) Specialization.flatten_program prog in
  let prog = optimize "specialize" true (Specialization.make (specialization_contexts param prog)) prog in
  let prog = optimize "user ground" (!(param.ground) <> []) (user_grounding param) prog in
  let prog = optimize "user specialize" (!(param.specialize) <> []) (user_specialization param) prog in
  let prog = optimize "inline" (!(param.inlining) >= 0) (Inlining.program !(param.inlining)) prog in
  let prog = optimize "reachability" true (Reachability.make !(param.reachability)) prog in
  prog
