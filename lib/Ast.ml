open Printf

include Types.AST
open T
open Predicate

module Print =
struct
  let symbol = Symbol.print
  let predicate = Predicate.print

  let rec term : type a. a term -> string = function
    | Variable e -> "?" ^ e
    | Fact (s, []) -> (symbol s)
    | Fact (s, l) -> sprintf "(%s %s)" (symbol s) (Utils.Print.unspaces term l)
  let atomic (s, l) = match l with
    | [] -> predicate s
    | _ -> sprintf "(%s %s)" (predicate s) (Utils.Print.unspaces term l)

  let literal = function
    | Okay t -> atomic t
    | Negation t -> sprintf "(not %s)" (atomic t)
    | Distinct (t1, t2) -> sprintf "(distinct %s %s)" (term t1) (term t2)
    | Equal (t1, t2) ->  sprintf "(not (distinct %s %s))" (term t1) (term t2)

  let rule (l, rs) =
    if rs = Logic.Ok then atomic l
    else sprintf "(<= %s %s)" (atomic l) (Logic.print literal rs)

  let program prog = Utils.Print.unlines rule prog
end

let rec cast_term : type a. a term -> free term = function
  | Variable _ as x -> x
  | Fact (symb, args) -> Fact (symb, List.map cast_term args)
let cast_atomic (pred, args) = (pred, List.map cast_term args)
let rec assert_ground_term : type a. a term -> ground term = function
  | Variable _ -> assert false
  | Fact (symb, args) -> Fact (symb, List.map assert_ground_term args)
let assert_ground_atomic (pred, args) = (pred, List.map assert_ground_term args)

let variable s = Variable s
let arity (_, a, _) = a
let arity_pred = function
  | Input | Does | Goal | Legal -> 2
  | Base | Init | Negative | Next | Positive | Role | True -> 1
  | Terminal -> 0
  | Constant s -> arity s

let get_variables term =
  let rec aux : type a. string list -> a term -> string list = fun accu -> function
    | Variable v -> v :: accu
    | Fact (_, ts) -> List.fold_left aux accu ts in
  aux [] term
let get_variables_atomic (_, args) = List.concat_map get_variables args
let get_variables_lit = function
  | Okay atom | Negation atom -> Utils.PList.uniques (get_variables_atomic atom)
  | Distinct (term1, term2) | Equal (term1, term2) -> Utils.PList.uniques (get_variables term1 @ get_variables term2)

let rec replace_variable_term : type a b. (string * b term) -> a term -> a term = fun (var, term) -> function
  | Variable v -> if v = var then cast_term term else Variable v
  | Fact (symb, terms) -> Fact (symb, List.map (replace_variable_term (var, term)) terms)
let replace_variable_atomic rep (pred, args) = (pred, List.map (replace_variable_term rep) args)
let replace_variable_lit rep = function
  | Okay atom -> Okay (replace_variable_atomic rep atom)
  | Negation atom -> Negation (replace_variable_atomic rep atom)
  | Distinct (term1, term2) -> Distinct (replace_variable_term rep term1, replace_variable_term rep term2)
  | Equal (term1, term2) -> Equal (replace_variable_term rep term1, replace_variable_term rep term2)

let ground atom = get_variables_atomic atom = []
let negate = function
  | Distinct pair -> Equal pair
  | Equal pair -> Distinct pair
  | Negation atom -> Okay atom
  | Okay atom -> Negation atom

let rec depth_term : type a. a term -> int = function
  | Variable _ -> 0
  | Fact (_, args) -> 1 + List.fold_left max 0 (List.map depth_term args)
let depth (_, args) = List.fold_left max 0 (List.map depth_term args)

let rec abstract : type a. (unit -> string) -> int -> a term -> free term = fun var depth term ->
  if depth <= 0 then Variable (var ())
  else match term with
  | Variable _ -> assert false
  | Fact (symb, args) -> Fact (symb, Utils.PList.map (abstract var (depth - 1)) args)

module S =
struct
  type term = Fact of (string * term list) | Variable of string
  type atomic = (string * term list)
  type literal = Positive of atomic | Negative of atomic
  type rule = (atomic * literal Logic.t)
  type program = rule list
end

let predicates = ["does"; "goal"; "init"; "legal"; "next"; "role"; "terminal"; "true" ]
let predicate pred arity = match (String.lowercase_ascii pred, arity) with
  | "input", 2 -> Input
  | "base", 1 -> Base
  | "does", 2 -> Does
  | "goal", 2 -> Goal
  | "init", 1 -> Init
  | "legal", 2 -> Legal
  | "next", 1 -> Next
(*  | "neg", 1 -> Negative
  | "pos", 1 -> Positive*)
  | "role", 1 -> Role
  | "terminal", 0 -> Terminal
  | "true", 1 -> True
  | _ -> if List.mem pred predicates then eprintf "Warning: Predicate %s used with surprising arity: %d\n" pred arity;
    Constant (pred, arity, arity)

let rec term = function
  | S.Fact (symbol, l) -> if List.mem symbol predicates then eprintf "Warning: Symbol %s named after a predicate\n" symbol;
    let arity = List.length l in
    Fact ((symbol, arity, arity), Utils.PList.map term l)
  | S.Variable v -> Variable v

let atomic (pred, terms) = (predicate pred (List.length terms), Utils.PList.map term terms)
let literal = function
  | S.Positive (pred, [t1; t2]) when String.lowercase_ascii pred = "distinct" -> Distinct (term t1, term t2)
  | S.Negative (pred, [t1; t2]) when String.lowercase_ascii pred = "distinct" -> Equal    (term t1, term t2)
  | S.Positive (pred, _) | S.Negative (pred, _) when String.lowercase_ascii pred = "distinct" -> failwith "distinct used with the wrong number of arguments."
  | S.Positive atom -> Okay     (atomic atom)
  | S.Negative atom -> Negation (atomic atom)
let rule (head, body) = (atomic head, Logic.map literal body)
let program prog = Utils.PList.map rule prog
