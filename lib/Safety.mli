(** A clause is safe if all variables occur in at least one positive literal. *)

val program : 'a Desugared.T.program -> (string * 'a Desugared.T.clause) option

(** A clause is sorted safe if all variables occur in at least one positive literal before occuring in any non positive literal. *)
val sorted_clause : 'a Desugared.T.clause -> bool
val sorted : 'a Desugared.T.program -> bool

val sort_literals : 'a Desugared.T.literal list -> 'a Desugared.T.literal list
val sort_clause : 'a Desugared.T.clause -> 'a Desugared.T.clause
val sort : 'a Desugared.T.program -> 'a Desugared.T.program
