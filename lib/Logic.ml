open Printf

type 'a t = And of ('a t * 'a t) | Or of ('a t * 'a t) | Literal of 'a | Ok

let rec print pr = function
  | And (t1, t2) -> sprintf "(and %s %s)" (print pr t1) (print pr t2)
  | Or  (t1, t2) -> sprintf "(or  %s %s)" (print pr t1) (print pr t2)
  | Literal a -> pr a
  | Ok -> "T"

let rec map f = function
  | And (t1, t2) -> And (map f t1, map f t2)
  | Or  (t1, t2) -> Or  (map f t1, map f t2)
  | Literal a -> Literal (f a)
  | Ok -> Ok

let rec distrib = function
  | And (p, Or (q, r)) -> Or (distrib (And (p, q)), distrib (And (p, r)))
  | And (Or (p, q), r) -> Or (distrib (And (p, r)), distrib (And (q, r)))
  | _ as fm -> fm
	
let rec rawdnf = function
  | And (p, q) -> distrib (And (rawdnf p, rawdnf q))
  | Or (p, q) -> Or (rawdnf p, rawdnf q)
  | _ as fm -> fm

let rec flatten_and = function
  | Or _ -> assert false
  | And (p, q) -> let p = flatten_and p and q = flatten_and q in p @ q
  | Literal s -> [s]
  | Ok -> []
	
let rec flatten_or = function
  | And _ as form -> [flatten_and form]
  | Literal s -> [[s]]
  | Or (p, q) -> let p = flatten_or p and q = flatten_or q in p @ q
  | Ok -> [[]]
      
let dnf expr_pre = flatten_or (rawdnf expr_pre)
  
let nest f = function
  | [] -> assert false
  | hd :: r -> List.fold_left f hd r

let nest_and = function
  | [] -> Ok
  | hd :: tl -> List.fold_left (fun x y -> And (x, y)) hd tl
let nest_or l = nest (fun x y -> Or  (x, y)) l
