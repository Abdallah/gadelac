open Printf

type 'a substitution = 'a Ast.T.term Variable.Map.t
type 'a t = 'a substitution option

let print_sub x = Variable.Map.print Ast.Print.term x
let print x = Utils.Print.option print_sub x

let empty_sub = Variable.Map.empty
let empty = Some empty_sub

let apply_var sub var = match Variable.Map.find_opt var sub with
  | None -> Ast.T.Variable var | Some term -> Ast.cast_term term
let rec apply_term : type a b. a substitution -> b Ast.T.term -> b Ast.T.term = fun sub -> function
  | Ast.T.Variable var -> apply_var sub var
  | Ast.T.Fact (symb, terms) -> Ast.T.Fact (symb, List.map (apply_term sub) terms)
let apply_atomic sub (pred, args) = (pred, List.map (apply_term sub) args)
let apply_literal sub = function
  | Ast.T.Okay atom -> Ast.T.Okay (apply_atomic sub atom)
  | Ast.T.Negation atom -> Ast.T.Negation (apply_atomic sub atom)
  | Ast.T.Distinct (t1, t2) -> Ast.T.Distinct (apply_term sub t1, apply_term sub t2)
  | Ast.T.Equal (t1, t2) -> Ast.T.Equal (apply_term sub t1, apply_term sub t2)
let equalities x = Option.map Variable.Map.bindings x

let codomains sub =
  let dom, terms = Utils.PList.split (Variable.Map.bindings sub) in
  let free_vars = List.concat_map Ast.get_variables terms in
  let cod = Utils.PList.uniques free_vars in
  assert (Utils.PList.intersect dom cod = []);
  assert (Utils.PList.no_doubles dom);
  dom, cod

let compose_aux tau theta =
  let _dom1, _cod1 = codomains tau
  and _dom2, _cod2 = codomains theta in
  if (Utils.PList.intersect _dom1 _dom2 <> []) then eprintf "Unification:%s %s\n" (print_sub tau) (print_sub theta);
  assert (Utils.PList.intersect _dom1 _dom2 = []);
  assert (Utils.PList.intersect _dom1 _cod2 = []);
  let add_one key term accu = Variable.Map.add key (apply_term theta term) accu in
  Variable.Map.fold add_one tau theta
let compose x = Utils.Option.combine compose_aux x

let rec unify_sequence_aux : type a. a Ast.T.term list -> a Ast.T.term list -> a t = fun l1 l2 ->
  assert (List.length l1 = List.length l2);
  let aux t1 t2 = function
    | None -> None
    | Some tau ->
      let theta = unify (apply_term tau t1) (apply_term tau t2) in
      Option.map (compose_aux tau) theta in
  List.fold_right2 aux l1 l2 empty
and unify : type a. a Ast.T.term -> a Ast.T.term -> a t = fun term1 term2 -> match term1, term2 with
  | Ast.T.Variable v1, Ast.T.Variable v2 when (v1 = v2) -> empty
  | Ast.T.Variable var, term -> if List.mem var (Ast.get_variables term) then None else Some (Variable.Map.singleton var term)
  | term, Ast.T.Variable var -> if List.mem var (Ast.get_variables term) then None else Some (Variable.Map.singleton var term)
  | Ast.T.Fact (f1,  _), Ast.T.Fact (f2,  _) when f1 <> f2 -> None
  | Ast.T.Fact (f1, l1), Ast.T.Fact (f2, l2) ->
     assert (f1 = f2 && List.length l1 = List.length l2);
     let u = unify_sequence_aux l1 l2 in u

let unify_sequence l1 l2 =
  assert (List.length l1 = List.length l2);
  unify_sequence_aux l1 l2

let add_equality sub (t1, t2) =
  let term1 = apply_term sub t1
  and term2 = apply_term sub t2 in
  compose (Some sub) (unify term1 term2)
