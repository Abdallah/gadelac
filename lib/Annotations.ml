open Printf

type t =
  { perms : GAtomic.Set.t;
    inits : GTerm.Set.t;
    strats : Ast.T.predicate list * Ast.T.predicate list;
    predicates : Predicate.Set.t;
    atoms : Symbol.Set.t;
    roles : Symbol.Set.t;
    frequency : Frequency.t Predicate.Map.t; }

let print annot =
  let perms = GAtomic.Set.print annot.perms in
  let inits = GTerm.Set.print annot.inits in
  let strats = Utils.Print.couple (Utils.Print.list Ast.Print.predicate) (Utils.Print.list Ast.Print.predicate) annot.strats in
  let predicates = Predicate.Set.print annot.predicates in
  let atoms = Symbol.Set.print annot.atoms in
  let roles = Symbol.Set.print annot.roles in
  let frequency = Predicate.Map.print Frequency.print annot.frequency in
  sprintf "perms: %s\ninits: %s\n\nstrats:\n%s\n\npredicates:\n%s\n\natoms:\n%s\n\nroles:\n%s\n\nfrequency:\n%s\n\n"
    perms inits strats predicates atoms roles frequency

let starting prog =
  let perm_rules = ComputeRigids.get prog in
  let make_perm = function
    | (head, []) -> assert (Desugared.get_variables_lit (Desugared.T.Okay head) = []); Some (Desugared.ground_clause (head, []))
    | (_, _ :: _) -> None in
  let perms = List.filter_map make_perm perm_rules in
  let is_init_role = function
    | (Predicate.Init, [gterm]) -> Either.Right gterm
    | (Predicate.Init, _) -> assert false
    | (Predicate.Role, (Ast.T.Fact (x, []) :: [])) -> Either.Left (Either.Right x)
    | (Predicate.Role, _) -> assert false
    | atom -> Either.Left (Either.Left atom) in
  let atoms, gterms = List.partition_map is_init_role perms in
  let atoms, roles  = Utils.PEither.partition_id atoms in
  GAtomic.Set.of_list atoms, GTerm.Set.of_list gterms, Symbol.Set.of_list roles
