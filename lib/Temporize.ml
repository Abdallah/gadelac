open Printf

let curr time = Ast.T.Variable time
(*let zero = Ast.T.Fact (("0", 0), [])
let next time = Ast.T.Fact (("+", 2), [Ast.T.Fact (("1", 0), []); curr time])*)
let zero = Ast.T.Fact (("__gad_zero", 0, 0), [])
let next time = Ast.T.Fact (("__gad_incr", 1, 1), [curr time])

let time_pred = Predicate.Constant ("time", 0, 0)
let current time = Desugared.T.Okay (time_pred, [curr time])

let time_var prog =
  let vars = List.concat_map Desugared.get_variables_clause prog in
  let vars = Variable.Set.of_list vars in
  let base = "t" in
  if not (Variable.Set.mem base vars) then base
  else
    let i = ref 0 in
    while Variable.Set.mem (sprintf "%s%d" base !i) vars do incr i done;
    sprintf "%s%d" base !i

let atomic = fun time perms (pred, args) ->
  let args = List.map Ast.cast_term args in
  match pred with
  | Predicate.Next -> (Predicate.True, next time :: args)
  | Predicate.Init -> (Predicate.True, zero :: args)
  | _ when Predicate.Set.mem pred perms -> (pred, args)
  | _ -> (pred, curr time :: args)

let literal : type a. string -> Predicate.Set.t -> a Desugared.T.literal -> Ast.T.free Desugared.T.literal = fun time perms -> function
  | Desugared.T.Okay     atom        -> Desugared.T.Okay     (atomic time perms atom)
  | Desugared.T.Negation atom        -> Desugared.T.Negation (atomic time perms atom)
  | Desugared.T.Distinct (var, term) -> Desugared.T.Distinct (var, Ast.cast_term term)

let clause time perms (head, body) =
  let body = Utils.PList.map (literal time perms) body in
  let body = if Predicate.Set.mem (fst head) perms then body else current time :: body in
  (atomic time perms head, body)

let program prog =
  let symbols = Analysis.symbols prog in
  if Symbol.Set.exists (fun (name, _, _) -> name = "+") symbols then failwith "Can't do a Time transformation with an existing \"+\" symbol";
  let predicates = Analysis.predicates prog in
  if Predicate.Set.mem time_pred predicates then failwith "Can't do a Time transformation with an existing \"time\" predicate";
  let perms = Frequency.permanents prog in
  let perms = Predicate.Set.remove Predicate.Legal perms in (** in case all the moves are rigidely legals *)
  let perms = Predicate.Set.remove Predicate.Goal perms in
  let time = time_var prog in
  Utils.PList.map (clause time perms) prog
