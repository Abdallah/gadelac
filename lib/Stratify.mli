type 'a program =
  { rules : 'a Desugared.T.program;
    strats : (Ast.T.predicate list * Ast.T.predicate list) }

val print : 'a program -> string
val strat_terms : 'a Desugared.T.program -> int Predicate.Map.t
val make : Frequency.param -> 'a Desugared.T.program -> 'a program
