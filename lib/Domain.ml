open Printf

let deprintf format = if false then eprintf format else ifprintf stderr format

module IntMap = Utils.Int.Map

module type S =
sig
  type t
  type substitutions
  val empty : t
  val unit : substitutions

  val equal : t -> t -> bool
  val print : t -> string
  val print_subs : substitutions -> string
  val unify : 'a Ast.T.atomic -> t -> substitutions
  val compose : t -> substitutions -> 'a Ast.T.atomic -> substitutions
  val neg_compose : t -> substitutions -> 'a Ast.T.atomic -> substitutions
  val dist_compose : substitutions -> string -> 'a Ast.T.term -> substitutions
  val add_subs : substitutions -> 'a Ast.T.atomic -> t -> t
  val consistent : substitutions -> bool
end

module Exact =
struct
  module Substitution =
  struct
    let compose s1 s2 = Unification.compose (Some s1) (Some s2)
  end

  module ArgumentsD = Utils.DataStructure.F (struct type t = Ast.T.free Ast.T.term list let compare = compare let print = Utils.Print.list Ast.Print.term end)
  module ArgSet = ArgumentsD.Set
  type t = ArgSet.t Predicate.Map.t
  type substitutions = Ast.T.free Unification.substitution list

  let empty = Predicate.Map.empty
  let unit = [Unification.empty_sub]
  let find pred domain = Predicate.Map.find_default pred ArgSet.empty domain
  let mem (pred, args) domain = ArgSet.mem (List.map Ast.cast_term args) (find pred domain)
  let equal = Predicate.Map.equal ArgSet.equal
  let print = Predicate.Map.print ArgSet.print
  let print_subs = Utils.Print.list (Variable.Map.print Ast.Print.term)
  let unify (pred, args) domain = ArgSet.filter_mapl (Unification.unify_sequence (List.map Ast.cast_term args)) (find pred domain)
  let compose domain subs atom =
    let one sub = List.filter_map (Substitution.compose sub) (unify (Unification.apply_atomic sub atom) domain) in
    let res = List.concat_map one subs in
    deprintf "Domain: %d %s %s %s\n%!" (List.length subs) (print_subs subs) (Ast.Print.atomic atom) (print_subs res);
    res
  let neg_compose domain subs atom =
    let is_ok sub = not (mem (Unification.apply_atomic sub atom) domain) in
    List.filter is_ok subs
  let dist_compose subs var term =
    let one sub =
      assert (List.for_all (Variable.Map.memr sub) (var :: Ast.get_variables term));
      Unification.apply_var sub var <> Unification.apply_term sub (Ast.cast_term term) in
    List.filter one subs
  let add_subs subs atom domain =
    let aux sub =
      let (pred, args) = Unification.apply_atomic sub atom in
      assert (Ast.ground (pred, args));
      Predicate.Map.update pred (ArgSet.add (List.map Ast.cast_term args)) ArgSet.empty in
    Utils.PList.fold_right aux subs domain

  let values domain =
    let aux_domain pred = ArgSet.fold (fun args accu -> (pred, args) :: accu) in
    Predicate.Map.fold aux_domain domain []
  let consistent subs = subs <> []
end

module Path =
struct
  type suffix = (Ast.T.symbol * int) list * Ast.T.symbol

  let print_suffix (path, tail) =
    let print_element (symb, i) = sprintf "%s/%d %d " (Ast.Print.symbol symb) (Ast.arity symb) i in
    sprintf "%s%s/%d" (Utils.Print.list' "" "" "" print_element path) (Symbol.print tail) (Ast.arity tail)
  module SuffixD = Utils.DataStructure.F (struct type t = suffix let compare = compare let print = print_suffix end)
  module Paths = SuffixD.Set
  let print_imap (pred, map) =
    let print_path i suffix = sprintf "(arg %s/%d %d %s)" (Ast.Print.predicate pred) (Ast.arity_pred pred) i (print_suffix suffix) in
    let pr_aux (i, paths) = Paths.unlines (print_path i) paths in
    IntMap.print' "" "\n" "" pr_aux map

  type t = Predicate.Set.t * Paths.t IntMap.t Predicate.Map.t
  type substitutions = Paths.t Variable.Map.t option
  let print ((preds, paths) : t) =
    sprintf "%s\n%s"
      (Predicate.Set.print' "" "\n" "" (fun pred -> sprintf "(arg %s/%d)" (Predicate.print pred) (Ast.arity_pred pred)) preds)
      (Predicate.Map.print' "" "\n" "" print_imap paths)
  let print_subs = Utils.Print.option (Variable.Map.print Paths.print)
  let empty = (Predicate.Set.empty, Predicate.Map.empty)
  let unit = Some Variable.Map.empty

  let equal (preds1, paths1) (preds2, paths2) = Predicate.Set.equal preds1 preds2 && Predicate.Map.equal (IntMap.equal Paths.equal) paths1 paths2
  let update pred i f = Predicate.Map.update pred (IntMap.update i f Paths.empty) IntMap.empty

  let fetch_paths (i, path) imap =
    let rec prefix partial (full, symb) = match partial, full with
      | [], _ -> Some (full, symb)
      | _, [] -> None
      | h1 :: r1, h2 :: r2 -> if h1 <> h2 then None else prefix r1 (r2, symb) in
    Paths.filter_map (prefix path) (IntMap.find_default i Paths.empty imap)

  (** Returns the opened and closed paths of an atomic argument *)
  let abstract (_, args) =
    let opened = ref [] in
    let closed = ref [] in
    let rev_path end_i rpath =
      let aux (i, path) (symb, j) = (j, (symb, i) :: path) in
      List.fold_left aux (end_i, []) rpath in
    let rec aux : type a. (Ast.T.symbol * int) list -> int -> a Ast.T.term -> unit = fun path i -> function
      | Ast.T.Variable var ->    let (star_i, rpath) = rev_path i path in opened := (var, (star_i, rpath)) :: !opened
      | Ast.T.Fact (symb, []) -> let (star_i, rpath) = rev_path i path in closed := (star_i, (rpath, symb)) :: !closed
      | Ast.T.Fact (symb, args) -> Utils.PList.iteri (aux ((symb, i) :: path)) 0 args in
    Utils.PList.iteri (aux []) 0 args;
    (!opened, !closed)

  let compose_aux atomic domain sub =
    let opened, closed = abstract atomic in
    if Predicate.Set.mem (fst atomic) (fst domain) then (assert (snd atomic = []); Some sub)
    else if snd atomic = [] then None
    else
      let imap = Predicate.Map.find_default (fst atomic) IntMap.empty (snd domain) in
      let closed_mem (i, path) = Paths.mem path (IntMap.find_default i Paths.empty imap) in
      let open_update sub (var, path) =
        let dom_paths = fetch_paths path imap in
        match Variable.Map.find_opt var sub with
        | None -> Variable.Map.add var dom_paths sub
        | Some old_paths -> Variable.Map.add var (Paths.inter dom_paths old_paths) sub in
      let new_sub = List.fold_left open_update sub opened in
      if not (List.for_all closed_mem closed) || List.mem Paths.empty (Variable.Map.values new_sub) then None
      else Some new_sub
  let compose domain sub atomic = Option.bind sub (compose_aux atomic domain)
  let dist_compose _ _ _ = assert false
  let neg_compose _ _ _ = assert false
  let unify atom domain = compose domain unit (Ast.cast_atomic atom)

  let interpret (fcts, map) = function
    | ([], fct) -> (fct :: fcts, map)
    | ((fct, i) :: rest, tail) -> (fcts, Symbol.Map.update fct (IntMap.update i (Utils.PList.cons (rest, tail)) []) IntMap.empty map)

  let rec make_terms paths =
    let fcts, map = List.fold_left interpret ([], Symbol.Map.empty) paths in
    let fcts = Utils.PList.map (fun fct -> Ast.T.Fact (fct, [])) fcts in
    let aux_symb symb imap accu =
      let args = List.sort compare (IntMap.bindings imap) in
      let args = Utils.PList.map make_terms (Utils.PList.map snd args) in
      let args = Utils.PList.cross_products args in
      Utils.PList.append (Utils.PList.map (fun args -> Ast.T.Fact (symb, args)) args) accu in
    Symbol.Map.fold aux_symb map fcts

  let add_subs subs ((pred, args) as atomic) ((preds, paths) as domain) = match subs, args with
    | None, _ -> domain
    | Some _, [] -> (Predicate.Set.add pred preds, paths)
    | Some sub, _ :: _ ->
      let opened, closed = abstract atomic in
        let up_open map (var, (i, path)) = assert (Variable.Map.mem var sub);
          let aux (set_path, symb) map = update pred i (Paths.add (path @ set_path, symb)) map in
          Paths.fold aux (Variable.Map.find var sub) map in
        let up_clos map (i, path) = update pred i (Paths.add path) map in
      (preds, List.fold_left up_clos (List.fold_left up_open paths opened) closed)

  let consistent subs = subs <> None

  let suffix_to_symb (l, end_symb) = match l with
    | [] -> end_symb
    | (symb, _) :: _ -> symb
  let develop subs = Option.map (Variable.Map.map (Paths.mapl suffix_to_symb)) subs
end

module Graph =
struct
  let print_intmap map =
    let print_set = Symbol.Set.print' "(set " " " ")" Symbol.Set.print_elt in
    let binds = List.sort compare (IntMap.bindings map) in
    let vals = Utils.PList.map snd binds in
    Utils.Print.unspaces print_set vals

  let print_pred_map = Predicate.Map.print'   "" "\n" "" (Utils.Print.couple' "(domain_p " " " ")" Predicate.Map.print_key   print_intmap)
  let print_symb_map = Symbol.Map.print' "" "\n" "" (Utils.Print.couple' "(domain_s " " " ")" Symbol.Map.print_key print_intmap)

  type t = Symbol.Set.t IntMap.t Predicate.Map.t * Symbol.Set.t IntMap.t Symbol.Map.t
  type substitutions = Symbol.Set.t Variable.Map.t option

  let print (pred_map, symb_map) = sprintf "%s\n\n%s" (print_pred_map pred_map) (print_symb_map symb_map)
  let print_subs = Utils.Print.option (Variable.Map.print Symbol.Set.print)

  let empty = Predicate.Map.empty, Symbol.Map.empty
  let unit = Some Variable.Map.empty

  let equal (pmap1, smap1) (pmap2, smap2) = Predicate.Map.equal (IntMap.equal Symbol.Set.equal) pmap1 pmap2 && Symbol.Map.equal (IntMap.equal Symbol.Set.equal) smap1 smap2

  let add_pred (pred, i, nsymb) = Predicate.Map.update   pred (IntMap.update i (Symbol.Set.add nsymb) Symbol.Set.empty) IntMap.empty
  let add_symb (symb, i, nsymb) = Symbol.Map.update symb (IntMap.update i (Symbol.Set.add nsymb) Symbol.Set.empty) IntMap.empty
  let find_pred (pmap, _) pred i = IntMap.find_default i Symbol.Set.empty (Predicate.Map.find_default   pred IntMap.empty pmap)
  let find_symb (_, smap) symb i = IntMap.find_default i Symbol.Set.empty (Symbol.Map.find_default symb IntMap.empty smap)
  let mem_pred domain (pred, i, nsymb) = Symbol.Set.mem nsymb (find_pred domain pred i)
  let mem_symb domain (symb, i, nsymb) = Symbol.Set.mem nsymb (find_symb domain symb i)

  let abstract (pred, args) =
    let (open_p, open_s, clos_p, clos_s) = (ref [], ref [], ref [], ref []) in
    let rec aux_s : type a. Ast.T.symbol -> int -> a Ast.T.term -> unit = fun osym i -> function
      | Ast.T.Variable var ->      open_s := (osym, i,  var) :: !open_s
      | Ast.T.Fact (symb, args) -> clos_s := (osym, i, symb) :: !clos_s; Utils.PList.iteri (aux_s symb) 0 args in
    let aux_p : type a. int -> a Ast.T.term -> unit = fun i -> function
      | Ast.T.Variable var ->      open_p := (pred, i,  var) :: !open_p
      | Ast.T.Fact (symb, args) -> clos_p := (pred, i, symb) :: !clos_p; Utils.PList.iteri (aux_s symb) 0 args in
    Utils.PList.iteri aux_p 0 args;
    (!open_p, !open_s, !clos_p, !clos_s)

  let compose_aux atomic domain sub =
    let (open_p, open_s, clos_p, clos_s) = abstract atomic in
    let preds (pred, i, var) = (var, find_pred domain pred i)
    and symbs (symb, i, var) = (var, find_symb domain symb i) in
    let update sub (var, dom_set) = match Variable.Map.find_opt var sub with
      | None -> Variable.Map.add var dom_set sub
      | Some old_set -> Variable.Map.add var (Symbol.Set.inter dom_set old_set) sub in
    let new_sub = List.fold_left update sub (Utils.PList.append (Utils.PList.map preds open_p) (Utils.PList.map symbs open_s)) in
    if not (List.for_all (mem_pred domain) clos_p) || not (List.for_all (mem_symb domain) clos_s) || List.mem Symbol.Set.empty (Variable.Map.values new_sub) then None
    else Some new_sub
  let compose domain sub atomic = Option.bind sub (compose_aux atomic domain)
  let dist_compose _ _ _ = assert false
  let neg_compose _ _ _ = assert false
  let unify atom domain = compose domain unit (Ast.cast_atomic atom)

  let add_subs subs atom (pmap, smap) = match subs with
    | None -> (pmap, smap)
    | Some sub ->
      let (open_p, open_s, clos_p, clos_s) = abstract atom in
      let update_p (pred, i, var) = assert (Variable.Map.mem var sub); Symbol.Set.fold (fun symb -> add_pred (pred, i, symb)) (Variable.Map.find var sub)
      and update_s (osym, i, var) = assert (Variable.Map.mem var sub); Symbol.Set.fold (fun symb -> add_symb (osym, i, symb)) (Variable.Map.find var sub) in
      (Utils.PList.fold_right update_p open_p (Utils.PList.fold_right add_pred clos_p pmap),
       Utils.PList.fold_right update_s open_s (Utils.PList.fold_right add_symb clos_s smap))

  let consistent subs = subs <> None
end
