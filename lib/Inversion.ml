open Printf

module A = Annotations

type literal =
  | One    of Ast.T.free Ast.T.atomic
  | Two    of (Ast.T.free Ast.T.atomic * Ast.T.free Ast.T.atomic)
  | OkBut  of (Ast.T.free Ast.T.atomic * Ast.T.free Ast.T.atomic)
  | OkDist of (Ast.T.free Ast.T.atomic * string * Ast.T.free Ast.T.term)

type fact = Variable of string | Free | Fact of (Ast.T.symbol * fact list)

let rec unboundToFact_aux bounds unbounds = function
  | Ast.T.Variable x ->
    assert (List.mem x bounds || not (List.mem x unbounds));
    if List.mem x bounds then (unbounds, Variable x) else (unbounds, Free)
  | Ast.T.Fact (f, ts) ->
    let aux (accu, ts) t =
      let (u, t') = unboundToFact_aux bounds accu t in
      (u, t' :: ts) in
    let (u, terms) = List.fold_left aux (unbounds, []) ts in
    (u, Fact (f, List.rev terms))

let get_vars terms = Utils.PList.uniques (List.concat_map Ast.get_variables terms)

type unify =
  { bindings : (string * string) list;
    terms : Ast.T.free Ast.T.term list;
    argument : Ast.T.predicate }

type hypothesis =
  | Okay
  | IfExists of Ast.T.free Ast.T.atomic
  | IfUnify of unify
  | IfNot of Ast.T.free Ast.T.atomic
  | IfDist of (string * Ast.T.free Ast.T.term)

type branch = Ast.T.free Ast.T.term list * hypothesis * Ast.T.free Ast.T.atomic
type matching = branch list

type program = matching Predicate.Map.t * A.t

module Print =
struct

  let rec fact = function
    | Free -> "??"
    | Variable x -> sprintf "?%s" x
    | Fact (symb, []) -> Ast.Print.symbol symb
    | Fact (symb, l) ->  sprintf "(%s %s)" (Ast.Print.symbol symb) (Utils.Print.unspaces fact l)

  let atomic = function
    | (pred, []) -> Ast.Print.predicate pred
    | (pred, l) ->  sprintf "(%s %s)" (Ast.Print.predicate pred) (Utils.Print.unspaces fact l)

  let bindings bindings = match bindings with
    | [] -> ""
    | _ -> Utils.Print.list' "(" ", " "), " (fun (a, b) -> sprintf "?%s = ?%s" a b) bindings

  let unify uni =
    let bindings = bindings uni.bindings in
    let terms = Utils.Print.list Ast.Print.term uni.terms in
    let argument = Ast.Print.predicate uni.argument in
    sprintf "upattern:%s, %sterms:%s" argument bindings terms

  let hypothesis = function
    | Okay -> ""
    | IfExists at -> sprintf "if exists %s then " (Ast.Print.atomic at)
    | IfUnify uni -> sprintf "if (unify %s) then " (unify uni)
    | IfNot at -> sprintf "if absent %s then " (Ast.Print.atomic at)
    | IfDist (t1, t2) -> sprintf "if %s <> %s then " t1 (Ast.Print.term t2)
  let branch (terms, hypo, ccl) = sprintf "(%s -> %s%s)" (Utils.Print.list Ast.Print.term terms) (hypothesis hypo) (Ast.Print.atomic ccl)

  let matching = Utils.Print.list' "" "\n  " "" branch

  let program (prog, annot) =
    let comment = Utils.MString.repeat ";" 30 in
    let map = Predicate.Map.print' "" "\n\n" "" (Utils.Print.couple Ast.Print.predicate matching) prog in
    sprintf "%s\n\n%s\n\n%s" map comment (A.print annot)
end

let new_var = 
  let counter = ref 0 in
  (fun x -> incr counter; sprintf "%s__%d" x !counter)

let rec unbound_aux nb_free vars bindings t : (int * string list * (string * string) list * Ast.T.free Ast.T.term) = match t with
  | Ast.T.Variable x ->
    if List.mem x vars
    then let xx = new_var x in (nb_free, xx :: vars, (xx, x) :: bindings, Ast.T.Variable xx)
    else (1 + nb_free, x :: vars, bindings, t)
  | Ast.T.Fact (f, ts) ->
    let step (nb, var, binds, lt) tt =
      let (nb', var', binds', t) = unbound_aux nb var binds tt in
      (nb', var', binds', t :: lt) in
    let new_nb_free, new_vars, new_bindings, terms = List.fold_left step (nb_free, vars, bindings, []) ts in
    (new_nb_free, new_vars, new_bindings, Ast.T.Fact (f, List.rev terms))

let unbound bound_vars terms = 
  let step (nb, var, binds, lt) tt =
    let (nb', var', binds', t) = unbound_aux nb var binds tt in
    (nb', var', binds', t :: lt) in
  let new_nb_free, new_vars, new_bindings, new_terms = List.fold_left step (0, bound_vars, [], []) terms in
  (new_nb_free, new_bindings, List.rev new_terms)

let transform_e2 a = function
  | Desugared.T.Okay b -> Two (a, b)
  | Desugared.T.Negation b -> OkBut (a, b)
  | Desugared.T.Distinct (s, t) -> OkDist (a, s, t)

let transform = function
  | Normal.E1 (ccl, e1) -> (One e1, ccl)
  | Normal.E2 (ccl, e1, e2) -> (transform_e2 e1 e2, ccl)

let double_rule e = match e with
  | Normal.E2 (ccl, a, Desugared.T.Okay b) -> [e; Normal.E2 (ccl, b, Desugared.T.Okay a)]
  | _ -> [e]
let double_rules eprog = List.concat_map double_rule eprog
let to_literals eprog = Utils.PList.map transform (double_rules eprog)

let exists_unify context atomic =
  let bound_vars = get_vars context in
  let nb, bindings, terms = unbound bound_vars (snd atomic) in
(*  eprintf "Inversion: %s %s %d %s %s\n" (Utils.Print.list Utils.Print.string bound_vars) (Ast.Print.atomic atomic) nb (Utils.Print.list (Utils.Print.couple Utils.Print.string Utils.Print.string) bindings) (Utils.Print.list Ast.Print.term terms);*)
  if nb = 0 then IfExists atomic
  else IfUnify { bindings; terms; argument = fst atomic }

let get_rules literals =
  let map = ref Predicate.Map.empty in
  let get f = try Predicate.Map.find f !map with | Not_found -> [] in
  let push f x = let old = get f in map := Predicate.Map.add f (x :: old) !map in
  let add_one (l, ccl) = match l with
    | One    (f, ts)                -> push f  (ts,  Okay, ccl)
    | Two    ((f1, ts1), (f2, ts2)) -> push f1 (ts1, exists_unify ts1 (f2, ts2), ccl)
    | OkBut  ((f, ts), t)           -> push f  (ts,  IfNot t, ccl)
    | OkDist ((f, ts), t1, t2)      -> push f  (ts,  IfDist (t1, t2), ccl) in
  List.iter add_one literals;
  map := Predicate.Map.map List.rev !map;
  !map

let low_funs prog = get_rules (to_literals prog)

let dead_ends keys low_level =
  let is_absent f = not (Predicate.Map.mem f low_level) in
  let is_dead f = is_absent f && f <> Predicate.Next in
  Predicate.Set.filter is_dead keys

let make () (rules, info) =
  let funs = low_funs rules in
  let dead = dead_ends info.A.predicates funs in
  let forward = Predicate.Set.fold (fun x -> Predicate.Map.add x []) dead funs in
  (forward, info)
