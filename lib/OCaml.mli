type param = unit

val prec_funcall : int
val prec_neg : int
val prec_conj : int
val prec_equal : int
val prec_ifthenelse : int
val prec_stmt : int

val dummy_var : Instructions.T.dummy_var -> string
val qualified : int -> Instructions.T.qualified_function -> string
val base_expr : Instructions.T.base_expr -> string
val expr : int -> Instructions.T.expr -> string
val stmt : int -> int -> Instructions.T.stmt -> string
val mbranch : Instructions.T.match_branch -> string
val global : int -> Instructions.T.global -> string
val program : param -> Instructions.T.program -> string
