open Printf

module I = Instructions.T
module S = Scope

let prec_print prec prec' arg = if prec <= prec' then sprintf "(%s)" arg else arg

let prec_funcall = 1
let prec_neg = 1
let prec_equal = 2
let prec_conj = 3
let prec_ifthenelse = 4
let prec_match = 5
let prec_stmt = 6

let escape = Utils.MString.escape ['_'; '+'; '-'; '<'; '>'; '='; '/'; '\''; '\\'] '_' 

let symbol (name, _, old_arity) = sprintf "%s_%d" name old_arity
let predicate fct =
  let name = match fct with
    | Predicate.Constant symb -> symbol symb
    | _ -> Ast.Print.predicate fct in
  sprintf "P_%s" (escape name)
let fundicate fct = sprintf "_fun%s" (predicate fct)

let _data arg = sprintf "kb_%s" arg

let types = function
  | I.TState -> "state"
  | I.TString -> "string"
  | I.TBool -> "bool"
  | I.TPredicate -> "predicate"
  | I.Void -> "void"
let var_type = function
  | `TTerm -> "struct term"
  | `TArray -> "struct term[]"
  | `TFTerm -> "optterm"
  | `TFArray -> "struct optterm[]"

let algebraic_type name l = sprintf "enum %s = %s;" name (Utils.Print.list' "{" ", " "}" (fun x -> x) l)

let typedef name = function
  | I.KB _ -> sprintf "type %s =...;" (types I.TState)
  | I.Predicates preds -> algebraic_type name (Predicate.Set.mapl predicate preds)
  | I.Symbols    symbs -> algebraic_type name (Symbol.Set.mapl symbol  symbs)

let dummy_var = function
  | I.DummyVar (_, s) -> Scope.Print.variable s
  | I.OtherVar (_, s) -> sprintf "%s" s
  | I.FunConstant fct -> symbol fct

let dummy_decl = function
  | I.DummyVar (t, s) -> sprintf "%s %s" (var_type t) (Scope.Print.variable s)
  | I.OtherVar (t, s) -> sprintf "%s %s" (types t) s
  | I.FunConstant fct -> assert (fct <> fct); "" (*sprintf "%s %s" (types I.Fact) (predicate fct)*)

let print_args vars = Utils.Print.list' "(" ", " ")" dummy_var vars

let local = function
  | I.AddFact -> "add_fact"
  | I.FPredicate pred -> sprintf "_fun%s" (predicate pred)
  | I.Initialization -> "initialization"
  | I.MakeMove -> "make_move"
  | I.PredicateToString -> "predicate_to_string"
  | I.Print -> "print"
  | I.PrintFact -> "print_fact"
  | I.SymbolToString -> "symbol_to_string"
  | I.Restart -> "restart"

let local_decl = function
  | I.AddFact -> sprintf "%s add_fact" (types I.Void)
  | I.FPredicate pred -> sprintf "%s _fun%s" (types I.Void) (predicate pred)
  | I.Initialization -> sprintf "%s initialization" (types I.Void)
  | I.MakeMove -> sprintf "%s make_move" (types I.TBool)
  | I.PredicateToString -> sprintf "%s predicate_to_string" (types I.TString)
  | I.Print -> sprintf "%s print" (types I.Void)
  | I.PrintFact -> sprintf "%s print_fact" (types I.TString)
  | I.SymbolToString -> sprintf "%s symbol_to_string" (types I.TString)
  | I.Restart -> sprintf "%s restart" (types I.Void)

let qualified = function
  | I.KBAdd   (v1, pred, v2) -> sprintf "%s %s %s" (_data   "add") (predicate pred) (print_args [v1; v2])
  | I.KBNext  (v1, pred, v2) -> sprintf "%s %s %s" (_data  "next") (predicate pred) (print_args [v1; v2])
  | I.KBGet   (v1, pred) -> sprintf "%s %s" (_data "get") (print_args [v1; I.FunConstant (ignore pred; assert false)])
  | I.KBClear    (v1, pred) -> sprintf "%s %s.%s" (_data "clear")  (dummy_var v1) (predicate pred)
  | I.KBCreate    _ -> sprintf "%s ()" (_data "create")
  | I.KBPrint    var -> sprintf "%s %s %s" (_data "print_fact") (local I.SymbolToString) (dummy_var var)
  | I.KBPrintArray var -> sprintf "%s %s %s" (_data "print_array") (local I.SymbolToString) (dummy_var var)
  | I.LocalCall  (lf, dvs) -> sprintf "%s %s" (local lf) (Utils.Print.unspaces dummy_var dvs)
  | I.PrintErr (pred, var) -> sprintf "prerr_endline (%S ^ \" \" ^ %s)" (predicate pred) (dummy_var var)

let base_expr = function
  | I.String s -> sprintf "\"%s\"" s
  | I.Bool b -> sprintf "%b" b
  | I.Predicate fct -> predicate fct
  | I.Symbol symb -> symbol symb
  | I.Variable var -> dummy_var var

let newline n = "\n" ^ Utils.MString.repeat "  " n

let make_space indent str = if str = "" then str else str ^ newline indent
let _fact = "term"
let _pred_field = "pred"
let _args_field = "args"
let _args_access = sprintf "%s[%d]" _args_field
let list l = Utils.Print.list' "{" ", " "}" (fun x -> x) l
let aand l =  Utils.Print.list' "" " && " "" (fun x -> x) l
let c_ifthen indent arg1 arg2 = sprintf "if (%s) {%s%s%s}" arg1 (newline (indent + 1)) arg2 (newline indent)
let c_ifthenelse indent arg1 arg2 arg3 =
  let then_part = sprintf "{%s%s%s}" (newline (indent + 1)) arg2 (newline indent)
  and else_part = sprintf "{%s%s%s}" (newline (indent + 1)) arg3 (newline indent) in
  sprintf "if (%s) %s %s" arg1 then_part else_part

let destroy var = sprintf "free(%s);" (dummy_var var)

let rec understand_match_branch offset term = match term with
  | Ast.T.Variable var -> [(offset, Scope.read_variable var)], []
  | Ast.T.Fact (fct, terms) ->
    let (vars, conds) = understand_match_branchs offset terms in
    (vars, conds @ [(offset, fct)])

and understand_match_branchs offset terms =
  let f (i, vars, conds) term' =
    let (nvars, nconds) = understand_match_branch (i :: offset) term' in
    (i + 1, nvars @ vars, nconds @ conds) in
  let (_, vars, conds) = List.fold_left f (0, [], []) terms in
  (vars, conds)

let access_offset var offset = if offset = [] then dummy_var var else Utils.Print.list' (dummy_var var ^ ".") "." "" _args_access (List.rev offset)
let mb_term indent match_var term stmt =
  let variable str = dummy_var (I.DummyVar (`TTerm, str)) in
  let (vars, conds) = understand_match_branch [] term in
  let vars_indent = if conds = [] then indent else indent + 1 in
  let (vars, conds) = (List.rev vars, List.rev conds) in
  let check (offset, fct) = sprintf "%s.%s == %s" (access_offset match_var offset) _pred_field (symbol fct) in
  let conds = aand (List.map check conds) in
  let add_var (offset, var) = sprintf "%s %s = %s;" _fact (variable var) (access_offset match_var offset) in
  let vars = make_space vars_indent (Utils.Print.list' "" (newline vars_indent) "" add_var vars) in
  if conds <> "" then c_ifthen indent conds (vars ^ stmt (indent + 1))
  else sprintf "%s%s" vars (stmt indent)

let mb_terms indent match_var terms stmt =
  let variable str = dummy_var (I.DummyVar (`TArray, str)) in
  let (vars, conds) = understand_match_branchs [] terms in
  let vars_indent = if conds = [] then indent else indent + 1 in
  let (vars, conds) = (List.rev vars, List.rev conds) in
  let check (offset, fct) = sprintf "%s.%s == %s" (access_offset match_var offset) _pred_field (symbol fct) in
  let conds = aand (List.map check conds) in
  let add_var (offset, var) = sprintf "%s %s = %s;" _fact (variable var) (access_offset match_var offset) in
  let vars = make_space vars_indent (Utils.Print.list' "" (newline vars_indent) "" add_var vars) in
  if conds <> "" then c_ifthen indent conds (vars ^ stmt (indent + 1))
  else sprintf "%s%s" vars (stmt indent)

let flat_term fct = function
  | None -> assert (Ast.arity fct = 0); sprintf "%s" (symbol fct)
  | Some var -> assert (Ast.arity fct > 0); sprintf "{.%s = %s, .%s = %s}" _pred_field (symbol fct) _args_field (Scope.Print.variable var)

let crecord indent lvar compound =
  ignore (indent, lvar, compound);
  assert false
(*  let rec aux i accu vars = match vars with
    | [] -> List.rev accu
    | v :: rest ->
      let asgmt = sprintf "%s = %s[%d];" (dummy_decl v) (dummy_var compound) i in
      aux (i + 1) (asgmt :: accu) rest in
  let asgmts = aux 0 [] lvar in
  Utils.Print.list' "" (newline indent) "" (fun x -> x) asgmts*)

let mbranch indent _prec matcher mb statmt = match mb with
  | I.MBTermTuple ts -> mb_terms indent matcher ts statmt
  | I.MBTerm t -> mb_term indent matcher t statmt
  | I.MBBase b -> c_ifthen indent (sprintf "%s == %s" (base_expr b) (dummy_var matcher)) (statmt indent)
  | I.Default  -> sprintf "%s" (statmt indent)

let construction indent = function
  | I.CFact   (var, fct, arg) -> sprintf "%s = %s;" (dummy_decl (I.DummyVar (`TTerm, var))) (flat_term fct arg)
  | I.CArray  (compound, l) -> sprintf "%s = %s;" (dummy_decl compound) (list (List.map dummy_var l))
  | I.CRecord (compound, l) -> crecord indent l compound

let rec expr prec = function
  | I.KBExists  (v1, pred, v2) -> sprintf "%s %s %s" (_data "exists") (predicate pred) (print_args [v1; v2])
  | I.Not              e -> prec_print prec prec_neg (sprintf "! %s" (expr prec_neg e))
  | I.Conjunction     es -> prec_print prec prec_conj (aand (List.map (expr prec_conj) es))
  | I.Equal     (e1, e2) -> prec_print prec prec_equal (sprintf "%s == %s" (dummy_var e1) (dummy_var e2))
  | I.Different (e1, e2) -> prec_print prec prec_equal (sprintf "%s != %s" (dummy_var e1) (dummy_var e2))

let rec stmt indent prec = function
  | I.Construct c -> construction indent c
  | I.Iterate iter -> iterate indent iter
  | I.SFunCall qf -> sprintf "%s;" (qualified qf)
  | I.ValueDef (s, qf) -> sprintf "%s = %s;" (dummy_decl s) (qualified qf)
  | I.ValueUpdate (s, qf) -> sprintf "%s = %s;" (dummy_var s) (qualified qf)
  | I.Sequence ss -> sequence indent prec ss
  | I.IfThenElse ite -> ifthenelse indent prec ite
  | I.Match (e, ess) -> matching indent prec e ess
  | I.Return e -> sprintf "return %s;" (base_expr e)
  | I.Free var -> destroy var
  | I.Noop  -> "{}"
  | I.Abort -> "assert false;"

and iterate indent (x, f, l) =
  let start = sprintf "%s = 0" (dummy_decl x)
  and end_ = sprintf "%s < end of %s" (dummy_var x) (dummy_var l)
  and incr = sprintf "%s++" (dummy_var x)
  and body = stmt (indent + 1) prec_stmt f in
  sprintf "for(%s; %s; %s){%s%s%s}" start end_ incr (newline (indent + 1)) body (newline indent)

and ifthenelse indent _prec (cond, s1, s2) =
  let arg1 = expr prec_ifthenelse cond
  and arg2 = stmt (indent + 1) prec_ifthenelse s1
  and arg3 = stmt (indent + 1) prec_ifthenelse s2 in
  if s2 = I.Noop then c_ifthen indent arg1 arg2
  else c_ifthenelse indent arg1 arg2 arg3

and matching indent prec dummy branches =
  let one_match (cond, ccl) =
    let ccl ind = stmt ind prec_match ccl in
    mbranch indent prec_funcall dummy cond ccl in
  let matcher = Utils.Print.list' "" (newline indent) "" one_match branches in
  prec_print prec prec_match matcher

and sequence indent prec ss = Utils.Print.list' "" (newline indent) "" (stmt indent prec) ss

let global indent = function
  | I.Typedef (name, typ) -> typedef name typ
  | I.TypeSdef (t1, t2) -> sprintf "type %s = %s;" t1 (var_type t2)
  | I.GlobalValueDef  (s, be) -> sprintf "%s = %s;" (dummy_decl s) (base_expr be)
  | I.GlobalConstruct c -> construction indent c
  | I.GlobalFunDef (s, es, st) ->
    let args = Utils.Print.list' "(" ", " ")" dummy_decl es in
    let arg2 = stmt (indent + 1) prec_stmt st in
    sprintf "%s %s {%s%s%s}%s" (local_decl s) args (newline (indent + 1)) arg2 (newline indent) (newline indent)
  | I.Recursive ls ->
    let one (s, es, st) =
      let arg1 = Utils.Print.list' "(" ", " ")" dummy_decl es
      and arg2 = stmt (indent + 1) prec_stmt st in
      sprintf "%s %s {%s%s%s}%s" (fundicate s) arg1 (newline (indent + 1)) arg2 (newline indent) (newline indent)  in
    Utils.Print.list' "void " (sprintf "%svoid " (newline indent)) "" one ls

let program () gs =
  let indent = 0 in
  String.concat (newline indent ^ newline indent) (List.map (global indent) gs)
