%token <string> CONSTANT
%token <string> VARIABLE
%token RULE
%token LPAREN RPAREN
%token OR AND NOT
%token EOF

%{
%}
%start <Ast.S.program> program
%%

term:
| symbol = CONSTANT { Ast.S.Fact (symbol, []) }
| LPAREN symbol = CONSTANT terms = list (term) RPAREN { Ast.S.Fact (symbol, terms) }
| v = VARIABLE { Ast.S.Variable v }

atomic:
| predicate = CONSTANT { (predicate, []) }
| LPAREN predicate = CONSTANT terms = list (term) RPAREN { (predicate, terms) }

literal:
| t = atomic { Ast.S.Positive t }
| LPAREN NOT t = atomic RPAREN { Ast.S.Negative t }

logic:
| l = literal { Logic.Literal l }
| LPAREN AND l = nonempty_list (logic) RPAREN { Logic.nest_and l }
| LPAREN OR  l = nonempty_list (logic) RPAREN { Logic.nest_or l }

rule:
| head = atomic { (head, Logic.Ok)}
| LPAREN RULE head = atomic body = list (logic) RPAREN { (head, Logic.nest_and body) }

program:
| l = list (rule) EOF { l }
