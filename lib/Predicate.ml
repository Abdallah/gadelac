type pred = Base | Input | Does | Goal | Init | Legal | Negative | Next | Positive | Role | Terminal | True | Constant of Symbol.t
include Utils.DataStructure.F
          (struct type t = pred
                  let compare = compare
                  let print = function
                    | Base     -> "base"
                    | Input    -> "input"
                    | Does     -> "does"
                    | Goal     -> "goal"
                    | Init     -> "init"
                    | Legal    -> "legal"
                    | Negative -> "neg"
                    | Next     -> "next"
                    | Positive -> "pos"
                    | Role     -> "role"
                    | True     -> "true"
                    | Terminal -> "terminal"
                    | Constant s -> Symbol.print s
           end)
