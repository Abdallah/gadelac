type 'a substitution = 'a Ast.T.term Variable.Map.t
type 'a t = 'a substitution option

val print : 'a t -> string
val empty : 'a t
val empty_sub : 'a substitution
val compose : 'a t -> 'a t -> 'a t
val unify : 'a Ast.T.term -> 'a Ast.T.term -> 'a t
val unify_sequence : 'a Ast.T.term list -> 'a Ast.T.term list -> 'a t
val apply_var : 'a substitution -> string -> Ast.T.free Ast.T.term
val apply_term : 'a substitution -> 'b Ast.T.term -> 'b Ast.T.term
val apply_atomic : 'b substitution -> 'a Ast.T.atomic -> 'a Ast.T.atomic
val apply_literal : 'b substitution -> 'a Ast.T.literal -> 'a Ast.T.literal
val add_equality : 'a substitution -> 'a Ast.T.term * 'a Ast.T.term -> 'a t
val equalities : 'a t -> (string * 'a Ast.T.term) list option
