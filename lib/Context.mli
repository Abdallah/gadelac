type t = (Ast.T.predicate * int, Ast.T.symbol * int) Either.t
val print : t -> string

val associated : Ast.T.symbol -> t list
val associated_pred : Ast.T.predicate -> t list

(** Returns the contexts associated to the argument function constant that are not associated to variables in the fact. *)
val non_variables : Ast.T.symbol * 'a Ast.T.term list -> t list

val from_variable : string -> 'a Desugared.T.clause -> t list
val to_variable : t -> 'a Desugared.T.clause -> Variable.Set.t

