(** Cluster rigid literals in clauses under new equivalent rigid predicates. *)

val program : 'a Desugared.T.program -> Ast.T.free Desugared.T.program
