type param =
  { specialize_tempo  : bool ref;
    specialize_bound  : bool ref;
    ground_all   : bool ref;
    ground      : (string * int) list ref;
    ground_count : int ref;
    measured : bool ref;
    flatten : bool ref;
    specialize  : (string * int) list ref;
    reachability : Reachability.param ref;
    compute_rigids : bool ref;
    merge_rigids : bool ref;
    inlining : int ref;
    frame_translation : bool ref;
    noop_move : string ref }

val default_param : unit -> param
val set_level : param -> int -> unit

module Print :
sig
  val param : param -> string
  val program : bool -> 'a Desugared.T.program -> string
end

val make : param -> 'a Desugared.T.program -> Ast.T.free Desugared.T.program
