open Printf

type param = bool

type action = Inversion.hypothesis * Ast.T.free Ast.T.atomic list
type branch = Ast.T.free Ast.T.term list * action list
type matching = branch list list

type program = matching Predicate.Map.t * Annotations.t

module Print =
struct

  let action (hypo, ccls) =
    let pr_ccls = match ccls with
      | [] -> "()"
      | a :: [] -> Ast.Print.atomic a
      | _ -> Utils.Print.list' "{" "; " "}" Ast.Print.atomic ccls in
    sprintf "%s%s" (Inversion.Print.hypothesis hypo) pr_ccls

  let branch (ts, acs) =
    let pr_actions acs = match acs with
      | [] -> assert false
      | a :: [] -> action a
      | _ -> Utils.Print.list' "\n {" ";\n  " "}" action acs in
    sprintf "%s -> %s" (Utils.Print.list Ast.Print.term ts) (pr_actions acs)

  let matching_aux ms = match ms with
    | [] -> assert false
    | a :: [] -> branch a
    | _ -> Utils.Print.list' " " "\n    " ""  branch ms

  let matching mss = match mss with
    | [] -> ""
    | a :: [] -> matching_aux a
    | _ -> Utils.Print.list' "(" ")\n  (" ")" matching_aux mss

  let program (prog, annot) =
    let map = Predicate.Map.print' "" "\n\n" "\n\n" (Utils.Print.couple Ast.Print.predicate matching) prog in
    sprintf "%s%s" map (Annotations.print annot)
end

module VariableNaming =
struct

  let new_names () =
    let counter = ref (-1) in
    let next () =
      incr counter;
      sprintf "v%d" !counter in
    next

  let rec through_term naming mapping = function
    | Ast.T.Variable x ->
      if Variable.Map.mem x mapping then Ast.T.Variable (Variable.Map.find x mapping), mapping
      else let y = naming () in Ast.T.Variable y, Variable.Map.add x y mapping
    | Ast.T.Fact (fct, terms) ->
      let (terms, map) = through_terms naming mapping terms in
      Ast.T.Fact (fct, terms), map
  and through_terms naming mapping terms = 
    let f (ts, map) t =
      let (nt, nmap) = through_term naming map t in
      (nt :: ts, nmap) in
    let (ts, map) = List.fold_left f ([], mapping) terms in
    List.rev ts, map

  let replace_name_binding mapping l = 
    assert (List.for_all (fun (a, b) -> Variable.Map.mem a mapping && Variable.Map.mem b mapping) l);
    let f (a, b) = (Variable.Map.find a mapping, Variable.Map.find b mapping) in
    List.map f l

  let replace_name_unify naming mapping unify =
    let terms, map = through_terms naming mapping unify.Inversion.terms in
(*    eprintf "Grouping\n mapping:%s\n unify:%s\n terms:%s\n map:%s\n"
      (Variable.Map.print Utils.Print.string mapping)
      (Inversion.Print.unify unify)
      (Utils.Print.list Ast.Print.term terms)
      (Variable.Map.print Utils.Print.string map);*)
    { Inversion.bindings = replace_name_binding map unify.Inversion.bindings;
      Inversion.terms = terms;
      Inversion.argument = unify.Inversion.argument },
    map

  let through_hypothesis naming mapping = function
    | Inversion.Okay -> Inversion.Okay, mapping
    | Inversion.IfExists atom -> Inversion.IfExists (Desugared.rename_variables_atomic mapping atom), mapping
    | Inversion.IfNot    atom -> Inversion.IfNot    (Desugared.rename_variables_atomic mapping atom), mapping
    | Inversion.IfUnify uni ->
      let uni, map = replace_name_unify naming mapping uni in
      Inversion.IfUnify uni, map
    | Inversion.IfDist (var, term) -> assert (Variable.Map.mem var mapping);
      Inversion.IfDist (Variable.Map.find var mapping, Desugared.rename_variables_term mapping term), mapping


  let rename_variables_aux ((terms, hypothesis, ccl) : Inversion.branch) =
    let naming = new_names () in
    let mapping = Variable.Map.empty in
    let f (ts, map) t =
      let (nt, nmap) = through_term naming map t in
      (nt :: ts, nmap) in
    let terms', map = List.fold_left f ([], mapping) terms in
    let nterms = List.rev terms' in
    let nmatching, map = through_hypothesis naming map hypothesis in
    let nccl = Desugared.rename_variables_atomic map ccl in
    (nterms, nmatching, nccl)

  let rename_variable lows =
    let one = List.map rename_variables_aux in
    Predicate.Map.map one lows
end

module Group =
struct

  let replace_or_add f default l = match Utils.PList.replace_one f l with
    | None -> Utils.PList.queue l (default ())
    | Some v -> v

  let rec incompatible l1 l2 =
    assert (List.length l1 = List.length l2);
    List.exists2 incompatible_fact l1 l2
  and incompatible_fact f1 f2 = match f1, f2 with
    | Ast.T.Variable _, _ | _, Ast.T.Variable _ -> false
    | Ast.T.Fact (f1', ts1), Ast.T.Fact (f2', ts2) -> f1' <> f2' || incompatible ts1 ts2

  let insert_action (actions : action list) (cond, ccl) : action list =
    let f (cond', ccls) = if cond = cond' then Some (cond', ccl :: ccls) else None in
    let default () = (cond, [ccl]) in
    replace_or_add f default actions

  let branch (ts, all_actions) : branch =
    let actions_groups = List.fold_left insert_action [] all_actions in
    (ts, List.rev actions_groups)

  let insert_match ((ts, hyp, ccl) : Inversion.branch) matches =
    let action = (hyp, ccl) in
    let f (ts', acts) =
      if ts = ts' then Some (ts', action :: acts)
      else if incompatible ts ts' then None
      else raise Exit in
    let default () = (ts, [action]) in
    try Some (replace_or_add f default matches) with
        Exit -> None

  let find_good_matches matches_list matching =
    let f = insert_match matching in
    let default () = Option.get (insert_match matching []) in
    try replace_or_add f default matches_list with _ -> assert false

  let matching (all_matches : Inversion.branch list) =
    let result = List.fold_left find_good_matches [] all_matches in
    List.rev_map (List.map branch) result

  let prog funs = Predicate.Map.map matching funs

end

module None =
struct
  let action hyp conc = (hyp, [conc])
  let branch ((terms, hyp, conc) : Inversion.branch) = [terms, [action hyp conc]]
  let matching mat = List.map branch mat
  let prog fs = Predicate.Map.map matching fs
end

let make use (map, annot) =
  let map = VariableNaming.rename_variable map in
  let group = if use then Group.prog else None.prog in
  group map, annot
