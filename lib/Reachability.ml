open Printf

let deprintf level format = if level >= 3 then eprintf format else ifprintf stderr format

let fluent = [Ast.T.Variable "fluent"]
let true_init = ((Predicate.True, fluent), [Desugared.T.Okay (Predicate.Init, fluent)])
let true_next = ((Predicate.True, fluent), [Desugared.T.Okay (Predicate.Next, fluent)])
let true_neg  = ((Predicate.True, fluent), [Desugared.T.Okay (Predicate.Negative, fluent)])
let true_pos  = ((Predicate.True, fluent), [Desugared.T.Okay (Predicate.Positive, fluent)])
let does_legal =
  let player_move = [Ast.T.Variable "player"; Ast.T.Variable "move"] in
  ((Predicate.Does, player_move), [Desugared.T.Okay (Predicate.Legal, player_move)])

let add_frames rules = does_legal :: true_next :: true_init :: true_neg :: true_pos :: Desugared.cast_program rules

module Naive (D : Domain.S) =
struct
  let body_filter negate domain body subs =
    let aux : type a. D.substitutions -> a Desugared.T.literal -> D.substitutions = fun subs -> function
      | Desugared.T.Okay atom -> D.compose domain subs atom
      | Desugared.T.Negation atom -> if negate then D.neg_compose domain subs atom else subs
      | Desugared.T.Distinct (var, term) -> if negate then D.dist_compose subs var term else subs in
    List.fold_left aux subs body

  let substitutions domain (head, body) = body_filter false domain (Desugared.T.Okay head :: body) D.unit

  let forw_step_rule negate domain (head, body) =
    let subs = body_filter negate domain body D.unit in
    D.add_subs subs head domain

  let back_step_rule domain negate needed (head, body) =
    deprintf 1 "Engine: needed %s clause %s\n%!" (D.print needed) (Desugared.Print.clause (head, body));
    let subs = body_filter negate domain body (D.unify head needed) in
    let apply_lit needs = function
      | Desugared.T.Okay atom | Desugared.T.Negation atom -> (*eprintf "Reachability: need %s, %d subs\n" (Ast.Print.atomic atom) (List.length subs);*)
        D.add_subs subs atom needs
      | Desugared.T.Distinct _ -> needs in
    List.fold_left apply_lit needed body

  let fixpoint negate step_rule domain rules =
    assert (Safety.sorted rules);
    let rec aux d =
      let d2 = List.fold_left (step_rule negate) d rules in
      deprintf 0 "fixpoint: %s\n%!" (D.print d2);
      if D.equal d2 d then d else aux d2 in
    let final_domain = aux domain in
    deprintf 1 "fixpoint end: %s\n%!" (D.print final_domain);
    final_domain

  let stage_program program =
    let program = Safety.sort program in
    let graph = Analysis.dep_graph program in
    let scc = Predicate.SCC.tarjan graph in
    let comp_rules comp = List.filter (fun ((pred, _), _) -> Predicate.Set.mem pred comp) program in
    Utils.PList.map comp_rules scc

  let forward exact program =
    let program = Desugared.cast_program program in
    let program = if not exact then add_frames program else program in
    let stages = stage_program program in
    List.fold_left (fixpoint exact forw_step_rule) D.empty stages

  let needed_atoms forw =
    let facts = [(Predicate.Role, [Ast.T.Variable "name"]);
                 (Predicate.Terminal, []);
                 (Predicate.Legal, [Ast.T.Variable "name"; Ast.T.Variable "action"]);
                 (Predicate.Goal, [Ast.T.Variable "name"; Ast.T.Variable "score"])] in
    let subs = List.fold_left (D.compose forw) D.unit facts in
    deprintf 1 "Reachability subs: %s\n" (D.print_subs subs);
    Utils.PList.fold_right (D.add_subs subs) facts D.empty

  let backward exact program =
    let program = Desugared.cast_program program in
    let program = if not exact then add_frames program else program in
    let stages = stage_program program in
    deprintf 1 "Reachability.backward stages computed: %s\n%!" (Utils.Print.unlines (Utils.Print.unspaces Desugared.Print.clause) stages);
    let forw_domain = List.fold_left (fixpoint exact forw_step_rule) D.empty stages in
    deprintf 1 "Reachability.backward domain: %s\n%!" (D.print forw_domain);
    let needed = needed_atoms forw_domain in
    deprintf 1 "Reachability.backward needed: %s\n%!" (D.print needed);
    List.fold_left (fixpoint exact (back_step_rule forw_domain)) needed (List.rev stages)

  let prune program =
    let back = backward false program in
    let useful_rule clause = D.consistent (substitutions back clause) in
    let useful, useless = List.partition useful_rule (Desugared.cast_program program) in
    useless, useful
end

module NaiveE = Naive (Domain.Exact)
module NaiveP = Naive (Domain.Path)
module NaiveG = Naive (Domain.Graph)

type param = Trivial | Graph | Paths | Precise
let print = function
  | Trivial -> "trivial"
  | Graph -> "graph"
  | Paths -> "paths"
  | Precise -> "precise"
let make param program =
  let prune = match param with
    | Trivial -> (fun prog -> ([], Desugared.cast_program prog))
    | Graph -> NaiveG.prune
    | Paths -> NaiveP.prune
    | Precise -> NaiveE.prune in
  let useless, useful = prune program in
  if useless <> [] then eprintf "REACHABILITY: The following rules are deemed useless\n%s\n%!" (Utils.Print.list Desugared.Print.clause useless);
  useful
