let is_safe' : type a. string list -> a Desugared.T.literal -> string option = fun safe_vars -> function
  | Desugared.T.Okay _ -> None
  | Desugared.T.Negation t -> Utils.PList.except (Utils.PList.memr safe_vars) (Ast.get_variables_atomic t)
  | Desugared.T.Distinct (s, t) -> Utils.PList.except (Utils.PList.memr safe_vars) (s :: Ast.get_variables t)

let is_safe safe_vars lit = is_safe' safe_vars lit = None

let get_safe_variables : type a. a Desugared.T.literal -> string list = function
  | Desugared.T.Okay atom -> Ast.get_variables_atomic atom
  | Desugared.T.Negation _ -> []
  | Desugared.T.Distinct _ -> []

let sort_literals body =
  let rec aux stored safe_vars rest =
    match Utils.PList.find_first (is_safe safe_vars) rest with
    | None -> assert (List.length stored = List.length body); List.rev stored
    | Some (before, lit, after) -> aux (lit :: stored) (get_safe_variables lit @ safe_vars) (List.rev_append before after) in
  aux [] [] body

let sort_clause (head, body) = (head, sort_literals body)
let sort prog = Utils.PList.map sort_clause prog

let check_clause (head, body) =
  let safe_vars = List.concat_map get_safe_variables body in
  Utils.PList.find' (is_safe' safe_vars) (Desugared.T.Okay head :: body)

let program prog =
  let aux cl = Option.map (fun v -> v, cl) (check_clause cl) in
  Utils.PList.find' aux prog

let sorted_clause clause = sort_clause clause = clause
let sorted prog = List.for_all sorted_clause prog
