val make : Context.t list -> 'a Desugared.T.program -> 'a Desugared.T.program
val flatten_program : 'a Desugared.T.program -> 'a Desugared.T.program

val specialize_atomic  : (Ast.T.predicate * int) list -> 'a Ast.T.atomic -> 'a Ast.T.atomic
val specialize_literal : (Ast.T.predicate * int) list -> 'a Desugared.T.literal -> 'a Desugared.T.literal
val specialize_clause  : (Ast.T.predicate * int) list -> 'a Desugared.T.clause -> 'a Desugared.T.clause
val specialize_program : (Ast.T.predicate * int) list -> 'a Desugared.T.program -> 'a Desugared.T.program
