val inline_aux : int -> 'a Desugared.T.program -> 'a Desugared.T.program -> Ast.T.free Desugared.T.program
val program : int -> 'a Desugared.T.program -> Ast.T.free Desugared.T.program

module Grounded :
sig
  val program : int * int -> Ast.T.ground Desugared.T.program -> Ast.T.free Desugared.T.program
end

module FullGround :
sig
  val program : 'a Desugared.T.program -> Ast.T.ground Desugared.T.program
end
