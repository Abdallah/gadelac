type one_rule = E1 of (Ast.T.free Ast.T.atomic * Ast.T.free Ast.T.atomic)
              | E2 of (Ast.T.free Ast.T.atomic * Ast.T.free Ast.T.atomic * Ast.T.free Desugared.T.literal)

type program = one_rule list * Annotations.t

module Print :
sig
  val one_rule : one_rule -> string
  val program : program -> string
end

val make : Frequency.param -> 'a Stratify.program -> program
