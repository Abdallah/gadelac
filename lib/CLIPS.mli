(** Generate a CLIPS file corresponding to the input GDL program. *)

val program : 'a Desugared.T.program -> string
