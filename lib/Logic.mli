type 'a t = And of ('a t * 'a t) | Or of ('a t * 'a t) | Literal of 'a | Ok

val print : ('a -> string) -> 'a t -> string
val map : ('a -> 'b) -> 'a t -> 'b t

val dnf : 'a t -> 'a list list

val nest_and : 'a t list -> 'a t
val nest_or : 'a t list -> 'a t
