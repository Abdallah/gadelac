open Printf

let deprintf format = if false then eprintf format else ifprintf stderr format

module NaiveE = Reachability.Naive (Domain.Exact)
module NaiveP = Reachability.Naive (Domain.Path)
module NaiveG = Reachability.Naive (Domain.Graph)

let expand_variables domain depth vars clause =
  let subs = NaiveE.substitutions domain clause in
  deprintf "Ground EV: %s depth:%d %d %s\n%!" (Domain.Exact.print domain) depth (List.length subs) (Utils.Print.unspaces (Variable.Map.print Ast.Print.term) subs);
  let subs = Utils.PList.map (Variable.Map.filter (fun key _ -> List.mem key vars)) subs in
  let asubs = Utils.PList.map (Variable.Map.mapi (fun key term -> Ast.abstract (Generate.variable (key ^ "_")) depth term)) subs in
  deprintf "Ground EV: %d %s\n%!" (List.length asubs) (Utils.Print.unspaces (Variable.Map.print Ast.Print.term) asubs);
  Utils.PList.uniques (List.concat_map (fun sub -> Desugared.apply_clause sub clause) asubs)

let instantiate_context dep_map depth prog context : 'a Desugared.T.program =
  let inst_clause clause =
    let vars = Context.to_variable context clause in
    let vars = Variable.Set.elements vars in
    (*let l = expand_variables dep_map depth vars clause in
    eprintf "Ground: context %s, vars %s\nclause: %s\nexp:%s\n\n%!" (Context.print context) (Utils.Print.unspaces Utils.Print.string vars) (Desugared.Print.clause clause) (Utils.Print.unlines Desugared.Print.clause l);*)
    expand_variables dep_map depth vars clause in
 List.concat_map inst_clause prog

let all_atoms prog = NaiveE.backward false prog
let depth_program contexts depth prog =
  if contexts = [] then prog else
    let domain = all_atoms prog in
    deprintf "Ground.DP domain:\n%s\n" (Domain.Exact.print domain);
    let aux p con = instantiate_context domain depth p con in
    List.fold_left aux prog contexts
let program contexts prog = depth_program contexts max_int prog

let expand_to_term var symbs =
  let aux symb =
    let gen = Generate.variable (var ^ "_") in
    Ast.T.Fact (symb, Utils.PList.map (fun _ -> Ast.T.Variable (gen ())) (Utils.PList.range 0 (Ast.arity symb))) in
  Utils.PList.map aux symbs

let expand_one domain vars clause =
  let subs = Domain.Path.develop (NaiveP.substitutions domain clause) in
  let subs = Option.map (Variable.Map.filter (fun key _ -> List.mem key vars)) subs in
  let subs = Option.map (Variable.Map.mapi expand_to_term) subs in
  let subs = Utils.Option.lift (Option.map Variable.Map.cross_products subs) in
  Utils.PList.uniques (List.concat_map (fun sub -> Desugared.apply_clause sub clause) subs)

let instantiate_context_one dep_map prog context : 'a Desugared.T.program =
  let inst_clause clause =
    let vars = Context.to_variable context clause in
    let vars = Variable.Set.elements vars in
    expand_one dep_map vars clause in
 List.concat_map inst_clause prog

let depth_program_one contexts prog =
  let dom_path = NaiveP.backward false prog in
  List.fold_left (instantiate_context_one dom_path) prog contexts
let hsfc prog = depth_program_one [Either.Left (Predicate.True, 0); Either.Left (Predicate.Next, 0)] prog

let assert_ground_prog prog =
  let assert_ground_literal : type a. a Desugared.T.literal -> Ast.T.ground Desugared.T.literal = function
    | Desugared.T.Distinct _ -> assert false
    | Desugared.T.Okay atom -> Desugared.T.Okay (Ast.assert_ground_atomic atom)
    | Desugared.T.Negation atom -> Desugared.T.Negation (Ast.assert_ground_atomic atom) in
  let assert_ground_clause (head, body) = (Ast.assert_ground_atomic head, Utils.PList.map assert_ground_literal body) in
  Utils.PList.map assert_ground_clause prog

let full_program prog =
  let domain = all_atoms prog in
  let full_clause clause = expand_variables domain max_int (Desugared.get_variables_clause clause) clause in
  let prog = Utils.PList.uniques (List.concat_map full_clause prog) in
  assert_ground_prog prog

let relevant_depth param terms =
  let max_depth = List.fold_left max 0 (List.map Ast.depth_term terms) in
  if List.length terms <= param then max_depth
  else
    let var () = "" in
    let rec aux depth =
      let terms = Utils.PList.uniques (List.map (Ast.abstract var depth) terms) in
      if List.length terms <= param then aux (depth + 1)
      else depth - 1 in
    aux 1

let count_clause param domain clause =
  let subs = NaiveE.substitutions domain clause in
  let doms = Variable.Map.merges Utils.PList.uniques subs in
  let deps = Variable.Map.map (relevant_depth param) doms in
  let aux var depth clauses = Utils.PList.uniques (List.concat_map (expand_variables domain depth [var]) clauses) in
  Variable.Map.fold aux deps [clause]

let count_program param prog =
  let domain = all_atoms prog in
  Utils.PList.uniques (List.concat_map (count_clause param domain) prog)

let rec measured_clause domain clause =
  let subs = Domain.Path.develop (NaiveP.substitutions domain clause) in
  let subs = Option.map (Variable.Map.filter (fun _ -> List.exists (fun symb -> Ast.arity symb > 0))) subs in
  let subs = Option.map (Variable.Map.mapi expand_to_term) subs in
  let subs = Utils.Option.lift (Option.map Variable.Map.cross_products subs) in
  let new_clauses = Utils.PList.uniques (List.concat_map (fun sub -> Desugared.apply_clause sub clause) subs) in
  if new_clauses = [clause] then new_clauses
  else List.concat_map (measured_clause domain) new_clauses

let measured prog =
  let domain = NaiveP.backward false prog in
  List.concat_map (measured_clause domain) prog
