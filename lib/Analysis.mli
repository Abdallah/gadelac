val symbols : 'a Desugared.T.program -> Symbol.Set.t
val predicates : 'a Desugared.T.program -> Predicate.Set.t
val dep_graph : 'a Desugared.T.program -> Predicate.Set.t Predicate.Map.t
val transitive_dep_graph : 'a Desugared.T.program -> Predicate.Set.t Predicate.Map.t
val start_map : 'a Desugared.T.program -> (Ast.T.predicate -> 'b) -> 'b Predicate.Map.t

(* old positive_lits negative_lits *)
val fixpoint : 'a Desugared.T.program -> 'b Predicate.Map.t -> ('b -> 'b list-> 'b list -> 'b) -> 'b Predicate.Map.t
