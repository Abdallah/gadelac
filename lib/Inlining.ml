open Printf

let deprintf format = if true then eprintf format else ifprintf stderr format

module DS = Variable.DisjointSet

let rename_lit map = Unification.apply_literal (Variable.Map.map Ast.variable map)

let make_eqs vars args =
  assert (List.length vars = List.length args);
  let var_args = List.combine vars args in
  let aux : type a. Ast.T.free Ast.T.literal list * string Variable.Map.t -> string * a Ast.T.term -> Ast.T.free Ast.T.literal list * string Variable.Map.t = fun (accu_eqs, accu_sub) (var, term) ->
    match Desugared.rename_variables_term accu_sub (Ast.cast_term term) with
    | Ast.T.Variable v when not (List.mem v vars) -> (accu_eqs, Variable.Map.add v var accu_sub)
    | term2 -> (Ast.T.Equal (Ast.T.Variable var, term2) :: accu_eqs, accu_sub) in
  List.fold_left aux ([], Variable.Map.empty) var_args

let force_body vars args body =
  let (eqs, sub) = make_eqs vars args in
  Utils.PList.map (rename_lit sub) body @ eqs

(** Collect the different predicates and map each one to a pair (list of variables * DNF) that can generate it. *)
let regroup program =
  let make_vars pred = Utils.PList.map (sprintf "inline_%d") (Utils.PList.range 0 (Ast.arity_pred pred)) in
  let add map ((pred, args), body) =
    let (vars, bodies) = Predicate.Map.find_default pred (make_vars pred, []) map in
    let body = Utils.PList.map (fun lit -> Desugared.sugar_lit (Desugared.cast_literal lit)) body in
    let body = force_body vars args body in
    Predicate.Map.add pred (vars, body :: bodies) map in
  List.fold_left add Predicate.Map.empty program

let add_literal context disj_set (var_map, others) lit =
  let relevant_vars = Utils.PList.subtract (Ast.get_variables_lit lit) context in
  match relevant_vars with
  | [] -> (var_map, lit :: others)
  | var :: rest ->
    let var = DS.find var disj_set in
    assert (List.for_all (fun elt -> DS.find elt disj_set = var) rest);
    (Variable.Map.update var (Utils.PList.cons lit) [] var_map, others)

let new_predicate = Generate.predicate "inline_negation"

let make_literal context var lits (accu, new_rules) =
  assert (List.length lits > 0);
  let body_vars = List.concat_map Ast.get_variables_lit lits in
  let comm_vars = List.filter (Utils.PList.memr body_vars) context in
(*  let new_rules, new_head = Generate.add_rule_ast new_rules comm_vars lits in*)
  let new_head = (new_predicate (List.length comm_vars), List.map Ast.variable comm_vars) in
  deprintf "Inline: %s %s\n" (Ast.Print.atomic new_head) (Utils.Print.list Ast.Print.literal lits);
  (Ast.T.Okay new_head :: accu, (new_head, lits) :: new_rules)
(*    (Ast.T.Okay new_head :: accu, new_rules)*)

let cluster_variables context (accu, new_rules) conj =
  let relevant_vars lit = Utils.PList.subtract (Ast.get_variables_lit lit) context in
  let use_literal dset lit = DS.unions (relevant_vars lit) dset in
  let disj_set = List.fold_left use_literal DS.empty conj in
  let (var_map, others) = List.fold_left (add_literal context disj_set) (Variable.Map.empty, []) conj in
  let lits, ruls = Variable.Map.fold (make_literal context) var_map ([], new_rules) in
  ((lits @ others) :: accu, ruls)

let inline_neg new_rules args vars bodies =
  eprintf "new_rules = %s\nargs = %s\nvars = %s\nbodies = %s\n%!" (Utils.Print.list (Utils.Print.couple Ast.Print.atomic (Utils.Print.unspaces Ast.Print.literal)) new_rules) (Utils.Print.list Ast.Print.term args) (Utils.Print.list Variable.print vars) (Utils.Print.unspaces (Utils.Print.list Ast.Print.literal) bodies);
  let original_vars = Utils.PList.uniques (List.concat_map Ast.get_variables args) in
  assert (List.length args = List.length vars);
  assert (Utils.PList.no_doubles vars);
  let var_args = List.combine vars args in
  let replace_bodies bodies rep = Utils.PList.map (Utils.PList.map (Ast.replace_variable_lit rep)) bodies in
  let bodies = List.fold_left replace_bodies bodies var_args in
  let bodies, rules = List.fold_left (cluster_variables original_vars) ([], new_rules) bodies in
  let bodies = Utils.PList.map (Utils.PList.map Ast.negate) bodies in
  eprintf "hola5 %d %s\n%!" (List.length bodies) (Utils.Print.list (fun l -> Utils.Print.int (List.length l)) bodies);
  eprintf "%s\n%!" (Utils.Print.unlines (Utils.Print.list Ast.Print.literal) bodies);
  let bodies = Utils.PList.cross_products bodies in
  eprintf "hola6\n%!";
  (bodies, rules)

let inline_pos args vars bodies =
  assert (List.length args = List.length vars);
  let eqs = List.map2 (fun var arg -> Ast.T.Equal (Ast.T.Variable var, arg)) vars args in
  List.map ((@) eqs) bodies

let inline_literal_aux group new_rules = function
  | Desugared.T.Distinct _ -> None
  | Desugared.T.Negation (Predicate.Does, _) | Desugared.T.Okay (Predicate.Does, _)
  | Desugared.T.Negation (Predicate.True, _) | Desugared.T.Okay (Predicate.True, _) -> None
  | Desugared.T.Okay (pred, args) ->
    assert (Predicate.Map.mem pred group);
    let (vars, bodies) = Predicate.Map.find pred group in
    Some (inline_pos args vars bodies, new_rules)
  | Desugared.T.Negation (pred, args) ->
    let (vars, bodies) = Predicate.Map.find pred group in
    Some (inline_neg new_rules args vars bodies)

let inline_literal param group (accu_lits, new_rules) lit = match inline_literal_aux group new_rules lit with
  | None -> ([[Desugared.sugar_lit lit]] :: accu_lits, new_rules)
  | Some (lits, _) when List.length lits > param -> ([[Desugared.sugar_lit lit]] :: accu_lits, new_rules)
  | Some (lits, rules) -> (lits :: accu_lits, rules)

let is_consistent (_, body) =
  let is_fact : type a. a Desugared.T.literal -> (a Ast.T.atomic, a Ast.T.atomic) Either.t option = function
    | Desugared.T.Distinct _ -> None
    | Desugared.T.Okay fact -> Some (Either.Right fact)
    | Desugared.T.Negation fact -> Some (Either.Left fact) in
  let negs, poss = Utils.PEither.partition_id (List.filter_map is_fact body) in
  Utils.PList.intersect negs poss = []

let inline_clause param group (accu, new_rules) (head, body) =
  let (head, body) = Desugared.cast_clause (head, body) in
  eprintf "cou0 %s %d %d\n%!" (Desugared.Print.clause (head, body)) (List.length accu) (List.length new_rules);
  let inlined_lits, rules = List.fold_left (inline_literal param group) ([], new_rules) body in
  eprintf "cou1\n%!";
  let inlined_lits = List.rev inlined_lits in
  eprintf "cou2\n%!";
  let bodies = Utils.PList.cross_products inlined_lits in
  eprintf "cou3\n%!";
  let clauses = Utils.PList.uniques (Utils.PList.map (fun bod -> (head, Utils.PList.uniques (Utils.PList.flatten bod))) bodies) in
  eprintf "cou4\n%!";
  let clauses = List.concat_map Desugared.from_clause clauses in
  eprintf "cou5\n%!";
  let clauses = Utils.PList.uniques (List.filter is_consistent clauses) in
  eprintf "cou6\n%!";
  List.rev_append clauses accu, rules

let inline_aux param base prog =
  let group = regroup base in
  eprintf "group:\n%s\n%!" (Predicate.Map.print (Utils.Print.couple (Utils.Print.list Utils.Print.string) (Utils.Print.list (Utils.Print.list Ast.Print.literal))) group);
(*  let rules, new_rules = List.fold_left (inline_clause param group) ([], Generate.empty new_predicate) prog in*)
  let rules, new_rules = List.fold_left (inline_clause param group) ([], []) prog in
  List.rev_append rules (List.concat_map Desugared.from_clause new_rules) (*Generate.get new_rules*)

let inline_program param prog = inline_aux param prog prog

let program param prog = if param < 0 then Desugared.cast_program prog else inline_program param prog

(** Things to do: avoid recreating existing rules so that we can use a fixpoint computation. *)

module Grounded = struct
  module Term = Utils.DataStructure.F (struct type t = Ast.T.ground Ast.T.term let compare = compare let print = Ast.Print.term end)
  module GAtomic = Utils.DataStructure.F (struct type t = Ast.T.ground Ast.T.atomic let compare = compare let print = Ast.Print.atomic end)

  let regroup program =
    let add map clause =
      let (head, body) = clause in
      assert (Desugared.get_variables_clause clause = []);
      GAtomic.Map.update head (Utils.PList.cons body) [] map in
    List.fold_left add GAtomic.Map.empty program

  let inline_pos (param1, param2) group atom = match atom with
    | (Predicate.True, _) | (Predicate.Does, _) -> [[Desugared.T.Okay atom]]
    | _ -> let substitute = GAtomic.Map.find_default atom [] group in
           let width = List.length substitute
           and length = Utils.PList.max compare (0 :: Utils.PList.map List.length substitute) in
           if width > param1 || length > param2 then [[Desugared.T.Okay atom]] else substitute

  let negate : Ast.T.ground Desugared.T.literal -> Ast.T.ground Desugared.T.literal = function
    | Desugared.T.Okay atom -> Desugared.T.Negation atom
    | Desugared.T.Negation atom -> Desugared.T.Okay atom

  let inline_neg (param1, param2) group atom = match atom with
    | (Predicate.True, _) | (Predicate.Does, _) -> [[Desugared.T.Negation atom]]
    | _ -> let substitute = GAtomic.Map.find_default atom [] group in
           let width = List.fold_left (fun x y -> x * List.length y) 1 substitute
           and length = List.length substitute in
           if width > param1 || length > param2 then [[Desugared.T.Negation atom]]
           else
             let substitute = Utils.PList.cross_products substitute in
             Utils.PList.map (Utils.PList.map negate) substitute
  let inline_lit param group : Ast.T.ground Desugared.T.literal -> Ast.T.ground Desugared.T.literal list list = function
    | Desugared.T.Okay atomic -> inline_pos param group atomic
    | Desugared.T.Negation atomic -> inline_neg param group atomic

  let inline_rule param (group : (Ast.T.ground Desugared.T.literal list list) GAtomic.Map.t) (head, body) =
    let bodies = Utils.PList.map (inline_lit param group) body in
    let bodies = Utils.PList.cross_products bodies in
    Utils.PList.map (fun body -> (head, Utils.PList.flatten body)) bodies

  let rec program_aux param prog : Ast.T.ground Desugared.T.program =
    let group = regroup prog in
    let prog' = Utils.PList.flatten (Utils.PList.map (inline_rule param group) prog) in
    if prog' <> prog then program_aux param prog' else prog'

  let program param prog =
    let prog' = program_aux param prog in
    Reachability.make Reachability.Graph prog'
end

module FullGround = struct
  module GAtomic = Utils.DataStructure.F (struct type t = Ast.T.ground Ast.T.atomic let compare = compare let print = Ast.Print.atomic end)

  let regroup program =
    let add map clause =
      let (head, body) = clause in
      assert (Desugared.get_variables_clause clause = []);
      GAtomic.Map.update head (Utils.PList.cons body) [] map in
    List.fold_left add GAtomic.Map.empty program

  let negate : Ast.T.ground Desugared.T.literal -> Ast.T.ground Desugared.T.literal = function
    | Desugared.T.Okay atom -> Desugared.T.Negation atom
    | Desugared.T.Negation atom -> Desugared.T.Okay atom

  let inline_lit key value : Ast.T.ground Desugared.T.literal -> Ast.T.ground Desugared.T.literal list list = function
    | Desugared.T.Okay atom | Desugared.T.Negation atom as literal when atom <> key -> [[literal]]
    | Desugared.T.Okay _ -> value
    | Desugared.T.Negation _ -> Utils.PList.map (Utils.PList.map negate) (Utils.PList.cross_products value)

  let inline_clause (key : Ast.T.ground Ast.T.atomic) (value : Ast.T.ground Desugared.T.literal list list) (body : Ast.T.ground Desugared.T.literal list) : Ast.T.ground Desugared.T.literal list list =
    let bodies = Utils.PList.map (inline_lit key value) body in
    let bodies = Utils.PList.cross_products bodies in
    Utils.PList.map Utils.PList.flatten bodies

  let inline_rule atom bodies hyps = List.concat_map (inline_clause atom bodies) hyps

  let candidate targets = GAtomic.Map.exists' (fun (pred, _) _ -> not (List.mem pred targets))
  let candidate targets map =
    let m = GAtomic.Map.filter (fun (pred, _) _ -> not (List.mem pred targets)) map in
    let b = GAtomic.Map.bindings m in
    let b = List.sort (fun (_, hyps1) (_, hyps2) -> compare (List.length hyps1) (List.length hyps2)) b in
    match b with
    | [] -> None
    | a :: _ -> Some a
  let inline targets map =
    let cand = ref (candidate targets map) in
    let m = ref map in
    while !cand <> None do
      match !cand with
      | None -> assert false
      | Some (k, v) ->
         (eprintf "Inlining %s %s\n%!" (Ast.Print.atomic k) (Utils.Print.list (Utils.Print.list Desugared.Print.literal) v);
          (*eprintf "Inlining %s %s\nCurrent map: %s\n%!" (Ast.Print.atomic k) (Utils.Print.list (Utils.Print.list Desugared.Print.literal) v) (GAtomic.Map.print (Utils.Print.list (Utils.Print.list Desugared.Print.literal)) !m);*)
          m := GAtomic.Map.remove k !m;
          m := GAtomic.Map.map (inline_rule k v) !m;
          cand := candidate targets !m)
    done;
    !m

  let remove_void_rules (prog : Ast.T.ground Desugared.T.program) =
    let basis = [Predicate.Does; Predicate.True] in
    let f : Ast.T.ground Desugared.T.literal -> bool = function | Desugared.T.Okay (pred, _) -> List.mem pred basis
        | Desugared.T.Negation _ -> true in
    List.filter (fun (_, body) -> List.for_all f body) prog
  let simplify_rules (prog : Ast.T.ground Desugared.T.program) =
    let basis = [Predicate.Does; Predicate.True] in
    let f : Ast.T.ground Desugared.T.literal -> Ast.T.ground Desugared.T.literal option = function | Desugared.T.Okay _ as atom -> Some atom
        | Desugared.T.Negation (pred, _) as atom -> if List.mem pred basis then None else Some atom in
    Utils.PList.map (fun (head, body) -> (head, List.filter_map f body)) prog
  let program prog : Ast.T.ground Desugared.T.program =
    let ground_prog = Ground.full_program prog in
    let targets = [Predicate.Goal; Predicate.Init; Predicate.Legal; Predicate.Next; Predicate.Role; Predicate.Terminal] in
    let map = regroup ground_prog in
    let map = inline targets map in
    let binds = GAtomic.Map.bindings map in
    let prog = List.concat_map (fun (head, bodies) -> List.map (fun body -> (head, body)) bodies) binds in
    simplify_rules (remove_void_rules prog)
end
