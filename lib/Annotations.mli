type t =
  { perms : GAtomic.Set.t;
    inits : GTerm.Set.t;
    strats : Ast.T.predicate list * Ast.T.predicate list;
    predicates : Predicate.Set.t;
    atoms : Symbol.Set.t;
    roles : Symbol.Set.t;
    frequency : Frequency.t Predicate.Map.t }

val print : t -> string

val starting : 'a Desugared.T.program -> GAtomic.Set.t * GTerm.Set.t * Symbol.Set.t
