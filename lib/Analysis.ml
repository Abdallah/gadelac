open Printf

let rec symbols_term : type a. a Ast.T.term -> Symbol.Set.t = function
  | Ast.T.Variable _ -> Symbol.Set.empty
  | Ast.T.Fact (cst, args) -> Symbol.Set.add cst (Symbol.Set.maps_union symbols_term args)
let symbols_atomic (_, args) = Symbol.Set.maps_union symbols_term args
let symbols_literal : type a. a Desugared.T.literal -> Symbol.Set.t = function
  | Desugared.T.Okay at | Desugared.T.Negation at -> symbols_atomic at
  | Desugared.T.Distinct (_, t) -> symbols_term t
let symbols_rule (head, body) = Symbol.Set.union (symbols_atomic head) (Symbol.Set.maps_union symbols_literal body)
let symbols prog = Symbol.Set.maps_union symbols_rule prog

let predicates_literal : type a. a Desugared.T.literal -> Predicate.Set.t = function
  | Desugared.T.Okay at | Desugared.T.Negation at -> Predicate.Set.singleton (fst at)
  | Desugared.T.Distinct _ -> Predicate.Set.empty
let predicates_rule (head, body) = Predicate.Set.add (fst head) (Predicate.Set.maps_union predicates_literal body)
let predicates prog = Predicate.Set.maps_union predicates_rule prog

let dep_graph_lit : type a. Ast.T.predicate -> Predicate.Set.t Predicate.Map.t -> a Desugared.T.literal -> Predicate.Set.t Predicate.Map.t = fun predicate map -> function
  | Desugared.T.Okay (cst, _) | Desugared.T.Negation (cst, _) -> Predicate.Map.update predicate (Predicate.Set.add cst) Predicate.Set.empty map
  | Desugared.T.Distinct _ -> map
let dep_graph_rule map (head, body) = List.fold_left (dep_graph_lit (fst head)) map body
let dep_graph rules =
  let map = Predicate.Map.init Predicate.Set.empty (Predicate.Set.elements (predicates rules)) in
  List.fold_left dep_graph_rule map rules

let transitive_dep_graph rules =
  let graph = dep_graph rules in
  let deps set = Predicate.Set.union_maps (fun predicate -> Predicate.Map.find_default predicate Predicate.Set.empty graph) set in
  let deps' set = Predicate.Set.union set (deps set) in
  let rec fixpoint acc_g = let new_g = Predicate.Map.map deps' acc_g in if Predicate.Map.compare Predicate.Set.compare acc_g new_g = 0 then acc_g else fixpoint new_g in
  fixpoint graph

let cst_from_literal : type a. a Desugared.T.literal -> Predicate.Set.t = function
  | Desugared.T.Okay (Predicate.True, [Ast.T.Fact (cst, _)])
  | Desugared.T.Negation (Predicate.True, [Ast.T.Fact (cst, _)]) -> Predicate.Set.add (Predicate.Constant cst) (Predicate.Set.singleton Predicate.True)
  | Desugared.T.Okay (cst, _)
  | Desugared.T.Negation (cst, _) -> Predicate.Set.singleton cst
  | Desugared.T.Distinct _ -> Predicate.Set.empty

let cst_from_rule ((cst, _), lits) = Predicate.Set.add cst (Predicate.Set.maps_union cst_from_literal lits)

let start_map rules (f : Ast.T.predicate -> 'a) : 'a Predicate.Map.t =
  let add t = Predicate.Map.add t (f t) in
  let set = Predicate.Set.maps_union cst_from_rule rules in
  Predicate.Set.fold add set Predicate.Map.empty

let from_lit : type a. a Desugared.T.literal -> (Ast.T.predicate, Ast.T.predicate) Either.t option = function
  | Desugared.T.Okay (Predicate.True, [Ast.T.Fact (cst, _)]) -> Some (Either.Right (Predicate.Constant cst))
  | Desugared.T.Negation (Predicate.True, [Ast.T.Fact (cst, _)]) -> Some (Either.Left (Predicate.Constant cst))
  | Desugared.T.Okay (cst, _) -> Some (Either.Right cst)
  | Desugared.T.Negation (cst, _) -> Some (Either.Left cst)
  | Desugared.T.Distinct _ -> None

let get_cst_literals ((cst, _), literals) =
  let negs, poss = Utils.PEither.partition_id (List.filter_map from_lit literals) in
  (cst, poss, negs)

let new_value f old_map (cst, poss, negs) =
  let old = Predicate.Map.find cst old_map in
  let get_val c = Predicate.Map.find c old_map in
  let poss = Utils.PList.map get_val poss
  and negs = Utils.PList.map get_val negs in
  Predicate.Map.add cst (f old poss negs) old_map

let rec fixpoint_aux rules start f =
  let result = List.fold_left (new_value f) start rules in
  if Predicate.Map.equal (=) start result then result
  else fixpoint_aux rules result f

let fixpoint prog strat f =
  let rules = Utils.PList.map get_cst_literals prog in
  try fixpoint_aux rules strat f with exn -> eprintf "%s\n%!" (Printexc.to_string exn); assert false
