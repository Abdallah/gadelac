open Printf

type 'a branch = { unive : Variable.t list;
                   exist : Variable.t list;
                   equal : 'a Ast.T.term list;
                   hyps : 'a Desugared.T.literal list }
type 'a program = 'a branch list Predicate.Map.t

let add_clause accu clause =
  let (head, body) = clause in
  let (pred, args) = head in
  let head_vars = Desugared.get_variables_lit (Desugared.T.Okay head)
  and body_vars = Desugared.get_variables_clause (head, body) in
  let rest_vars = List.filter (fun v -> not (List.mem v head_vars)) body_vars in
  let branch =
  { unive = head_vars;
    exist = rest_vars;
    equal = args;
    hyps = body } in
  Predicate.Map.update pred (Utils.PList.cons branch) [] accu

let completion inputs prog =
  let inputs = Predicate.Set.of_list inputs in
  let preds = Predicate.Set.diff (Desugared.collect_predicates prog) inputs in
  let map = Predicate.Map.from_set (fun _ -> []) preds in
  List.fold_left add_clause map prog

module FOL = FirstOrder

let literal = function
  | Desugared.T.Okay atom -> FOL.Atom atom
  | Desugared.T.Negation atom -> FOL.Nega (FOL.Atom atom)
  | Desugared.T.Distinct (var, ter) -> FOL.Dist (Ast.T.Variable var, ter)
let branch b =
  let hyps = List.map literal b.hyps in
  let equals =
    let print_one i v = FOL.Equa (Ast.T.Variable (sprintf "%d" i), v) in
    let (_, eqs) = List.fold_left (fun (i, acc) v -> (i + 1, print_one i v :: acc)) (0, []) b.equal in
    eqs in
  FOL.Fora (b.unive, FOL.Exis (b.exist, FOL.Conj (equals @ hyps)))

let program prog =
  let inputs = [Predicate.True; Predicate.Does] in
  let compl = completion inputs (Desugared.cast_program prog) in
  let binds = Predicate.Map.bindings compl in
  let print_one (pred, bs) =
    let variables = List.map (sprintf "%d") (Utils.PList.range 0 (Ast.arity_pred pred)) in
    let left_hand = FOL.Atom (pred, List.map Ast.variable variables) in
    let right_hand = FOL.Disj (List.map branch bs) in
    { FOL.name = "def_" ^ Ast.Print.predicate pred;
      body = FOL.Fora (variables, FOL.Equi (left_hand, right_hand)) } in
  List.map print_one binds
