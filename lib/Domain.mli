module type S =
sig
  type t
  type substitutions

  val empty : t
  val unit : substitutions

  val equal : t -> t -> bool
  val print : t -> string
  val print_subs : substitutions -> string
  val unify : 'a Ast.T.atomic -> t -> substitutions
  val compose : t -> substitutions -> 'a Ast.T.atomic -> substitutions
  val neg_compose : t -> substitutions -> 'a Ast.T.atomic -> substitutions
  val dist_compose : substitutions -> string -> 'a Ast.T.term -> substitutions
  val add_subs : substitutions -> 'a Ast.T.atomic -> t -> t
  val consistent : substitutions -> bool
end

module Exact : sig include S with type substitutions = Ast.T.free Unification.substitution list val values : t -> Ast.T.free Ast.T.atomic list end
module Path  : sig include S val develop : substitutions -> Symbol.t list Variable.Map.t option end
module Graph : S with type t = Symbol.Set.t Utils.Int.Map.t Predicate.Map.t * Symbol.Set.t Utils.Int.Map.t Symbol.Map.t
