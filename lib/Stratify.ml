open Printf

type 'a program = { rules : 'a Desugared.T.program;
                    strats : (Ast.T.predicate list * Ast.T.predicate list) }

let print prog =
  sprintf "%s\n%s\n" (Desugared.Print.program prog.rules) (Utils.Print.couple (Utils.Print.list Predicate.print) (Utils.Print.list Predicate.print) prog.strats)

let stratified prog =
  let dep_graph = Analysis.transitive_dep_graph prog in
  let forbidden_lit : type a. Ast.T.predicate -> a Desugared.T.literal -> (Ast.T.predicate * Ast.T.predicate) option = fun head_pred -> function
    | Desugared.T.Okay _ -> None
    | Desugared.T.Distinct _ -> None
    | Desugared.T.Negation (pred, _) -> Some (head_pred, pred) in
  let forbidden_rule (head, body) = List.filter_map (forbidden_lit (fst head)) body in
  let forbidden = List.concat_map forbidden_rule prog in
  let safe (head, pred) = not (Predicate.Set.mem head (Predicate.Map.find pred dep_graph)) in
  List.for_all safe forbidden

let strat_terms_gen rules =
  let start = Analysis.start_map rules (fun _ -> 0) in
  let f old poss negs = List.fold_left max old (Utils.PList.append (Utils.PList.map ((+) 1) negs) poss) in
  Analysis.fixpoint rules start f

let next_does = (Predicate.Next, []), [Desugared.T.Okay (Predicate.Does, [])]
let does_legal = (Predicate.Does, []), [Desugared.T.Okay (Predicate.Legal, [])]

let strat_terms prog =
  if not (stratified prog) then invalid_arg "Not stratified input!";
  let prog = does_legal :: next_does :: prog in
  assert (stratified prog);
  strat_terms_gen prog

let strat_fun i = Predicate.Constant (sprintf "strat%d" i, 0, 0)

let add_stratificator strats frequency (ccl, hyps) =
  let aux : type a. a Desugared.T.literal list -> a Desugared.T.literal -> a Desugared.T.literal list = fun accu lit -> match lit with
    | Desugared.T.Okay _ -> lit :: accu
    | Desugared.T.Distinct _ -> lit :: accu
    | Desugared.T.Negation (predicate, _) ->
      assert (Predicate.Map.mem predicate strats);
      let strat = Predicate.Map.find predicate strats in
      let strat_hyp = Desugared.T.Okay (strat_fun strat, []) in
      if List.mem strat_hyp accu then lit :: accu else lit :: strat_hyp :: accu in
  match hyps with
  | [] -> if Predicate.Map.find (fst ccl) frequency = Frequency.Ephemeral then (ccl, [Desugared.T.Okay (strat_fun 0, [])]) else (ccl, [])
  | _ -> (ccl, List.rev (List.fold_left aux [] hyps))

let add_stratificators param rules =
  let strats = strat_terms rules in
  let frequency = Frequency.frequency param rules in
  assert (Predicate.Map.mem Predicate.Legal strats && Predicate.Map.mem Predicate.Next strats);
  let earl_strat = List.map strat_fun (Utils.PList.range 0 (Predicate.Map.find Predicate.Legal strats))
  and late_strat = List.map strat_fun (Utils.PList.range (Predicate.Map.find Predicate.Legal strats) (Predicate.Map.find Predicate.Next strats)) in
  let rules = List.map (add_stratificator strats frequency) rules in
  (rules, earl_strat, late_strat)

let make param prog =
  let rules, earl_strats, late_strats = add_stratificators param prog in
  { rules;
    strats = (earl_strats, late_strats) }

