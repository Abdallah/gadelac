type param = unit
val print_param : param -> string
val read_param : string -> param

type unify =
  { bindings : (Scope.T.variable * Scope.T.variable) list;
    terms : Ast.T.free Ast.T.term list }

type hypothesis =
  | Okay
  | IfExists of (Ast.T.predicate * Scope.T.variable)
  | IfUnify  of (Ast.T.predicate * unify)
  | IfNot    of (Ast.T.predicate * Scope.T.variable)
  | IfDist   of (Scope.T.variable * Scope.T.variable)

type conclusion = FunCall of Ast.T.predicate * Scope.T.variable option | Next of Ast.T.predicate * Scope.T.variable
type action = hypothesis * Scope.T.affectation list * conclusion list
type branch = Ast.T.free Ast.T.term list * Scope.T.affectation list * action list
type matching = branch list list

type program =
  { global : Scope.T.affectation list;
    funs : matching Predicate.Map.t;
    inits : Scope.T.variable list option Predicate.Map.t;
    perms : Scope.T.variable list option Predicate.Map.t;
    annot : Annotations.t }

module Print :
sig
  val hypothesis : hypothesis -> string
  val action : action -> string
  val branch : branch -> string
  val matching : matching -> string
  val program : program -> string
end

val make : param -> Grouping.program -> program
