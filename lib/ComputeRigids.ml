module NaiveE = Reachability.Naive (Domain.Exact)

let get program =
  let perms = Frequency.permanents program in
  let is_permanent ((pred, _), _) = Predicate.Set.mem pred perms in
  let (perm_rules, other) = List.partition is_permanent program in
  let perms, _time = Utils.Time.measure (fun rules -> Domain.Exact.values (NaiveE.forward true rules)) perm_rules in
  let new_rules = List.sort compare (Utils.PList.map (fun a -> a, []) perms) in
  new_rules @ Desugared.cast_program other
