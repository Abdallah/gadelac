open Printf

module CLI = CommandLine

type ('input, 'output, 'param) pass_data =
  { name      : string;
    printer   : 'output -> string;
    transform : 'param -> 'input -> 'output;
    parameter : CLI.param -> 'param;
    debug : CLI.debug -> bool }

let spec_parsed =
  { name      = "parsed";
    printer   = Ast.Print.program;
    transform = Desugared.Parse.from_file;
    parameter = (fun _ -> ());
    debug = fun d -> d.CLI.parsed }
let spec_desugared =
  { name      = "desugared";
    printer   = Desugared.Print.program;
    transform = Desugared.make;
    parameter = (fun _ -> ());
    debug = fun d -> d.CLI.desugared }
let spec_optimized =
  { name      = "optimized";
    printer   = Desugared.Print.program;
    transform = Optimization.make;
    parameter = (fun p -> p.CLI.poptimized);
    debug = fun d -> d.CLI.optimized }
let spec_stratified =
  { name      = "stratified";
    printer   = Stratify.print;
    transform = Stratify.make;
    parameter = (fun p -> p.CLI.pinverted);
    debug = fun d -> d.CLI.stratified }
let spec_normalized =
  { name      = "normalizd";
    printer   = Normal.Print.program;
    transform = Normal.make;
    parameter = (fun p -> p.CLI.pinverted);
    debug = fun d -> d.CLI.normalized }
let spec_inverted =
  { name      = "inverted";
    printer   = Inversion.Print.program;
    transform = Inversion.make;
    parameter = (fun _ -> ());
    debug = fun d -> d.CLI.inverted }
let spec_grouped =
  { name      = "grouped";
    printer   = Grouping.Print.program;
    transform = Grouping.make;
    parameter = (fun p -> p.CLI.pgrouped);
    debug = fun d -> d.CLI.grouped }
let spec_preinstruc =
  { name      = "preinstructed";
    printer   = PreInstruction.Print.program;
    transform = PreInstruction.make;
    parameter = (fun _ -> ());
    debug = fun d -> d.CLI.preinstruc }
let spec_instructed =
  { name      = "instructed";
    printer   = Instructions.Print.program;
    transform = Instructions.make;
    parameter = (fun p -> p.CLI.pinstructed);
    debug = fun d -> d.CLI.instructed }

let output_general spec options input =
  let { CLI.param; debug; verbose; input_name } = options in
  let (output, time) = Utils.Time.measure (spec.transform (spec.parameter param)) input in
  if !verbose >= 1 then eprintf "\t%s %s in %.3f sec\n" !input_name spec.name time; flush stderr;
  if spec.debug debug then eprintf "DEBUG %s for %s:\n%s" spec.name !input_name (spec.printer output);
  output

let o_parsed     options = output_general spec_parsed     options
let o_desuga     options = output_general spec_desugared  options
let o_optimi     options = output_general spec_optimized  options
let o_strati     options = output_general spec_stratified options
let o_normal     options = output_general spec_normalized options
let o_inverted   options = output_general spec_inverted   options
let o_grouped    options = output_general spec_grouped    options
let o_preinstruc options = output_general spec_preinstruc options
let o_instructed options = output_general spec_instructed options

let transform_instructed options input = o_instructed options input
let transform_preinstruc options input = transform_instructed options (o_preinstruc options input)
let transform_grouped    options input = transform_preinstruc options (o_grouped    options input)
let transform_inverted   options input = transform_grouped    options (o_inverted   options input)
let transform_normalized options input = transform_inverted   options (o_normal     options input)
let transform_stratified options input = transform_normalized options (o_strati     options input)
let transform_optimized  options input = transform_stratified options (o_optimi     options input)
let transform_desugared  options input = transform_optimized  options (o_desuga     options input)
let transform_parsed     options input = transform_desugared  options (o_parsed     options input)

let to_parsed     options input = o_parsed     options input
let to_desugared  options input = o_desuga     options (to_parsed     options input)
let to_optimized  options input = o_optimi     options (to_desugared  options input)
let to_stratified options input = o_strati     options (to_optimized  options input)
let to_normalized options input = o_normal     options (to_stratified options input)
let to_inverted   options input = o_inverted   options (to_normalized options input)
let to_grouped    options input = o_grouped    options (to_inverted   options input)
let to_preinstruc options input = o_preinstruc options (to_grouped    options input)
let to_instructed options input = o_instructed options (to_preinstruc options input)
