include module type of Types.INSTRUCTIONS with module T = Types.INSTRUCTIONS.T


module Print:
sig
  val program : T.program -> string
end

type param = string

val make : param -> PreInstruction.program -> T.program
