type 'a t =
  | Conj of 'a t list
  | Disj of 'a t list
  | Atom of 'a Ast.T.atomic
  | Exis of (Variable.t list * 'a t)
  | Fora of (Variable.t list * 'a t)
  | Nega of 'a t
  | Dist of ('a Ast.T.term * 'a Ast.T.term)
  | Equa of ('a Ast.T.term * 'a Ast.T.term)
  | Impl of ('a t * 'a t)
  | Equi of ('a t * 'a t)

type 'a axiom =
  { name : string;
    body : 'a t; }

type 'a program = 'a axiom list

module Print :
sig
  val t : 'a t  -> string
  val axiom  : 'a axiom  -> string
  val program : 'a program -> string
end
