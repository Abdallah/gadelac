module AST = struct module T = struct
(** (name, current_arity, original_arity) *)
type symbol = Symbol.t
type predicate = Predicate.t
type free = Free
type ground = Ground
type _ term = Variable : string -> free term | Fact : (symbol * 'a term list) -> 'a term
type 'a atomic = predicate * 'a term list
type 'a literal = Okay of 'a atomic | Negation of 'a atomic | Distinct of ('a term * 'a term) | Equal of ('a term * 'a term)
type 'a clause = ('a atomic * 'a literal list)
type 'a program = ('a atomic * 'a literal Logic.t) list
end end

module DESUGARED = struct module T = struct
type _ literal = Okay : 'a AST.T.atomic -> 'a literal | Negation : 'a AST.T.atomic -> 'a literal | Distinct : (string * AST.T.free AST.T.term) -> AST.T.free literal
(*type 'a literal = Okay of 'a Ast.T.atomic | Negation of 'a Ast.T.atomic | Distinct of (string * 'a Ast.T.term)*)
type fliteral = AST.T.free literal
type 'a clause = 'a AST.T.atomic * 'a literal list
type 'a program = 'a clause list
end end

module SCOPE = struct module T = struct
type var_type = [ `TTerm | `TFTerm | `TArray | `TFArray]
type variable = string

type affectation =
  | Cons  of (variable * AST.T.symbol)
  | Term  of (variable * AST.T.symbol * variable)
  | Array of (variable * variable list)
end end

module INSTRUCTIONS = struct module T = struct
type types = | TState | TString | TBool | TPredicate | Void

type typedef =
  | Predicates of Predicate.Set.t
  | Symbols    of Symbol.Set.t
  | KB         of Predicate.Set.t

type dummy_var = | DummyVar of (SCOPE.T.var_type * SCOPE.T.variable) | FunConstant of AST.T.symbol | OtherVar of (types * string)

type local_function = | AddFact | FPredicate of AST.T.predicate | Initialization | MakeMove | PredicateToString | Print | PrintFact  | Restart | SymbolToString

type qualified_function =
  | KBAdd     of (dummy_var * AST.T.predicate * dummy_var)
  | KBNext    of (dummy_var * AST.T.predicate * dummy_var)
  | KBGet     of (dummy_var * AST.T.predicate)
  | KBClear   of (dummy_var * AST.T.predicate)
  | KBCreate  of AST.T.predicate
  | KBPrint   of dummy_var
  | KBPrintArray of dummy_var
  | LocalCall of (local_function * dummy_var list)
  | PrintErr  of (AST.T.predicate * dummy_var)

type base_expr = | String of string | Bool of bool | Predicate of AST.T.predicate | Symbol of AST.T.symbol | Variable of dummy_var

type match_branch = | MBTermTuple of AST.T.free AST.T.term list | MBTerm of AST.T.free AST.T.term | MBBase of base_expr | Default

type construction =
  | CFact   of (SCOPE.T.variable * AST.T.symbol * SCOPE.T.variable option)
  | CRecord of (dummy_var * (AST.T.predicate * dummy_var) list)
  | CArray  of (dummy_var * dummy_var list)

type expr =
  | KBExists    of (dummy_var * AST.T.predicate * dummy_var)
  | Not         of expr
  | Conjunction of expr list
  | Equal       of (dummy_var * dummy_var)
  | Different   of (dummy_var * dummy_var)

type stmt =
  | Construct   of construction
  | Iterate     of (dummy_var * stmt * dummy_var)
  | SFunCall    of qualified_function
  | ValueDef    of (dummy_var * qualified_function)
  | ValueUpdate of (dummy_var * qualified_function)
  | Sequence    of stmt list
  | IfThenElse  of (expr * stmt * stmt)
  | Match       of (dummy_var * (match_branch * stmt) list)
  | Return      of base_expr
  | Free        of dummy_var
  | Noop
  | Abort

type global =
  | Typedef         of (string * typedef)
  | TypeSdef        of (string * SCOPE.T.var_type)
  | GlobalValueDef  of (dummy_var * base_expr)
  | GlobalConstruct of construction
  | GlobalFunDef    of (local_function * dummy_var list * stmt)
  | Recursive       of (AST.T.predicate * dummy_var list * stmt) list

type program = global list
end end
