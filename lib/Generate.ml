open Printf

let deprintf format = if false then eprintf format else ifprintf stderr format

module Hyps =
struct
  module Ord = struct type t = string list * Ast.T.free Desugared.T.literal list 
                      let compare = compare 
                      let print = Utils.Print.couple (Utils.Print.list Utils.Print.string) (Utils.Print.unspaces Desugared.Print.literal) end
  include Utils.DataStructure.F (Ord)
end

type t = (int -> Ast.T.predicate) * Ast.T.free Ast.T.atomic Hyps.Map.t

let make_head predicate required_vars lits =
  let vars = List.concat_map Desugared.get_variables_lit lits in
  assert (List.for_all (Utils.PList.memr vars) required_vars);
  let required_vars = List.sort compare required_vars in
  let pred = predicate (List.length required_vars) in
  (pred, Utils.PList.map Ast.variable required_vars)

let replace_vars map = Utils.PList.map (Desugared.rename_variables_lit map)

let choose_one =
  let rec aux choices before = function
    | [] -> List.rev choices
    | a :: after -> aux ((a, List.rev_append before after) :: choices) (a :: before) after in
  aux [] []

let rec unify_args map b1 b2 = match b1, b2 with
  | [], [] -> Some map
  | [], _ :: _ | _ :: _, [] -> assert false
  | Ast.T.Fact a1 :: r1, Ast.T.Fact (s2, a2) :: r2 -> if a1 = (s2, Utils.PList.map (Desugared.rename_variables_term map) a2) then unify_args map r1 r2 else None
  | Ast.T.Fact _ :: _, Ast.T.Variable _ :: _ | Ast.T.Variable _ :: _, Ast.T.Fact _ :: _ -> None
  | Ast.T.Variable v1 :: r1, Ast.T.Variable v2 :: r2 ->
    if Variable.Map.mem v2 map
    then if v1 = Variable.Map.find v2 map then unify_args map r1 r2 else None
    else if List.mem v1 (Variable.Map.values map) then None
    else unify_args (Variable.Map.add v2 v1 map) r1 r2

let rec unify_body b1 b2 map = match b1, b2 with
  | [], [] -> [map]
  | [], _ :: _ -> []
  | a :: r, _ -> Utils.PList.filter_maps (unify_hyp map (a, r)) (choose_one b2)
and unify_hyp map (a1, b1) (a2, b2) = match a1, a2 with
  | Desugared.T.Okay (p1, _), Desugared.T.Okay (p2, _)
  | Desugared.T.Negation (p1, _), Desugared.T.Negation (p2, _) when p1 <> p2 -> None
  | Desugared.T.Okay _, Desugared.T.Negation _ | Desugared.T.Okay _, Desugared.T.Distinct _
  | Desugared.T.Negation _, Desugared.T.Okay _ | Desugared.T.Negation _, Desugared.T.Distinct _
  | Desugared.T.Distinct _, Desugared.T.Okay _ | Desugared.T.Distinct _, Desugared.T.Negation _ -> None
  | Desugared.T.Negation (_, args1), Desugared.T.Negation (_, args2)
  | Desugared.T.Okay     (_, args1), Desugared.T.Okay     (_, args2) -> Option.map (unify_body b1 b2) (unify_args map args1 args2)
  | Desugared.T.Distinct (v1, t1),   Desugared.T.Distinct (v2, t2)   -> Option.map (unify_body b1 b2) (unify_args map [Ast.T.Variable v1; t1] [Ast.T.Variable v2; t2])

let unify vars1 body1 (vars2, body2) head =
  assert (Utils.PList.no_doubles vars1 && Utils.PList.no_doubles vars2);
  if List.length vars1 <> List.length vars2 then None
  else
    let vars = List.combine vars2 vars1 in
    let map = Variable.Map.from_bindings vars in
    match unify_body body1 body2 map with
    | [] -> None
    | a :: r ->
      if r <> [] then eprintf "Generate many unifs: %d %s\n" (1 + List.length r) (Utils.Print.unspaces (Variable.Map.print Utils.Print.string) (a :: r));
      Some (Desugared.rename_variables_atomic a head)

let add_rule (predicates, rules) vars body =
  let body = List.map Desugared.cast_literal body in
  let substitutions = Hyps.Map.filter_map (unify vars body) rules in
  match Hyps.Map.values substitutions with
  | [] ->
    let head = make_head predicates vars body in
    deprintf "Generate created rule:\n%s\n%!" (Desugared.Print.clause (head, body));
    ((predicates, Hyps.Map.add (vars, body) head rules), head)
  | head :: [] -> ((predicates, rules), head)
  | _ :: _ :: _ -> assert false

let empty predicates = (predicates, Hyps.Map.empty)
let get (_, rules) = Hyps.Map.fold (fun (_, body) head accu -> (head, body) :: accu) rules []

let variable prefix =
  let counter = ref (-1) in
  let next () =
    incr counter;
    sprintf "%s%d" prefix !counter in
  next
let predicate prefix =
  let counter = ref (-1) in
  let next arity =
    incr counter;
    let name = sprintf "%s%d" prefix !counter in
    Predicate.Constant (name, arity, arity) in
  next
