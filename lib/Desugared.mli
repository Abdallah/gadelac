module Parse :
sig
  val from_file : unit -> string -> Ast.T.free Ast.T.program
end

include module type of Types.DESUGARED with module T = Types.DESUGARED.T

module Print :
sig
  val literal : 'a T.literal -> string
  val clause  : 'a T.clause  -> string
  val program : 'a T.program -> string
end

val get_variables_lit : 'a T.literal -> string list
val get_variables_clause : 'a T.clause -> string list
val is_variable : 'a Ast.T.term -> bool
val only_free_arguments : Ast.T.symbol * 'a Ast.T.term list -> bool
val ground : 'a Ast.T.term -> Ast.T.ground Ast.T.term option
val ground_clause : 'a T.clause -> Ast.T.ground Ast.T.atomic (** Assumes an empty body *)
val get_fun : Ast.T.ground Ast.T.term -> Ast.T.symbol
val from_clause : 'a Ast.T.atomic * 'a Ast.T.literal list -> 'a T.clause list
val make : unit -> 'a Ast.T.program -> 'a T.program

val cast_literal : 'a T.literal -> Ast.T.free T.literal
val cast_clause  : 'a T.clause  -> Ast.T.free T.clause
val cast_program : 'a T.program -> Ast.T.free T.program

(** returns [None] when it is necessarily true, or [Some l] if it is true with [l] being the conditions. *)
(*val simplify_distinct : Ast.T.fterm -> Ast.T.fterm -> (string * Ast.T.fterm) list option*)

val simplify_distinct : 'b Unification.substitution -> ('a Ast.T.term * 'a Ast.T.term) -> 'a T.literal list list

val sugar_lit : 'a T.literal -> 'a Ast.T.literal

val rename_variables_term : string Variable.Map.t -> 'a Ast.T.term -> 'a Ast.T.term
val rename_variables_atomic : string Variable.Map.t -> 'a Ast.T.atomic -> 'a Ast.T.atomic
val rename_variables_lit : string Variable.Map.t -> 'a T.literal -> 'a T.literal
val apply_clause : 'b Unification.substitution -> 'a T.clause -> 'a T.clause list

val get_positive : Ast.T.free T.clause -> (Ast.T.predicate * Ast.T.free Ast.T.term list) list

val collect_predicates : 'a T.program -> Predicate.Set.t
