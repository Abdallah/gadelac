open Printf

type 'a t =
  | Conj of 'a t list
  | Disj of 'a t list
  | Atom of 'a Ast.T.atomic
  | Exis of (Variable.t list * 'a t)
  | Fora of (Variable.t list * 'a t)
  | Nega of 'a t
  | Dist of ('a Ast.T.term * 'a Ast.T.term)
  | Equa of ('a Ast.T.term * 'a Ast.T.term)
  | Impl of ('a t * 'a t)
  | Equi of ('a t * 'a t)

type 'a axiom =
  { name : string;
    body : 'a t; }

type 'a program = 'a axiom list

module Print =
struct
  let escape = Utils.MString.escape ['-'; '+'; '<'; '>'] '_'
  let symbol symb = escape (Ast.Print.symbol symb)
  let predicate pred = escape (Ast.Print.predicate pred)
  let variable v = escape (sprintf "V%s" v)
  let rec term : type a. (a Ast.T.term) -> string = function
    | Ast.T.Variable v -> variable v
    | Ast.T.Fact (s, []) -> symbol s
    | Ast.T.Fact (s, args) -> sprintf "%s%s" (symbol s) (Utils.Print.list' "(" "," ")" term args)
  let atom = function
    | (pred, []) -> predicate pred
    | (pred, args) -> sprintf "%s%s" (predicate pred) (Utils.Print.list' "(" "," ")" term args)
  let rec t = function
    | Conj [] -> "true"
    | Conj (a :: []) | Disj (a :: []) -> t a
    | Conj l -> Utils.Print.list' "(" " & " ")" t l
    | Disj [] -> "FALSE"
    | Disj l -> Utils.Print.list' "(" " | " ")" t l
    | Atom a -> atom a
    | Exis ([], a) | Fora ([], a) -> t a
    | Exis (vs, a) -> sprintf "(%s : %s)" (Utils.Print.list' "?[" ", " "]" variable vs) (t a)
    | Fora (vs, a) -> sprintf "(%s : %s)" (Utils.Print.list' "![" ", " "]" variable vs) (t a)
    | Nega (Atom a) -> sprintf "~%s" (atom a)
    | Nega a -> sprintf "~(%s)" (t a)
    | Dist (t1, t2) -> sprintf "(%s != %s)" (term t1) (term t2)
    | Equa (t1, t2) -> sprintf "(%s = %s)" (term t1) (term t2)
    | Impl (a1, a2) -> sprintf "(%s => %s)" (t a1) (t a2)
    | Equi (a1, a2) -> sprintf "(%s <=> %s)" (t a1) (t a2)
  let axiom { name; body } =
    sprintf "fof(%s, axiom, %s)." (escape name) (t body)
  let program prog = Utils.Print.unlines axiom prog
end
