open Printf

include Types.DESUGARED
open T

let cast_literal : type a. a literal -> Ast.T.free literal = function
  | Okay atom -> Okay (Ast.cast_atomic atom)
  | Negation atom -> Negation (Ast.cast_atomic atom)
  | Distinct (v, term) -> Distinct (v, term)
let cast_clause (head, body) = (Ast.cast_atomic head, List.map cast_literal body)
let cast_program prog = Utils.PList.map cast_clause prog

let get_fun : Ast.T.ground Ast.T.term -> Ast.T.symbol = function (Ast.T.Fact (s, _)) -> s

let get_variables_lit : type a. a literal -> string list = function
  | Okay atom | Negation atom -> Utils.PList.uniques (Ast.get_variables_atomic atom)
  | Distinct (var, term) -> Utils.PList.uniques (var :: Ast.get_variables term)
let get_variables_clause (head, body) =
  let head_vars = get_variables_lit (Okay head)
  and body_vars = Utils.PList.uniques (List.concat_map get_variables_lit body) in
  assert (List.for_all (Utils.PList.memr body_vars) head_vars);
  body_vars

let is_variable : type a. a Ast.T.term -> bool = function
  | Ast.T.Variable _ -> true
  | Ast.T.Fact _ -> false

let only_free_arguments (_, ts) = List.for_all is_variable ts && Utils.PList.no_doubles (List.concat_map Ast.get_variables ts)

module Print =
struct
  let literal : type a. a literal -> string = function
    | Okay t -> Ast.Print.atomic t
    | Negation t -> sprintf "(not %s)" (Ast.Print.atomic t)
    | Distinct (s, t) -> sprintf "(distinct ?%s %s)" s (Ast.Print.term t)

  let clause (l, rs) =
    if rs = [] then Ast.Print.atomic l
    else sprintf "(<= %s %s)" (Ast.Print.atomic l) (Utils.Print.unspaces literal rs)

  let program rules = Utils.Print.unlines clause rules
end

let distincts dist =
  let rec aux : type a. (a Ast.T.term * a Ast.T.term) -> a literal list list = function
    | Ast.T.Variable var1, Ast.T.Variable var2 when var1 = var2 -> []
    | Ast.T.Variable var, term -> [[Distinct (var, term)]]
    | term, Ast.T.Variable var -> [[Distinct (var, term)]]
    | Ast.T.Fact (symb1, _), Ast.T.Fact (symb2, _) when symb1 <> symb2 -> [[]]
    | Ast.T.Fact (_, terms1), Ast.T.Fact (_, terms2) ->
      assert (List.length terms1 = List.length terms2);
      List.concat_map aux (List.combine terms1 terms2) in
  Utils.PList.uniques (aux dist)

let simplify_distinct sub (t1, t2) = distincts (Unification.apply_term sub t1, Unification.apply_term sub t2)

let partition_literals : 'a Ast.T.literal list -> ('a Unification.substitution * (('a Ast.T.term * 'a Ast.T.term, 'a literal) Either.t list)) option = fun body ->
  let aux (accu_sub, accu_body) = function
    | Ast.T.Okay     atom -> accu_sub, Either.Right (Okay     atom) :: accu_body
    | Ast.T.Negation atom -> accu_sub, Either.Right (Negation atom) :: accu_body
    | Ast.T.Distinct dist -> accu_sub, Either.Left dist :: accu_body
    | Ast.T.Equal eq -> Option.bind accu_sub (fun sub -> Unification.add_equality sub eq), accu_body in
  let sub, body = List.fold_left aux (Unification.empty, []) body in
  match sub with
  | None -> None
  | Some sub -> Some (sub, body)

let from_clause (head, body) = match partition_literals body with
  | None -> []
  | Some (sub, body) ->
    let aux : type a. a literal list list -> (a Ast.T.term * a Ast.T.term, a literal) Either.t -> a literal list list = fun accu -> function
      | Either.Right Okay     fact -> Utils.PList.map (Utils.PList.cons (Okay     (Unification.apply_atomic sub fact))) accu
      | Either.Right Negation fact -> Utils.PList.map (Utils.PList.cons (Negation (Unification.apply_atomic sub fact))) accu
      | Either.Right Distinct _ -> assert false
      | Either.Left dist ->
        let distincts = simplify_distinct sub dist in
        Utils.PList.cross_product_map (@) distincts accu in
    let bodies = List.fold_left aux [[]] body in
    let head = Unification.apply_atomic sub head in
    Utils.PList.map (fun body -> (head, body)) bodies

let from_logic_rule (head, (logic : 'a Ast.T.literal Logic.t)) =
  let logs = Logic.dnf logic in
  let logs = Utils.PList.map (fun log -> (head, log)) logs in
  List.concat_map from_clause logs

let from_logic_program rules = List.concat_map from_logic_rule rules

let rec ground : type a. a Ast.T.term -> Ast.T.ground Ast.T.term option = function
  | Ast.T.Variable _ -> None
  | Ast.T.Fact (f, ts) ->
    try let gts = List.filter_map ground ts in
        assert (List.length gts = List.length ts);
        Some (Ast.T.Fact (f, gts))
    with Assert_failure _ -> None

let ground_clause (head, body) =
  assert (body = []);
  let grounds = List.filter_map ground (snd head) in
  assert (List.length grounds = List.length (snd head));
  (fst head, grounds)

let make () prog = from_logic_program prog

let sugar_lit : type a. a literal -> a Ast.T.literal = function
  | Okay arg -> Ast.T.Okay arg
  | Negation arg -> Ast.T.Negation arg
  | Distinct (v, t) -> Ast.T.Distinct (Ast.T.Variable v, t)

let rename_variables_term map term =
  let aux key elt = Ast.replace_variable_term (key, Ast.T.Variable elt) in
  Variable.Map.fold aux map term
let rename_variables_atomic map atom = Unification.apply_atomic (Variable.Map.map Ast.variable map) atom
let rename_variables_lit : type a. string Variable.Map.t -> a literal -> a literal = fun map -> function
  | Okay atom -> Okay (rename_variables_atomic map atom)
  | Negation atom -> Negation (rename_variables_atomic map atom)
  | Distinct (v, term) -> Distinct (Variable.Map.find_default v v map, rename_variables_term map term)

let apply_clause sub (head, body) =
  let head = Unification.apply_atomic sub head
  and body = List.map (fun lit -> Unification.apply_literal sub (sugar_lit lit)) body in
  from_clause (head, body)

let get_positive (_, body) =
  let aux accu = function
    | Okay atom -> atom :: accu
    | Negation _ | Distinct _ -> accu in
  List.rev (List.fold_left aux [] body)

let collect_predicates_literal : type a. Predicate.Set.t -> a literal -> Predicate.Set.t = fun accu -> function
  | Okay (pred, _) -> Predicate.Set.add pred accu
  | Negation _ -> accu
  | Distinct _ -> accu
let collect_predicates_clause accu (head, body) =
  let (pred, _) = head in
  List.fold_left collect_predicates_literal (Predicate.Set.add pred accu) body
let collect_predicates prog = List.fold_left collect_predicates_clause Predicate.Set.empty prog

module Parse =
struct
  let print_error name lexer =
    let position = Lexing.lexeme_start_p lexer in
    let line = position.Lexing.pos_lnum
    and char = position.Lexing.pos_cnum - position.Lexing.pos_bol in
    sprintf "%s: Parser : at line %d, column %d: syntax error.\n" name line char

  let from_file () name =
    let input = if name <> "-" then open_in name else stdin in
    let close () = if name <> "-" then close_in input in
    let lexer = Lexing.from_channel input in
    let tree =
      try Parser.program Lexer.token lexer with
      | Lexer.Error msg -> close (); invalid_arg (sprintf "%s: %s" name msg)
      | Parser.Error -> close (); invalid_arg (print_error name lexer)
      | exn -> close (); raise exn in
    close ();
    Ast.program tree
end

