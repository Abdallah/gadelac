{
  module P = Parser
  exception Error of string

  let incr_linenum lexbuf =
    let pos = lexbuf.Lexing.lex_curr_p in
    lexbuf.Lexing.lex_curr_p <-
      { pos with
        Lexing.pos_lnum = pos.Lexing.pos_lnum + 1;
        Lexing.pos_bol = pos.Lexing.pos_cnum }

  let print_error lexer =
    let position = Lexing.lexeme_start_p lexer in
    let line = position.Lexing.pos_lnum
    and char = position.Lexing.pos_cnum - position.Lexing.pos_bol in
    Printf.sprintf "Lexer : at line %d, column %d: unexpected character.\n%!" line char

  let string_of_char c = Printf.sprintf "%c" c
}

let digit = ['0' - '9']
let sc = ['a' - 'z']
let bc = ['A' - 'Z']
let letter = sc | bc | digit | ['_'] | ['-'] | ['+'] | ['<'] | ['>'] | ['*'] | ['/'] | ['='] | ['@'] | ['#']
let variable = ['?']letter+
let constant = letter+
let lf = "\r\n" | ['\n' '\r']

rule line_comment = parse
  | ([^'\n']* '\n') { incr_linenum lexbuf; token lexbuf }
  | ([^'\n']* eof) { P.EOF }
  | _   { raise (Error (print_error lexbuf)) }

and token = parse
  | '('     { P.LPAREN } | ')'  { P.RPAREN }
  | "or" | "OR" { P.OR } | "and" | "AND" { P.AND } | "not" | "NOT" { P.NOT }
  | "<="    { P.RULE }
  | [' ' '\t'] { token lexbuf }
  | lf   { incr_linenum lexbuf; token lexbuf }
  | variable as v { let v' = Scanf.sscanf v "?%s" (fun s -> s) in P.VARIABLE v' }
  | constant as c { P.CONSTANT c }
  | eof  { P.EOF }
  | "/*" { comment lexbuf }
  | ";"  { line_comment lexbuf }
  | _    { raise (Error (print_error lexbuf)) }

and comment = parse
  | "*/" { token lexbuf }
  | lf   { incr_linenum lexbuf; comment lexbuf }
  | _    { comment lexbuf }
