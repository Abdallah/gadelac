open Printf

let prec_print prec prec' = if prec <= prec' then sprintf "(%s)" else sprintf "%s"

type param = unit

let escape = Utils.MString.escape ['_'; '+'; '-'; '<'; '>'; '='; '/'; '\''; '\\'] '_' 

let newline n = "\n" ^ Utils.MString.repeat "  " n

let aand f = function
  | [] -> "true"
  | a :: [] -> f a
  | (_ :: _ :: _ as l) -> Utils.Print.list' "(" " && " ")" f l

let o_ifthen n cond = sprintf "if %s%sthen %s" cond (newline n)
let o_ifthenelse n cond ccl1 ccl2 = sprintf "if %s%sthen %s%selse %s" cond (newline n) ccl1 (newline n) ccl2

let algebraic_type name options = sprintf "type %s =%s" name (Utils.Print.list' "" "" "" (sprintf "%s| %s" (newline 1)) options)

module I = Instructions.T

let prec_funcall = 1
let prec_neg = 1
let prec_equal = 2
let prec_conj = 3
let prec_ifthenelse = 4
let prec_stmt = 5
let prec_match = 6

let symbol         symb = sprintf "S_%s'%d"    (escape (Ast.Print.symbol   symb)) (Ast.arity     symb)
let predicate       fct = sprintf "P_%s'%d"    (escape (Ast.Print.predicate fct)) (Ast.arity_pred fct)
let fundicate       fct = sprintf "_fun_%s'%d" (escape (Ast.Print.predicate fct)) (Ast.arity_pred fct)
let predicate_field fct = sprintf "%s'%d"      (escape (Ast.Print.predicate fct)) (Ast.arity_pred fct)

let _data arg = sprintf "Runtime.%s" arg
let _fact = _data "Fact"
let _const = _data "Const"

(*let types = function
  | I.TState -> "t"
  | I.TString -> "string"
  | I.TBool -> "bool"
  | I.TPredicate -> "predicate"
  | I.Void -> "unit"*)

let typedef name = function
  | I.KB preds ->
    let tbase_one = sprintf "symbol %s" (_data "t") in
    let print_one pred =
      sprintf "%s : %s" (predicate_field pred) (if pred <> Predicate.Next then tbase_one else sprintf "(predicate, symbol) %s" (_data "next")) in
    let record = Predicate.Set.print' "{\n  " ";\n  " " }" print_one preds in
    sprintf "type %s = %s" name record
  | I.Predicates preds -> algebraic_type name (Predicate.Set.mapl predicate preds)
  | I.Symbols    symbs -> algebraic_type name (Symbol.Set.mapl symbol  symbs)

let rec var_type t = match t with
  | `TTerm -> sprintf "symbol %s" (_data "fact")
  | `TArray -> sprintf "%s array" (var_type `TTerm)
  | `TFTerm -> sprintf "predicate option %s" (_data "fact")
  | `TFArray -> sprintf "%s list" (var_type `TFTerm)

let dummy_var = function
  | I.DummyVar (_, s) -> escape (Scope.Print.variable s)
  | I.OtherVar (_, s) -> sprintf "%s" s
  | I.FunConstant fct -> sprintf "(%s %s)" _const (symbol fct)

let local = function
  | I.AddFact -> "add_fact"
  | I.FPredicate pred -> fundicate pred
  | I.Initialization -> "init"
  | I.MakeMove -> "make_move"
  | I.PredicateToString -> "predicate_to_string"
  | I.Print -> "print"
  | I.PrintFact -> "print_fact"
  | I.Restart -> "restart"
  | I.SymbolToString -> "symbol_to_string"

let qualified _prec = function
  | I.KBAdd   (v1, pred, v2)       -> sprintf "%s %s.%s %s"    (_data "add")         (dummy_var v1) (predicate_field pred)           (dummy_var v2)
  | I.KBNext  (v1, pred, v2)       -> sprintf "%s %s.%s %s %s" (_data "next")        (dummy_var v1) (predicate_field Predicate.Next) (predicate pred) (dummy_var v2)
  | I.KBGet   (v1, Predicate.Next) -> sprintf "%s %s.%s"       (_data "get_next")    (dummy_var v1) (predicate_field Predicate.Next)
  | I.KBGet   (v1,      fct)       -> sprintf "%s %s.%s"       (_data "get")         (dummy_var v1) (predicate_field fct)
  | I.KBClear (v1, Predicate.Next) -> sprintf "%s %s.%s"       (_data "clear_next")  (dummy_var v1) (predicate_field Predicate.Next)
  | I.KBClear (v1,      fct)       -> sprintf "%s %s.%s"       (_data "clear")       (dummy_var v1) (predicate_field fct)
  | I.KBCreate      Predicate.Next -> sprintf "%s ()"          (_data "create_next")
  | I.KBCreate                   _ -> sprintf "%s ()"          (_data "create")
  | I.KBPrint                  var -> sprintf "%s %s %s"       (_data "print_fact")  (local I.SymbolToString) (dummy_var var)
  | I.KBPrintArray             var -> sprintf "%s %s %s"       (_data "print_array") (local I.SymbolToString) (dummy_var var)
  | I.LocalCall  (lf, dvs) -> sprintf "%s %s" (local lf) (Utils.Print.unspaces dummy_var dvs)
  | I.PrintErr (pred, var) -> sprintf "prerr_endline (%S ^ \" \" ^ %s)" (predicate pred) (dummy_var var)
let flat_const fct = sprintf "%s %s" _const (symbol fct)
let flat_term fct = function
  | None -> assert (Ast.arity fct = 0); flat_const fct
  | Some var -> assert (Ast.arity fct > 0); sprintf "%s (%s, %s)" _fact (symbol fct) (escape (Scope.Print.variable var))

let rec term = function
  | Ast.T.Variable x -> x
  | Ast.T.Fact (f, []) -> flat_const f
  | Ast.T.Fact (f, ts) -> sprintf "%s (%s, %s)" _fact (symbol f) (Utils.Print.array_of_list term ts)

let base_expr = function
  | I.String s -> sprintf "%S" s
  | I.Bool   b -> sprintf "%b" b
  | I.Predicate fct -> predicate fct
  | I.Symbol symb -> symbol symb
  | I.Variable var -> dummy_var var

let mbranch = function
  | I.MBTermTuple ts -> Utils.Print.array_of_list term ts
  | I.MBTerm t -> term t
  | I.MBBase b -> base_expr b
  | I.Default -> "_"

let construction = function
  | I.CFact   (var, fct, arg) -> sprintf "let %s = %s" (escape (Scope.Print.variable var)) (flat_term fct arg)
  | I.CArray  (compound, l) -> sprintf "let %s = %s" (dummy_var compound) (Utils.Print.array_of_list dummy_var l)
  | I.CRecord (compound, l) -> 
    let print_line (pred, var) = sprintf "%s = %s" (predicate_field pred) (dummy_var var) in
    sprintf "let %s = %s"  (dummy_var compound) (Utils.Print.list' "{ " "; " " }" print_line l)

let rec expr prec = function
  | I.KBExists (v1, pred, v2) -> prec_print prec prec_funcall (sprintf "%s %s.%s %s" (_data "exists") (dummy_var v1) (predicate_field pred) (dummy_var v2))
  | I.Not              e -> prec_print prec prec_neg   (sprintf "not %s" (expr prec_neg e))
  | I.Conjunction     es -> prec_print prec prec_conj  (aand (expr prec_conj) es)
  | I.Equal     (e1, e2) -> prec_print prec prec_equal (sprintf "%s = %s" (dummy_var e1) (dummy_var e2))
  | I.Different (e1, e2) -> prec_print prec prec_equal (sprintf "%s <> %s" (dummy_var e1) (dummy_var e2))

let rec stmt indent prec = function
  | I.Construct c -> construction c ^ " in"
  | I.Iterate (x, f, l) -> sprintf "List.iter (fun %s -> %s) %s" (dummy_var x) (stmt (indent + 1) prec_stmt f) (dummy_var l)
  | I.SFunCall f -> prec_print prec prec_funcall (qualified prec_funcall f)
  | I.ValueDef (s, qf) ->
    assert (prec <= prec_stmt);
    sprintf "let %s = %s in" (dummy_var s) (qualified prec_stmt qf)
  | I.ValueUpdate (s, qf) ->
    assert (prec <= prec_stmt);
    sprintf "%s := %s" (dummy_var s) (qualified prec_stmt qf)
  | I.Sequence [] -> "()"
  | I.Sequence ss -> prec_print prec prec_stmt (sequence indent ss)
  | I.IfThenElse ite -> ifthenelse indent prec ite
  | I.Match (e, ess) -> 
    let arg = Utils.Print.list' "" (newline indent) "" (one_branch (indent + 2)) ess in
    let matcher = sprintf "match %s with%s%s" (dummy_var e) (newline indent) arg in
    prec_print prec prec_match matcher
  | I.Return e -> base_expr e
  | I.Free _ -> "()"
  | I.Noop -> "()"
  | I.Abort -> prec_print prec prec_funcall "assert false"

and ifthenelse indent prec (cond, s1, s2) =
  let arg1 = expr prec_ifthenelse cond
  and arg2 = stmt (indent + 1) prec_ifthenelse s1
  and arg3 = stmt (indent + 1) prec_ifthenelse s2 in
  if arg3 = "()" then prec_print prec prec_ifthenelse (o_ifthen indent arg1 arg2)
  else prec_print prec prec_ifthenelse (o_ifthenelse indent arg1 arg2 arg3)

and one_branch indent (cond, ccl) =
  let arg1 = mbranch cond 
  and arg2 = stmt indent prec_match ccl in
  sprintf "| %s -> %s" arg1 arg2

and sequence indent = function
  | [] -> "()"
  | s1 :: rest ->
    let arg1 = stmt indent prec_stmt s1
    and arg2 = sequence indent rest
    and semi = match s1 with | I.Construct _ | I.ValueDef _  -> "" | _ -> ";" in
    if arg2 = "()" then arg1 else if arg1 = "()" then arg2
    else sprintf "%s%s%s%s" arg1 semi (newline indent) arg2

let global indent = function
  | I.Typedef (name, typ) -> typedef name typ
  | I.TypeSdef (t1, t2) -> sprintf "type %s = %s" t1 (var_type t2)
  | I.GlobalValueDef (s, be)   -> sprintf "let %s = %s" (dummy_var s) (base_expr be)
  | I.GlobalConstruct c -> construction c
  | I.GlobalFunDef   (s, [], st) -> sprintf "let %s () =%s%s" (local s) (newline (indent + 1)) (stmt (indent + 1) prec_stmt st)
  | I.GlobalFunDef   (s, es, st) -> sprintf "let %s %s =%s%s" (local s) (Utils.Print.unspaces dummy_var es) (newline (indent + 1)) (stmt (indent + 1) prec_stmt st)
  | I.Recursive ls ->
    let one (s, es, st) =
      let arg1 = Utils.Print.unspaces dummy_var es
      and arg2 = stmt (indent + 1) prec_stmt st in
      sprintf "%s %s =%s%s" (fundicate s) arg1 (newline (indent + 1)) arg2  in
    Utils.Print.list' "let rec " (sprintf "%s%sand " (newline indent) (newline indent)) "" one ls

let is_seq = function
  | I.GlobalConstruct _ -> true
  | I.GlobalValueDef _ | I.Typedef _ | I.TypeSdef _ | I.GlobalFunDef _ | I.Recursive _ -> false
let regroup_aux (go, accu1, accu2) gn =
  if is_seq go && is_seq gn then (gn, go :: accu1, accu2) else (gn, [], List.rev (go :: accu1) :: accu2)
let regroup_similar = function
  | [] -> assert false
  | head :: rest ->
    let (g, accu1, accu2) = List.fold_left regroup_aux (head, [], []) rest in
    let accu1 = List.rev (g :: accu1) in
    List.rev (accu1 :: accu2)

let program () gs =
  let rgped = regroup_similar gs in
  Utils.Print.list' "" (newline 0 ^ newline 0) "" (Utils.Print.list' "" (newline 0) "" (global 0)) rgped
