(** This module allows creating new predicates and rules to generate them and avoid duplications. *)

type t

val variable : string -> (unit -> string)
val predicate : string -> (int -> Ast.T.predicate)
val empty : (int -> Ast.T.predicate) -> t
val add_rule : t -> string list -> 'a Desugared.T.literal list -> t * Ast.T.free Ast.T.atomic
val get : t -> Ast.T.free Desugared.T.clause list
