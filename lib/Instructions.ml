open Printf

module A = Annotations

include Types.INSTRUCTIONS
open T

type param = string

module Print =
struct
  let types = function
    | TState     -> "State"
    | TString    -> "String"
    | TBool      -> "TBool"
    | TPredicate -> "TPredicate"
    | Void       -> "Void"

  let typedef = function
    | KB         preds -> sprintf "KB %s" (Predicate.Set.print preds)
    | Predicates fs -> sprintf "Predicates := %s" (Predicate.Set.print fs)
    | Symbols    fs -> sprintf "Symbols := %s" (Symbol.Set.print fs)

  let dummy_var = function
    | DummyVar (t, s) -> sprintf "Var(%s %s)" (Scope.Print.var_type t) (Scope.Print.variable s)
    | OtherVar (t, s) -> sprintf "Var(%s %s)" (types t) s
    | FunConstant fct -> Ast.Print.symbol fct

  let local_function = function
    | AddFact           -> "AddFact"
    | FPredicate pred   -> sprintf "P %s" (Ast.Print.predicate pred)
    | Initialization    -> "Initialization"
    | MakeMove          -> "MakeMove"
    | PredicateToString -> "PredicateToString"
    | Print             -> "Print"
    | PrintFact         -> "PrintFact"
    | Restart           -> "Restart"
    | SymbolToString    -> "SymbolToString"

  let qualified_function = function
    | KBAdd    (db, pred, var) -> sprintf "KBAdd %s %s %s"    (dummy_var db) (Ast.Print.predicate pred) (dummy_var var)
    | KBNext   (db, pred, var) -> sprintf "KBNext %s %s %s"   (dummy_var db) (Ast.Print.predicate pred) (dummy_var var)
    | KBGet    (db, pred)  -> sprintf "KBGet %s %s"  (dummy_var db) (Ast.Print.predicate pred)
    | KBClear  (db, pred)  -> sprintf "KBClear %s %s"  (dummy_var db) (Ast.Print.predicate pred)
    | KBCreate pred        -> sprintf "KBCreate %s" (Ast.Print.predicate pred)
    | KBPrint var          -> sprintf "KBPrint %s" (dummy_var var)
    | KBPrintArray var     -> sprintf "KBPrintArray %s" (dummy_var var)
    | LocalCall (lf, dvs)  -> sprintf "Local (%s, %s)" (local_function lf) (Utils.Print.list dummy_var dvs)
    | PrintErr (pred, var) -> sprintf "PrintErr (%s, %s)" (Ast.Print.predicate pred) (dummy_var var)

  let base_expr = function
    | String s -> sprintf "\"%s\"" s
    | Bool b    -> sprintf "%b" b
    | Predicate pred -> sprintf "P %s" (Ast.Print.predicate pred)
    | Symbol    symb -> sprintf "S %s" (Ast.Print.symbol  symb)
    | Variable var -> dummy_var var

  let match_branch = function
    | MBTermTuple ts -> Utils.Print.list' "(" ", " ")" Ast.Print.term ts
    | MBTerm       t -> Ast.Print.term t
    | MBBase be      -> base_expr be
    | Default        -> "Default"

  let construction = function
    | CFact (dv1, fc, Some dv2) -> sprintf "%s := Fact (%s, %s)" (Scope.Print.variable dv1) (Ast.Print.symbol fc) (Scope.Print.variable dv2)
    | CFact (dv1, fc,     None) -> sprintf "%s := Const (%s)"    (Scope.Print.variable dv1) (Ast.Print.symbol fc)
    | CArray  (dv, dvs) -> sprintf "%s := %s" (dummy_var dv) (Utils.Print.list' "(" ", " ")" dummy_var dvs)
    | CRecord (dv, dvs) -> sprintf "%s := %s" (dummy_var dv) (Utils.Print.list (Utils.Print.couple Ast.Print.predicate dummy_var) dvs) 

  let rec expr = function
    | KBExists (db, pred, var) -> sprintf "KBExists %s %s %s" (dummy_var db) (Ast.Print.predicate pred) (dummy_var var)
    | Not               e -> sprintf "Not (%s)" (expr e)
    | Conjunction es      -> Utils.Print.list' "(" "&" ")" expr es
    | Equal     (dv, dv') -> sprintf "%s = %s" (dummy_var dv) (dummy_var dv')
    | Different (dv, dv') -> sprintf "%s <> %s" (dummy_var dv) (dummy_var dv')

  let rec stmt = function
    | Construct           c -> construction c
    | Iterate  (dv, s, dv') -> sprintf "Iterate(%s, %s, %s)" (dummy_var dv) (stmt s) (dummy_var dv')
    | SFunCall           qf -> sprintf "Call (%s)" (qualified_function qf)
    | ValueDef     (dv, qf) -> sprintf "%s := %s" (dummy_var dv) (qualified_function qf)
    | ValueUpdate  (dv, qf) -> sprintf "%s <- %s" (dummy_var dv) (qualified_function qf)
    | Sequence           ss -> Utils.Print.list' "" ";\n" "" stmt ss
    | IfThenElse (e, s, s') -> sprintf "If (%s)\nThen (%s)\nElse (%s)" (expr e) (stmt s) (stmt s')
    | Match (dv, l) ->
      let print_one (b, s) = sprintf "| %s -> %s" (match_branch b) (stmt s) in
      sprintf "match %s with %s" (dummy_var dv) (Utils.Print.unlines print_one l)
    | Return be -> sprintf "Return %s" (base_expr be)
    | Free dv   -> sprintf "Free %s" (dummy_var dv)
    | Noop  -> "Noop"
    | Abort -> "Abort"

  let global = function
    | Typedef (str, typ) -> sprintf "Typedef %s := %s" str (typedef typ)
    | TypeSdef (str,  t) -> sprintf "Typedef %s := %s" str (Scope.Print.var_type t)
    | GlobalValueDef  (str,   be) -> sprintf "%s := %s" (dummy_var str) (base_expr be)
    | GlobalConstruct c -> construction c
    | GlobalFunDef   (lf, dvs, s) -> sprintf "%s %s := %s" (local_function lf) (Utils.Print.list' "(" " " ")" dummy_var dvs) (stmt s)
    | Recursive rs ->
      let one (lf, dvs, s) = sprintf "%s %s := %s"  (Ast.Print.predicate lf) (Utils.Print.list' "(" " " ")" dummy_var dvs) (stmt s) in
      Utils.Print.list' "" "\n\n" "" one rs

  let program gs = Utils.Print.list' "" "\n\n" "" global gs

end

let to_dummy' var = DummyVar (`TTerm, var)

let _db = OtherVar (TState, "_db")

let affectation = function
  | Scope.T.Cons (var, fct)      -> Construct (CFact  (var, fct, None)),     Free (DummyVar (`TTerm, var))
  | Scope.T.Term (var, fct, arg) -> Construct (CFact  (var, fct, Some arg)), Free (DummyVar (`TTerm, var))
  | Scope.T.Array (var, vars)    -> Construct (CArray (DummyVar (`TArray, var), List.map to_dummy' vars)), Free (DummyVar (`TTerm, var))

let sequence l = match l with
  | [] -> Noop
  | a :: [] -> a
  | _ :: _ :: _-> Sequence l

let conclusion (conc : PreInstruction.conclusion) =
  let funcall = match conc with
    | PreInstruction.FunCall (f, None)     -> LocalCall (FPredicate f, [_db])
    | PreInstruction.FunCall (f, Some var) -> LocalCall (FPredicate f, [_db; DummyVar (`TTerm, var)])
    | PreInstruction.Next (f, var) -> KBNext (_db, f, DummyVar (`TTerm, var)) in
  SFunCall funcall

let conclusions ccls = sequence (Utils.PList.map conclusion ccls)

let if_exists (pred, v) cclusions =
  let var = to_dummy' v in
  let fun_call = KBExists (_db, pred, var) in
  IfThenElse (fun_call, cclusions, Noop)

let if_not (pred, v) cclusions =
  let var = to_dummy' v in
  let fun_call = KBExists (_db, pred, var) in
  IfThenElse (Not fun_call, cclusions, Noop)

let if_dist (s1, v) cclusions =
  let t1_var = DummyVar (`TTerm, s1) in
  let t2_var = to_dummy' v in
  IfThenElse (Different (t1_var, t2_var), cclusions, Noop)

let unify_call running_var vars terms ccl =
  match vars, ccl with
  | [], SFunCall _ -> ccl
  | _, _ ->
    let default = (Default, Noop) in
    let one_guard (xx, x) = Equal (DummyVar (`TTerm, xx), DummyVar (`TTerm, x)) in
    let vars_guard = Conjunction (List.map one_guard vars) in
    let action = if vars = [] then ccl else IfThenElse (vars_guard, ccl, Noop) in
    if List.for_all Desugared.is_variable terms
    then Match (running_var, [(MBTermTuple terms, action)])
    else Match (running_var, [(MBTermTuple terms, action); default])

(* attention s'il y a plusieurs fois la même variable à l'interieur d'un truc unifié ! Vérifier avant *)
(* S'il n'y a pas de variable libre, pas besoin du start_with ! *)
let if_unify (fct, u) conclusions =
  let bindings = u.PreInstruction.bindings
  and terms = u.PreInstruction.terms in
  let facts_var = DummyVar (`TArray, Scope.read_variable "facts") in
  let facts_bind = ValueDef (facts_var, KBGet (_db, fct)) in
  let instance_var = DummyVar (`TTerm, Scope.read_variable "instance") in
  let get_process = unify_call instance_var bindings terms conclusions in
  let process = Iterate (instance_var, get_process, facts_var) in
  [facts_bind; process]

let add_affectations affs stmts =
  let binds, frees = Utils.PList.split_map affectation affs in
  binds @ stmts @ frees

let action ((hypothesis, affs, ccls) : PreInstruction.action) =
  let ccls = sequence (add_affectations affs [conclusions ccls]) in
  match hypothesis with
  | PreInstruction.Okay -> [ccls]
  | PreInstruction.IfExists t -> [if_exists t ccls]
  | PreInstruction.IfNot t -> [if_not t ccls]
  | PreInstruction.IfDist terms -> [if_dist terms ccls]
  | PreInstruction.IfUnify unify -> if_unify unify ccls

let branch (terms, affs, actions) = (MBTermTuple terms, sequence (add_affectations affs (List.concat_map action actions)))

let appears_in_unify var uni =
  let in_binding (x, y) = var = x || var = y in
  let in_term t = List.mem (Scope.Print.variable var) (Ast.get_variables t) in
  List.exists in_binding uni.PreInstruction.bindings || List.exists in_term uni.PreInstruction.terms
let appears_in_aff var = function
  | Scope.T.Cons  (var', _) -> assert (var <> var'); false
  | Scope.T.Term  (var', _, arg) -> assert (var <> var'); var = arg
  | Scope.T.Array (var', arr) -> assert (var <> var'); List.mem var arr
let appears_in_hyp var hyp = match hyp with
  | PreInstruction.Okay -> false
  | PreInstruction.IfExists (_, var2) -> var = var2
  | PreInstruction.IfUnify  (var2, uni) -> appears_in_unify var uni
  | PreInstruction.IfNot    (_, var2) -> var = var2
  | PreInstruction.IfDist   (str, var2) -> var = var2 || str = var
let appears_in_ccl var = function
  | PreInstruction.FunCall (_, var_opt) -> var_opt = Some var
  | PreInstruction.Next (_, var2) -> var2 = var
let appears_in_actions var actions =
  let f (hyp, affs, ccls) = List.exists (appears_in_aff var) affs || appears_in_hyp var hyp || List.exists (appears_in_ccl var) ccls in
  List.exists f actions
let appear_in_actions vars actions =
  let f = function
    | Ast.T.Fact _ -> assert false
    | Ast.T.Variable var -> appears_in_actions (Scope.read_variable var) actions in
  List.exists f vars

let matching main_arg_var (ms : PreInstruction.branch list) =
  match main_arg_var, ms with
  | None, ([], affs, actions) :: _ -> sequence (add_affectations affs (List.concat_map action actions))
  | Some _, ([], _, _) :: _ -> assert false
  | None, (_ :: _, _, _) :: _ -> assert false
  | None, [] -> assert false
  | Some arg_var, _ ->
    let matches = List.map branch ms @ [(Default, Noop)] in
    Match (arg_var, matches)

let empty_pred_arg = OtherVar (TBool, "[||]")
let func (prd, ms) =
  let (args, scope_main_var, main_arg_var) =
    let scope_main_arg = Scope.read_variable "_arg" in
    if Ast.arity_pred prd > 0
    then let mav = DummyVar (`TArray, scope_main_arg) in ([_db; mav], Some scope_main_arg, Some mav)
    else ([_db], None, None) in
  let arg_var = match main_arg_var with Some v -> v | None -> empty_pred_arg in
  let fact_absent = Not (KBExists (_db, prd, arg_var)) in
  let fact_add = SFunCall (KBAdd (_db, prd, arg_var)) in
  let conseq = sequence (fact_add :: List.map (matching main_arg_var) ms) in
  let ifthen = IfThenElse (fact_absent, conseq, Noop) in
  (prd, args, ifthen)

let add_fact predicates =
  let preds = Predicate.Set.filter (fun pred -> pred <> Predicate.Next && pred <> Predicate.True) predicates in
  let pred_var = DummyVar (`TTerm, Scope.read_variable "pred") in
  let args_var = DummyVar (`TArray, Scope.read_variable "args") in
  let one_branch pred =
    let arity = Ast.arity_pred pred in
    if arity = 0
    then (MBBase (Predicate pred), SFunCall (LocalCall (FPredicate pred, [_db])))
    else (MBBase (Predicate pred), SFunCall (LocalCall (FPredicate pred, [_db; args_var]))) in
  let pairs = Predicate.Set.mapl one_branch preds in
  let default = (Default, Abort) in
  let matching = Match (pred_var, pairs @ [default]) in
  GlobalFunDef (AddFact, [_db; pred_var; args_var], matching)

let dynamics prog =
  let annot = prog.PreInstruction.annot in
  assert (Predicate.Set.for_all (fun p -> Predicate.Map.mem p annot.A.frequency) annot.A.predicates);
  let freqs = Predicate.Map.remove Predicate.True annot.A.frequency in
  let pred_freq = Predicate.Map.bindings freqs in
  let pred_temp (pred, freq) = match freq with
    | Frequency.Permanent -> None | Frequency.Ephemeral | Frequency.Persistent -> Some (SFunCall (KBClear (_db, pred))) in
  let pred_ephe (pred, freq) = match freq with
    | Frequency.Permanent | Frequency.Persistent -> None | Frequency.Ephemeral -> Some (SFunCall (KBClear (_db, pred))) in
  let clear_temp = List.filter_map pred_temp pred_freq in
  let clear_ephe = List.filter_map pred_ephe pred_freq in
  let add_one fct arg = match arg with
    | None -> conclusion (PreInstruction.FunCall (fct, None))
    | Some l -> Sequence (List.map (fun var -> conclusion (PreInstruction.FunCall (fct, Some var))) l) in
  let perms = Predicate.Map.values (Predicate.Map.mapi add_one prog.PreInstruction.perms) in
  let inits = Predicate.Map.values (Predicate.Map.mapi add_one prog.PreInstruction.inits) in
  let create_db pred =
    let var = DummyVar (`TTerm, Scope.read_variable (sprintf "_db_%s" (Ast.Print.predicate pred))) in
    (pred, var), ValueDef (var, KBCreate pred) in
  let aggr, db_binds = Utils.PList.split (Predicate.Set.mapl create_db annot.A.predicates) in
  let db_bind = Construct (CRecord (_db, aggr)) in
  let earl, late = annot.A.strats in
  let earl_strats = Utils.PList.map (fun strat -> SFunCall (LocalCall (FPredicate strat, [_db]))) earl in
  let late_strats = Utils.PList.map (fun strat -> SFunCall (LocalCall (FPredicate strat, [_db]))) late in
  let moves_var = DummyVar (`TArray, Scope.read_variable "moves") in
  let move_loop =
    let move_var = DummyVar (`TTerm, Scope.read_variable "move") in
    let loop_body = SFunCall (LocalCall (FPredicate Predicate.Does, [_db; move_var])) in
    Iterate (move_var, loop_body, moves_var) in
  let nexts_var = DummyVar (`TArray, Scope.read_variable "nexts") in
  let next_loop =
    let next_var  = DummyVar (`TTerm, Scope.read_variable "(pred, facts)") in
    let next_var' = DummyVar (`TTerm, Scope.read_variable "pred facts") in
    let loop_body = SFunCall (LocalCall (AddFact, [_db; next_var'])) in
    Iterate (next_var, loop_body, nexts_var) in
  let nexts_bind = ValueDef (nexts_var, KBGet (_db, Predicate.Next)) in
  let return_var = DummyVar (`TArray, Scope.read_variable "result") in
  let lega_bind = Sequence [ValueUpdate (return_var, KBGet (_db, Predicate.Legal)); Return (Bool  true)] in
  let goal_bind = Sequence [ValueUpdate (return_var, KBGet (_db,  Predicate.Goal)); Return (Bool false)] in
  let condition = [IfThenElse (KBExists (_db, Predicate.Terminal, empty_pred_arg), goal_bind, lega_bind)] in
  let print_loop pred =
    let array_var = DummyVar (`TArray, Scope.read_variable "array") in
    let array_str = DummyVar (`TArray, Scope.read_variable "str") in
    let arrays_var = DummyVar (`TArray, Scope.read_variable "arrays") in
    let arrays_bind = ValueDef (arrays_var, KBGet (_db, pred)) in
    let array_bind = ValueDef (array_str, KBPrintArray array_var) in
    let loop = Iterate (array_var, Sequence [array_bind; SFunCall (PrintErr (pred, array_str))], arrays_var) in
    [arrays_bind; loop] in
  let prin = GlobalFunDef (Print, [_db], Sequence (List.concat_map print_loop (Predicate.Map.keys (Predicate.Map.remove Predicate.Next freqs)))) in
  let init = GlobalFunDef (Initialization, [], Sequence (db_binds @ db_bind :: perms @ [Return (Variable _db)]))
  and rest = GlobalFunDef (Restart, [_db; return_var], Sequence (clear_temp @ inits @ earl_strats @ condition))
  and move = GlobalFunDef (MakeMove, [_db; moves_var; return_var], Sequence (move_loop :: late_strats @ nexts_bind :: clear_ephe @ next_loop :: earl_strats @ condition)) in
  [prin; init; rest; move]

let types annot =
  let tpred = Typedef ("predicate", Predicates annot.A.predicates) in
  let tsymb = Typedef ("symbol", Symbols annot.A.atoms) in
  let tfact = TypeSdef ("fact", `TTerm) in
  let tbase = Typedef ("t", KB annot.A.predicates) in
  [tpred; tsymb; tfact; tbase]

let casting annot =
  let predicate_var = OtherVar (TPredicate, "_arg") in
  let pred_to_string =
    let one pred = (MBBase (Predicate pred), Return (String (Ast.Print.predicate pred))) in
    let body = Match (predicate_var, Predicate.Set.mapl one annot.A.predicates) in
    GlobalFunDef (PredicateToString, [predicate_var], body) in
  let symb_to_string =
    let one symb = (MBBase (Symbol symb), Return (String (Ast.Print.symbol symb))) in
    let body = Match (predicate_var, Symbol.Set.mapl one annot.A.atoms) in
    GlobalFunDef (SymbolToString, [predicate_var], body) in
  let print_fact =
    let arg_var = DummyVar (`TTerm, Scope.read_variable "fact") in
    let print_call = SFunCall (KBPrint arg_var) in
    GlobalFunDef (PrintFact, [arg_var], print_call) in
  [pred_to_string; symb_to_string; print_fact]


let players annot =
  let roles = Symbol.Set.mapl (fun r -> FunConstant r) annot.A.roles in
  GlobalConstruct (CArray (DummyVar (`TArray, Scope.read_variable "players"), roles))

let global_terms global_affects =
  let f = function
    | Scope.T.Cons (var, fct)      -> GlobalConstruct (CFact  (var, fct, None))
    | Scope.T.Term (var, fct, arg) -> GlobalConstruct (CFact  (var, fct, Some arg))
    | Scope.T.Array (var, args)    -> GlobalConstruct (CArray (DummyVar (`TArray, var), List.map to_dummy' args)) in
  List.map f global_affects

let make name (prog : PreInstruction.program) =
  let annot = prog.PreInstruction.annot in
  let name = GlobalValueDef (OtherVar (TString, "name"), String name) in
  let globs = global_terms prog.PreInstruction.global in
  let program = Recursive (List.map func (Predicate.Map.bindings prog.PreInstruction.funs)) in
  name :: types annot @ casting annot @ players annot :: globs @ program :: add_fact annot.A.predicates :: dynamics prog
