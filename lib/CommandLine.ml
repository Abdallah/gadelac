open Printf

let clean_name s =
  let reg = Str.regexp "-" in
  let s = Str.global_replace reg "_" s in
  let reg = Str.regexp "/" in
  Str.global_replace reg "_" s

type backend = ASP | C | CLIPS | FOL | GDL | OCaml | Prolog | Table | UAMBAT
let backends = ["asp"; "c"; "clips"; "fol"; "gdl"; "ocaml"; "prolog"; "table"; "uambat"]
let print_backend = function | ASP -> "asp" | C -> "c" | CLIPS -> "clips" | FOL -> "fol" | GDL -> "gdl" | OCaml -> "ocaml" | Prolog -> "prolog" | Table -> "table" | UAMBAT -> "uambat"
let read_backend = function | "asp" -> ASP | "c" -> C | "clips" -> CLIPS | "fol" -> FOL | "gdl" -> GDL | "ocaml" -> OCaml | "prolog" -> Prolog | "table" -> Table | "uambat" -> UAMBAT | _ -> assert false

type debug =
  { mutable parsed : bool;
    mutable desugared : bool;
    mutable optimized : bool;
    mutable stratified : bool;
    mutable normalized : bool;
    mutable inverted : bool;
    mutable grouped : bool;
    mutable preinstruc : bool;
    mutable instructed : bool;
    mutable compiled : bool }

type param =
  { mutable poptimized : Optimization.param;
    mutable pinverted : Frequency.param;
    mutable pgrouped : Grouping.param;
    mutable pinstructed : Instructions.param }

type pass = Parsed | Desugared | Optimized | Stratified | Normalized | Inverted | Grouped | PreInstructed | Instructed | Compiled

type options =
  { input_name : string ref;
    output_name : string ref;
    verbose : int ref;
    backend : backend ref;
    hsfc : bool ref;
    count_roles : bool ref;
    param : param;
    debug : debug }

let print_pass = function
  | Parsed        -> "parsed"
  | Desugared     -> "desugared"
  | Optimized     -> "optimized"
  | Stratified    -> "stratified"
  | Normalized    -> "normalized"
  | Inverted      -> "inverted"
  | Grouped       -> "grouped"
  | PreInstructed -> "preinstructed"
  | Instructed    -> "instructed"
  | Compiled      -> "compiled"
let print_debug d =
  sprintf "parsed:%b; desugared:%b; optimized:%b; stratified:%b; normalized:%b; inverted:%b; grouped:%b; compiled:%b"
    d.parsed d.desugared d.optimized d.stratified d.normalized d.inverted d.grouped d.compiled
let print_poptimized p = sprintf "optimization:%s" (Optimization.Print.param p)
let print_pinverted t = sprintf "temporization:%s" (Frequency.print_param t)
let print_pcompiled _ = sprintf ""
let print_param p =
  sprintf "param_optimized:%s,param_inverted:%s,param_grouped:%b,param_instructed:%s" (print_poptimized p.poptimized) (print_pinverted p.pinverted) p.pgrouped (print_pcompiled p.pinstructed)
let print { input_name; output_name; verbose; backend; hsfc; count_roles; param; debug } =
  sprintf "input_name:%s;\noutput_name:%s;\nverbose:%d;\nbackend:%s;\nhsfc:%B;\ncount_roles:%B;\n%s;\n%s" !input_name !output_name !verbose (print_backend !backend) !hsfc !count_roles (print_param param) (print_debug debug)

let debug_true () =
  { parsed       = true; desugared    = true; optimized    = true; stratified   = true; normalized   = true;
    inverted     = true; grouped      = true; preinstruc   = true; instructed   = true; compiled     = true }
let debug_false () =
  { parsed       = false; desugared    = false; optimized    = false; stratified   = false; normalized   = false;
    inverted     = false; grouped      = false; preinstruc   = false; instructed   = false; compiled     = false }

let adapt_debug debug bool = function
  | Parsed        -> debug.parsed       <- bool
  | Desugared     -> debug.desugared    <- bool
  | Optimized     -> debug.optimized    <- bool
  | Stratified    -> debug.stratified   <- bool
  | Normalized    -> debug.normalized   <- bool
  | Inverted      -> debug.inverted     <- bool
  | Grouped       -> debug.grouped      <- bool
  | PreInstructed -> debug.preinstruc   <- bool
  | Instructed    -> debug.instructed   <- bool
  | Compiled      -> debug.compiled     <- bool

let passes = ["parsed"; "desugared"; "optimized"; "stratified"; "normalized"; "inverted"; "grouped"; "preinstructed"; "instructed"; "compiled"]
let reachabs = ["trivial"; "graph"; "paths"; "precise"]
let print_reachab = Reachability.print
let read_reachab = function | "trivial" -> Reachability.Trivial | "graph" -> Reachability.Graph | "paths" -> Reachability.Paths | "precise" -> Reachability.Precise | _ -> assert false

let do_pass f pass = assert (List.mem pass passes);
  let pass = match pass with
    | "parsed"        -> Parsed
    | "desugared"     -> Desugared
    | "optimized"     -> Optimized
    | "stratified"    -> Stratified
    | "normalized"    -> Normalized
    | "inverted"      -> Inverted
    | "grouped"       -> Grouped
    | "preinstructed" -> PreInstructed
    | "instructed"    -> Instructed
    | "compiled"      -> Compiled
    | _ -> assert false in
  f pass

module Opt = Optimization

let default_param () =
  { poptimized = Optimization.default_param ();
    pinverted = Frequency.T2;
    pgrouped = true;
    pinstructed = "" }

let add_defaults specs =
  let add_one (option, spec, descr, default) = (option, spec, sprintf "%s Default: %s" descr default) in
  List.map add_one specs

let skip_defaults specs =
  let skip_one (option, spec, descr, _) = (option, spec, descr) in
  List.map skip_one specs

let update reference value = reference := value
let get () =
  let debug = debug_false () in
  let verbose = ref 0 in
  let output_name = ref "" in
  let input_name = ref "" in
  let backend = ref GDL in
  let hsfc = ref true in
  let count_roles = ref false in
  let grouping = ref true in
  let opti = Opt.default_param () in
  let pinverted = ref Frequency.T2 in
  let set_backend back = backend := read_backend back in
  let set_reachab reac = opti.Opt.reachability := read_reachab reac in
  let set_inverted arg = pinverted := Frequency.read_param arg in
  let ground_string = ref "" in
  let ground_index i = opti.Opt.ground := (!ground_string, i) :: !(opti.Opt.ground) in
  let specialize_string = ref "" in
  let specialize_index i = opti.Opt.specialize := (!specialize_string, i) :: !(opti.Opt.specialize) in
  let debug_pass = ref "" in
  let set_debug_pass pass = debug_pass := pass in
  let set_debug bool = do_pass (adapt_debug debug bool) !debug_pass in
  let opti_level lvl = Opt.set_level opti lvl; if lvl <= 0 then grouping := false else grouping := true in
  let print_contexts = Utils.Print.list (Utils.Print.couple Utils.Print.string Utils.Print.int) in
  let speclist =
    [("--verbose",        Arg.Set_int verbose, "Enable verbose mode.", sprintf "%d" !verbose);
     ("-o",               Arg.Set_string output_name, "Set the output path.", !output_name);
     ("--count_roles",    Arg.Set count_roles, "Display the number of roles", "unset");
     ("--debug",          Arg.Tuple [Arg.Symbol (passes, set_debug_pass); Arg.Bool set_debug], "Set the debugging for the given pass.", "FIXME");
     ("--backend",        Arg.Symbol (backends, set_backend), " Set the backend.", print_backend !backend);
     ("--hsfc",           Arg.Bool (update hsfc), " Partially ground true and next.", sprintf "%B" !hsfc);
     ("--inverted",       Arg.String set_inverted,  "Set the inverted parameters.", "FIXME");
     ("--grouped",        Arg.Bool (update grouping),       "Enable the grouping optimization.", sprintf "%B" !grouping);
     ("--specialize_tempo", Arg.Bool (update opti.Opt.specialize_tempo), "Specialize temporal indices in symbols.", sprintf "%B" !(opti.Opt.specialize_tempo));
     ("--specialize_bound", Arg.Bool (update opti.Opt.specialize_bound), "Specialize bound indices in symbols.", sprintf "%B" !(opti.Opt.specialize_bound));
     ("--ground_all",     Arg.Bool (update opti.Opt.ground_all),     "Enable grounding of all variables.", sprintf "%B" !(opti.Opt.ground_all));
     ("--measured",       Arg.Bool (update opti.Opt.measured), "Ground variables when they might instantiate to compound terms.", sprintf "%B" !(opti.Opt.measured));
     ("--flatten",        Arg.Bool (update opti.Opt.flatten), "Eliminate term nesting.", sprintf "%B" !(opti.Opt.flatten));
     ("--reachability",   Arg.Symbol (reachabs, set_reachab),   " Enable the reachability optimization.", print_reachab !(opti.Opt.reachability));
     ("--compute_rigids", Arg.Bool (update opti.Opt.compute_rigids), "Enable the computation of rigids predicate instances.", sprintf "%B" !(opti.Opt.compute_rigids));
     ("--merge_rigids",   Arg.Bool (update opti.Opt.merge_rigids),   "Enable the merging of rigids optimization.", sprintf "%B" !(opti.Opt.merge_rigids));
     ("--inlining",       Arg.Set_int opti.Opt.inlining,   "Set the inlining aggressivity. Use -1 for no inlining, 0 for minimal inlining, 1 for more inlining etc.", sprintf "%d" !(opti.Opt.inlining));
     ("--ground",     Arg.Tuple [Arg.Set_string ground_string; Arg.Int ground_index], "Ground the given index of the given predicate/symbol.", print_contexts !(opti.Opt.ground));
     ("--ground_count",   Arg.Set_int opti.Opt.ground_count, "Ground variables as long as there are fewer than the given number of instances.", sprintf "%d" !(opti.Opt.ground_count));
     ("--specialize", Arg.Tuple [Arg.Set_string specialize_string; Arg.Int specialize_index], "Ground and specialize the given index of the given predicate/symbol.", print_contexts !(opti.Opt.specialize));
     ("--frame",          Arg.Bool (update opti.Opt.frame_translation), "Perform the Frame translation.", sprintf "%B" !(opti.Opt.frame_translation));
     ("--noop",           Arg.Set_string opti.Opt.noop_move, "Set the no-operation move, if any.", !(opti.Opt.noop_move));
     ("-O",               Arg.Int opti_level, "Set the optimization level, 0 = none, 1 = no_change, 2 = full.", "1");
     ("-",                Arg.Unit (fun () -> update input_name "-"), "Read the GDL from the standard input.", "Disabled");
    ] in
  let usage_msg = "GaDeLaC, the Game Description Language Compiler, version 0.6.1. Options available:" in
  Arg.parse (add_defaults speclist) (update input_name) usage_msg;
  let param = { pinstructed = !input_name;
                pgrouped = !grouping;
                pinverted = !pinverted;
                poptimized = opti } in
  let options =
    { input_name;
      output_name;
      verbose;
      backend;
      hsfc;
      count_roles;
      param;
      debug } in
  if !input_name = "" then failwith "not enough arguments.";
  options
