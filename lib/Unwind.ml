open Printf

let deprintf format = if true then eprintf format else ifprintf stderr format

(** *)
let is_recursive_clause scc (head, body) =
  let (pred, _) = head in
  match List.find_opt (Predicate.Set.mem pred) scc with
  | None -> None (*failwith (sprintf "pred: %s" (Ast.Print.predicate pred)); assert false*)
  | Some component ->
     let in_set : type a. a Desugared.T.literal -> (a Ast.T.atomic, a Ast.T.atomic) Either.t option = function
       | Desugared.T.Distinct _ -> None
       | Desugared.T.Okay (p, args) -> if Predicate.Set.mem p component then Some (Either.Left (p, args)) else Some (Either.Right (p, args))
       | Desugared.T.Negation (p, _) -> if Predicate.Set.mem p component then failwith (sprintf "Recursion through negation: %s.\n" (Ast.Print.predicate p)) else None in
     let recurs, other = List.partition_map Fun.id (List.filter_map in_set body) in
     if recurs = [] then None else Some (recurs, other)

(*let deep_vars : type a. a Ast.T.term -> string list = function
  | Ast.T.Variable _ -> []
  | Ast.T.Fact (_, ts) -> Utils.PList.uniques (List.concat_map Ast.get_variables ts)
let is_context pred rvars i arg = if List.exists (fun v -> List.mem v rvars) (deep_vars arg) then Some (Either.Left (pred, i)) else None*)
let is_context pred rvars i arg = if List.exists (fun v -> List.mem v rvars) (Ast.get_variables arg) then Some (Either.Left (pred, i)) else None
let context_clause scc (head, body) =
  match is_recursive_clause scc (head, body) with
  | None -> []
  | Some (r, o) ->
     let (pred, args) = head in
     let hvars = List.concat_map Ast.get_variables args in
     let rvars, ovars = List.concat_map Ast.get_variables_atomic r, List.concat_map Ast.get_variables_atomic o in
     let rvars = List.filter (fun v -> (List.mem v ovars)) hvars in
     let rvars = Utils.PList.uniques rvars in
     (*eprintf "rule: %s; ro: %s %s; rvars: %s\n%!" (Desugared.Print.clause (head, body))  (Utils.Print.list Ast.Print.atomic r) (Utils.Print.list Ast.Print.atomic o) (Utils.Print.list Fun.id rvars);*)
     let contexts = List.mapi (is_context pred rvars) args in
     let contexts = List.filter_map Fun.id contexts in
     contexts

let context_programs scc (prog) =
  let cs = List.concat_map (context_clause scc) prog in
  Utils.PList.uniques cs

let needs_unwind_atomic comp (pred, _) = Predicate.Set.mem pred comp
let needs_unwind_literal comp = function
  | Desugared.T.Okay atom -> needs_unwind_atomic comp atom
  | Desugared.T.Negation _ | Desugared.T.Distinct _ -> false

let int_term i = Ast.T.Fact ((sprintf "%d" i, 0, 0), [])
let head_var_name = "v__h"
let body_var_name = "v__b"
let head_var = Ast.T.Variable head_var_name
let body_var = Ast.T.Variable body_var_name
let succ_atom n0 n1=
  let pred = Predicate.Constant ("__succ", 2, 2) in
  (pred, [n0; n1])
let stop_literal comp =
  let n = int_term (Predicate.Set.cardinal comp) in
  Desugared.T.Distinct (body_var_name, n)
let make_recursive_pred pred =
  let a = Ast.arity_pred pred in
  Predicate.Constant (sprintf "%s__rec" (Ast.Print.predicate pred), a+1, a+1)
(*let make_recursive_left_atomic scc atom =
  let (pred, args) = atom in
  if List.exists (List.mem pred) scc then (make_recursive_pred pred, args @ [0]) else atom*)
let make_recursive_clause comp (head, body) =
  let (pred, args) = head in
  let recursive_clause = List.exists (needs_unwind_literal comp) body in
  let head_arg  = if recursive_clause then head_var else int_term 0 in
  (*let body_args = [] in*)
  let body_args = if recursive_clause then [Desugared.T.Okay (succ_atom body_var head_var); stop_literal comp] else [] in
  let update_lit = function
    | Desugared.T.Distinct _ | Desugared.T.Negation _ as lit -> lit
    | Desugared.T.Okay (p, ars)-> let atom = if Predicate.Set.mem p comp then (make_recursive_pred p, ars @ [body_var]) else (p, ars) in Desugared.T.Okay atom in
  let body = List.map update_lit body in
  ((make_recursive_pred pred, args @ [head_arg]), body @ body_args)

let unwind_clause scc clause =
  let (head, _) = clause in
  let in_head comp = needs_unwind_atomic comp head in
  match List.find_opt in_head scc with
  | None -> clause
  | Some comp -> make_recursive_clause comp clause

let unwind_prog scc prog =
  let prog = Utils.PList.map (unwind_clause scc) prog in
  let conversion_rule n pred =
    let a = Ast.arity_pred pred in
    let vars = List.map (fun i -> Ast.T.Variable (sprintf "v%d" i)) (Utils.PList.range 0 a) in
    let head = (pred, vars)
    and body = [Desugared.T.Okay (make_recursive_pred pred, vars @ [int_term n])] in
    (head, body) in
  let upgrade_rule n pred =
    let a = Ast.arity_pred pred in
    let vars = List.map (fun i -> Ast.T.Variable (sprintf "v%d" i)) (Utils.PList.range 0 a)
    and rpred = make_recursive_pred pred in
    let head = (rpred, vars @ [head_var])
    and hypo = Desugared.T.Okay (rpred, vars @ [body_var])
    and succ = Desugared.T.Okay (succ_atom body_var head_var)
    and dist = Desugared.T.Distinct (body_var_name, int_term n) in
    let body = [hypo; succ; dist] in
    (head, body) in
  let conversion_comp comp =
    let n = Predicate.Set.cardinal comp in
    Predicate.Set.maps (fun p -> [upgrade_rule (n-1) p; conversion_rule (n-1) p]) comp in
  let predicate_rules = List.concat_map conversion_comp scc in
  let largest = List.fold_left max 0 (List.map Predicate.Set.cardinal scc) in
  let succ_rule n =
    let head = succ_atom (int_term n) (int_term (n+1)) in
    (head, []) in
  let succs = List.map succ_rule (Utils.PList.range 0 (largest)) in
  prog @ predicate_rules @ succs

let compute_sccs prog =
  let dep = Analysis.dep_graph prog in
  let scc = Predicate.SCC.tarjan dep in
  let direct_self s = match Predicate.Map.find_opt s dep with
    | Some d -> Predicate.Set.mem s d
    | None -> assert false in
  let keep s = Predicate.Set.cardinal s > 1 || Predicate.Set.exists direct_self s in
  List.filter keep scc
let update_sccs scc =
  let update_comp = Predicate.Set.map make_recursive_pred in
  List.map update_comp scc
let program prog =
  let prog = Desugared.cast_program prog in
  let scc = compute_sccs prog in
  deprintf "Unwind.sccs: %s\n%!" (Utils.Print.unspaces Predicate.Set.print scc);
  let oldcontexts = context_programs scc prog in
  let prog = unwind_prog scc prog in
  deprintf "Unwind.rewrite:\n%s\n%!" (Desugared.Print.program prog);
  let scc' = update_sccs scc in
  deprintf "Unwind.updated sccs': %s\n%!" (Utils.Print.unspaces Predicate.Set.print scc');
  let scc = compute_sccs prog in
  deprintf "Unwind.updated sccs: %s\n%!" (Utils.Print.unspaces Predicate.Set.print scc);
  assert (scc' = scc);
  let contexts = context_programs scc prog in
  deprintf "Unwind.contexts: %s\n%!" (Utils.Print.unspaces Context.print contexts);
  let prog = Ground.program contexts prog in
  deprintf "Unwind.grounded:\n%s\n%!" (Desugared.Print.program prog);
  let prog = Specialization.specialize_program (fst (List.partition_map Fun.id contexts)) prog in
  eprintf "Unwind.specialized:\n%s\n%!" (Desugared.Print.program prog);
  Desugared.cast_program prog
