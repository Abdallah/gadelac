#! /usr/bin/env python3
# -*- coding: utf-8 -*-

too_long_one_player = ["ruledepthquadratic", ]
one_player = ["blocks", "coins", "duplicatestatesmall", "duplicatestatemedium", "duplicatestatelarge", "ruledepthlinear", "ruledepthexponential", "hanoi", "firefighter", "max_knights", "queens", "statespacesmall", "statespacemedium", "statespacelarge"]

bugged_two_player = ["ad_game_2x2", # problem -> predicate(?x, ?x, blabla)
                     "bidding-tictactoe", # hasn't been tested
                     "bomberman2p", # problem -> cf avg result + avg moves
                     "brawl", # ?
                     "catcha_mouse", # problem -> cf avg result
                     "ghostmaze2p", # problem -> cf avg result + avg moves
                     "mummymaze2p", # problem -> cf nb playouts (2 !)
                     ]

two_player = ["battle", "beatmania", "bidding-tictactoe", "blocker", "brawl", "breakthrough", "bunk_t", "checkers", "chomp", "connect4", "crisscross",  "doubletictactoe", "guess", "knightthrough", "merrills", "minichess", "nim1", "nim2", "nim3", "nim4", "numbertictactoe", "othellosuicide", "pawntoqueen", "pawn_whopping", "pentago_2008", "point_grab", "quarto", "quartosuicide", "Qyshinsu", "racer", "racetrackcorridor", "roshambo2","sheep_and_wolf", "skirmish", "sum15", "tictactoe", "tictactoex9", "tictactoeserial", "tictictoe", "wallmaze"
]

unsafe_two_player = ["numbertictactoe", "sum15", ] #doesn't compile
safe_two_player = ["battle", "beatmania", "blocker", "brawl", "breakthrough", "bunk_t", "checkers", "chomp", "connect4", "crisscross",  "doubletictactoe", "guess", "knightthrough", "merrills", "minichess", "nim1", "nim2", "nim3", "nim4", "othellosuicide", "pawntoqueen", "pawn_whopping", "pentago_2008", "point_grab", "quarto", "quartosuicide", "Qyshinsu", "racer", "racetrackcorridor", "roshambo2","sheep_and_wolf", "skirmish", "tictactoe", "tictactoex9", "tictactoeserial", "tictictoe", "wallmaze"
]

test_games = ["busy-beaver-3-2", "busy-beaver-3-2", "busy-beaver-4-2", "busy-beaver-5-2"]

one_player_input = "Games/oneplayer"
two_player_input = "Games/twoplayers"
test_input = "Games/Tests"
