% Needs Clingo4.3

#const max_time = 40.
p_time(0..max_time).

1 { p_does(T, R, M) : p_legal(T, R, M) } 1 :- p_role(R), not terminated(T), p_time(T).

hasmove(R, T) :- p_legal(T, R, _).
hasgoal(R, T) :- p_goal(T, R, _).
bad(T, zeromoves) :- p_role(R), p_time(T), not hasmove(R, T),  not p_terminal(T), not terminated(T-1).
bad(T, zerogoals) :- p_role(R), p_time(T), not hasgoal(R, T),      p_terminal(T), not terminated(T-1).
bad(T, manygoals) :- p_goal(T, R, G1), p_goal(T, R, G2), G1 != G2, p_terminal(T), not terminated(T-1).
:- p_terminal(T), not bad(T, zerogoals), not bad(T, manygoals), not terminated(T-1).

terminated(T2) :- bad(T1, _), p_time(T2), T2 >= T1.

dom_time(T2) :- bad(T1, _), bad(T2, _), T1 < T2.
mintime(T) :- bad(T, _), not dom_time(T).

#minimize { T : mintime(T) }.

#show bad/2.
#show does/3.
