% Needs Clingo4.3

#const max_time = 40.
p_time(0..max_time).

1 { p_does(T, R, M) : p_legal(T, R, M) } 1 :- p_role(R), not terminated(T), p_time(T).

terminated(T2) :- p_terminal(T1), p_time(T2), T2 >= T1.

:- 0 { terminated(T) : p_time(T) } 0.
score(S) :- terminated(T), not terminated(T-1), p_goal(T, R, S).

dom_score(S2) :- score(S1), score(S2), S1 > S2.
maxscore(S1) :- score(S1), not dom_score(S1).

#maximize { S : maxscore(S) }.

#show score/1.
#show p_does/3.
