#!/usr/bin/env python3
import requests
import os

def get_list(server):
    url = "http://games.ggp.org/" + server + "/games/"
    response = requests.get(url)
    games_list = response.text.split(",")
    games_list[0] = games_list[0][1:]
    games_list[-1] = games_list[-1][:-1]
    games_list.sort()
    games_list = [game[1:-1].strip(r'" ') for game in games_list]
#    print(games_list)
    return games_list

def get_file(server, game_name):
    if server == "base":
        url_file = "http://games.ggp.org/" + server + "/games/" + game_name + "/" + game_name + ".kif"
    else:
        url_file = "http://games.ggp.org/" + server + "/games/" + game_name + "/rulesheet.kif"
    response = requests.get(url_file)
    loc_name = server + "/" + game_name + ".gdl" 
    with open(loc_name, "w") as loc_file:
        loc_file.write(response.text)

def get_files(server):
    games_list = get_list(server)
    if games_list and not os.path.exists(server):
        print("Creating directory " + server)
        os.makedirs(server)
    for game in games_list:
        print("Downloading " + game + " from " + server)
        get_file(server, game)

if __name__ == "__main__":
    get_files("base")
#    get_files("dresden")
    get_files("stanford")
