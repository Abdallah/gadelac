README file for GaDeLaC, please also read the LICENSE file.

June 2014

version 0.6.1

General
-------

GaDeLaC is a compilation framework for the Game Description Language (GDL), written in OCaml.
GaDeLaC will preprocess and translate game rules written in GDL to a number of backend languages including GDL itself, ASP, CLIPS, and Prolog.

The version of GDL implemented is 1.0, that is with deterministic and full information games.

Latest Version
--------------

Details of the latest version can be obtained via the project website https://bitbucket.org/Abdallah/gadelac or via email to abdallah.saffidine at gmail.com.
Suggestions and patches welcomed.

Organization
------------

The source of the compiler can be found in the Src directory.
The Games directory contains a list of games written in GDL obtained from the Dresden game server.
The Runtime directory contains helper libraries and wrappers for the generated code.
The Experimentation.ml and test.ml files show a possible usage of generated code.
The file ary.data show some statistics on random Mone Carlo playouts performed on a variety of games.
Theses statistics can be used for comparison purposes and for checking that generated files implement the correct game.


Requirement
-----------

The OCaml system version 3.12.1 or above
ocamlfind 1.4.0
Menhir version 20130912

Installation Notes
------------------

`make` will produce an executable, gadelac, which can be used to compile or preprocess files in the Game Description Languages.

Known Issues
------------

See the files KNOWN_BUGS and TODO.
