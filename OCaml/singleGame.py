#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import filecmp
import itertools
import os
import resource
import subprocess
import sys

import compile_all

one_player = compile_all.one_player
two_player = compile_all.two_player
test_games = compile_all.test_games
one_player_input = compile_all.one_player_input
two_player_input = compile_all.two_player_input
test_input = compile_all.test_input

def sizeof_fmt(num):
    for x in ['bytes','KB','MB','GB','TB']:
        if num < 1024.0:
            return "%3.1f %s" % (num, x)
        num /= 1024.0
#    return "%3.1f %s" % (num / 1024, 'KB')

def compile_game(directory, game, parameters):
    gadelac_noprint = ["--noprint", "parsed", "--noprint", "desugared", "--noprint", "optimized", "--noprint", "stratified", "--noprint", "normalized", "--noprint", "inverted", "--noprint", "grouped", "--noprint", "preinstructed", "--noprint", "instructed"]
    input_file = directory + "/" + game
    gadelac_options = parameters + ["--name", game] + ["-o", "Game"] + gadelac_noprint + [input_file]
    old_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    subprocess.check_output(["../gadelac"] + gadelac_options)
    g_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    ocamlbuild_options = ["-cflags", "-unsafe", "-tag", "keepcode", "SingleGame.native"]
    subprocess.check_output(["ocamlbuild"] + ocamlbuild_options)
    ml_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    old_time = old_info.ru_utime + old_info.ru_stime
    gadelac_time = g_info.ru_utime + g_info.ru_stime - old_time
    ocaml_time = ml_info.ru_utime + ml_info.ru_stime - gadelac_time - old_time
    gdl_size = os.path.getsize(directory + "/" + game + ".gdl")
    ml_size = os.path.getsize("_build/Game.ml")
    s_size = os.path.getsize("_build/Game.s")
    sys.stderr.write("Compilation time: Gadelac {0:.3f}, OCaml: {1:.3f}   ".format(gadelac_time, ocaml_time))
    sys.stderr.write("File size: .gdl: {0}, .ml {1}, .s {2}   {3}\n".format(sizeof_fmt(gdl_size), sizeof_fmt(ml_size), sizeof_fmt(s_size), game))

def run_game(game, runs, time):
    out = subprocess.check_output(["./SingleGame.native", str(runs), str(time)])
    parsed_out = out.decode().strip().split(" ")
    parsed_out[1] = float(parsed_out[1])
    parsed_out[2] = float(parsed_out[2])
    parsed_out[3] = float(parsed_out[3])
    sys.stdout.write("{3:s} Average playouts: {0:.3f} {1:.3g} {2:.3g}\n".format(parsed_out[1], parsed_out[2], parsed_out[3], game))
    sys.stderr.flush()
    sys.stdout.flush() 

def check_useless_variables(directory, game, parameters):
    output_directory = ["-od", "."]
    gadelac_noprint = ["--noprint", "parsed", "--noprint", "desugared", "--noprint", "instantiated", "--noprint", "stratified", "--noprint", "normalized", "--print", "optimized", "--noprint", "inverted", "--noprint", "grouped", "--noprint", "preinstructed", "--noprint", "instructed"]
    gadelac_options = ["-id", directory] + output_directory + parameters + ["-in", game] + ["-gn", game] + ["-on", "Game"] + gadelac_noprint
#    subprocess.check_output(["./UselessVariables.native"] + gadelac_options)
    subprocess.check_output(["./UselessArguments.native"] + gadelac_options)

def experiments_tplp():
    params_    = ("",    ["--inverted", "t1", "--grouped", "false", "--optimized", "nothing"]) # 
    params_t   = ("t",   ["--inverted", "t2", "--grouped", "false", "--optimized", "nothing"]) # t
    params_i   = ("i",   ["--inverted", "t1", "--grouped", "false", "--optimized", "prune"])   # i
    params_ti  = ("ti",  ["--inverted", "t2", "--grouped", "false", "--optimized", "prune"])   # ti
    params_g   = ("g",   ["--inverted", "t1", "--grouped", "true", "--optimized", "nothing"]) # g
    params_gti = ("gti", ["--inverted", "t2", "--grouped", "true", "--optimized", "prune"])   # gti
    games_    = ["breakthrough", "tictactoex9", "connect4", "bunk_t", "doubletictactoe", "nim1", "minichess", "blocker", "tictactoe", "roshambo2"]
    games_t   = ["bunk_t", "doubletictactoe"]
    games_i   = ["tictactoex9", "connect4", "minichess", "blocker", "tictactoe"]
    games_ti  = ["tictactoex9", "connect4", "blocker", "tictactoe"]
    games_g   = ["breakthrough", "tictactoex9", "connect4", "bunk_t", "doubletictactoe", "nim1", "minichess", "blocker", "tictactoe", "roshambo2"]
    games_gti = ["tictactoex9", "connect4", "bunk_t", "doubletictactoe", "minichess", "blocker", "tictactoe"]
    experiments = [(params_, games_), (params_t, games_t), (params_i, games_i), (params_ti, games_ti), (params_g, games_g), (params_gti, games_gti)]
    for (params, games) in experiments:
        sys.stdout.write("params: '{:s}'\n".format(params[0]))
        sys.stderr.flush()
        sys.stdout.flush() 
        for game in games:
            compile_game(two_player_input, game, params[1])
            run_game(game, 10, 30)

if __name__ == "__main__":
#    experiments_tplp()
    params_    = ("",    ["--inverted", "t1", "--grouped", "false", "--optimized", "noinstantiatetempo", "--ground_all", "false", "--optimized", "nospecializetempo", "--optimized", "nospecializebound", "--reachability", "false"]) # 
    params_t   = ("t",   ["--inverted", "t2", "--grouped", "false", "--optimized", "noinstantiatetempo", "--ground_all", "false", "--optimized", "nospecializetempo", "--optimized", "nospecializebound", "--reachability", "false"]) # t
    params_i   = ("i",   ["--inverted", "t1", "--grouped", "false", "--optimized", "instantiatetempo", "--ground_all", "false", "--optimized", "specializetempo", "--optimized", "nospecializebound", "--reachability", "true"]) # i
    params_ti  = ("ti",  ["--inverted", "t2", "--grouped", "false", "--optimized", "instantiatetempo", "--ground_all", "false", "--optimized", "specializetempo", "--optimized", "nospecializebound", "--reachability", "true"]) # ti
    params_g   = ("g",   ["--inverted", "t1", "--grouped", "true", "--optimized", "noinstantiatetempo", "--ground_all", "false", "--optimized", "nospecializetempo", "--optimized", "nospecializebound", "--reachability", "false"]) # g
    params_gti = ("gti", ["--inverted", "t2", "--grouped", "true", "--optimized", "instantiatetempo", "--ground_all", "false", "--optimized", "specializetempo", "--optimized", "nospecializebound", "--reachability", "true"]) # gti
    params_all = ("all", ["--inverted", "t2", "--grouped", "true", "--optimized", "instantiatetempo", "--ground_all", "false", "--optimized", "specializetempo", "--optimized", "nospecializebound", "--reachability", "true", "--inlining", "0", "--backend", "ocaml"]) # all
#    all_params = [params_, params_t, params_i, params_ti, params_g, params_gti, ]
    games = ["breakthrough", "tictactoex9", "connect4", "bunk_t", "doubletictactoe", "nim1", "minichess", "blocker", "tictactoe", "roshambo2"]
#    games = ["tictactoex9"]
    games = ["bunk_t", "doubletictactoe", "nim1", "minichess", "blocker", "tictactoe", "roshambo2"]
#    games = ["duplicatestatesmall", "duplicatestatemedium", "duplicatestatelarge", "statespacesmall", "statespacemedium", "statespacelarge" ]
    all_params = [params_all]
#    all_params = [params_]
#    games = ["statespacelarge"]
    games = ["tictactoe"]
    for params in all_params:
        print(params[0])
        for game in games:
#            check_useless_variables(two_player_input, game, params[1])
            compile_game(two_player_input, game, params[1])
#            compile_game(one_player_input, game, params[1])
            run_game(game, 2, 3)
