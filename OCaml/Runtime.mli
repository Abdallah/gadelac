type 'a fact = Fact of 'a * 'a fact array | Const of 'a

val print_fact : ('a -> string) -> 'a fact -> string
val print_array : ('a -> string) -> 'a fact array -> string

module type KB =
sig
  type 'a t
  type ('a, 'b) next

  val exists : 'a t -> 'a fact array -> bool
  val absent : 'a t -> 'a fact array -> bool
  val add : 'a t -> 'a fact array -> unit
  val next : ('a, 'b) next -> 'a -> 'b fact array -> unit
  val get : 'a t -> 'a fact array list
  val get_next : ('a, 'b) next -> ('a * 'b fact array) list
  val clear : 'a t -> unit
  val clear_next : ('a, 'b) next -> unit
  val create : unit -> 'a t
  val create_next : unit -> ('a, 'b) next
  val copy : 'a t -> 'a t
  val copy_next : ('a, 'b) next -> ('a, 'b) next

(*  val print_all : ('a -> string) -> 'a t -> string*)
end

include KB

module type Game =
sig
  type fact
  type t
  
  val name : string
  val init : unit -> t
  val restart : t -> fact array list ref -> bool
  val make_move : t -> fact array list -> fact array list ref -> bool
  val print_fact : fact -> string
  val print : t -> unit
end
