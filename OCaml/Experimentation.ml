open Printf

let foi = float_of_int
let debug = false
let moves = false

type limit = Simus of int | Time of float
type test_setting =
  { move_limit : int;
    print_frequence : int;
    limit : limit }

let print_list pr l = sprintf "[%s]" (String.concat "; " (List.map pr l))
let print_scores (sc, nb) = sprintf "(%s, %.2f)" (print_list string_of_int sc) nb
let print_results r = String.concat " " (List.map print_scores r)
let hashtbl_to_list h = Hashtbl.fold (fun x y l -> (x, y) :: l) h []

type stats =
  { time : float;
    moves : int;
    result : int list }

type aggregate_stats =
  { playouts : float * float * float;
    a_time : float;
    a_moves : float;
    a_results : ((int list) * float) list }
let pre (a, _, _) = a
and deu (_, b, _) = b
and tro (_, _, c) = c

let print_aggr a = sprintf "playouts: %f, time: %.2f, moves: %.2f, scores: %s" (pre a.playouts) a.a_time a.a_moves (print_results a.a_results)

let print_aggr2 a = sprintf "%f %f %f" (pre a.playouts) (deu a.playouts) (tro a.playouts)

let aggregate2 l =
  match l with
  | [] -> assert false
  | head :: tail ->
    let aux (nb, playouts, time, moves, _) s = (nb + 1, playouts +. pre s.playouts, time +. s.a_time, moves +. s.a_moves, []) in
    let start = (1, pre head.playouts, head.a_time, head.a_moves, head.a_results) in
    let (nb, playouts, time, moves, results) = List.fold_left aux start tail in
    let sample_mean = playouts /. foi nb in
    let aux2 acc s = acc +. (sample_mean -. pre s.playouts) ** 2. in
    let variance = List.fold_left aux2 0. l /. foi (nb - 1) in
    let deviation = sqrt variance in
    let ci = 1.96 *. deviation /. sqrt (foi nb) in
    { playouts = sample_mean, deviation, ci;
      a_time = time /. foi nb;
      a_moves = moves /. foi nb;
      a_results = List.map (fun (x, y) -> (x, y /. foi nb)) results }

module Tester (G : Runtime.Game) =
struct

  let rec partition = function
    | [] -> []
    | (a, b) :: r ->
      let with_a, out_a = List.partition (fun (aa, _) -> a = aa ) r in
      (a, b :: List.map snd with_a) :: partition out_a

  let partition_legal arrays =
    let pairs = List.map (function [|a; b|] -> (a, b) | _ -> assert false) arrays in
    partition pairs
  let partition_goals arrays =
    let to_int p = Scanf.sscanf (G.print_fact p) "%d" (fun d -> d) in
    let pairs = List.map (function [|a; b|] -> (a, to_int b) | _ -> assert false) arrays in
    let pairs = partition pairs in
    List.map (function | (a, [g]) -> (a, g) | _ -> assert false) pairs 
  let combine_doeses pairs =
    List.map (fun (a, b) -> [|a; b|]) pairs

  let rand (pl, l) = 
    let n = List.length l in
    let mv = List.nth l (Random.int n) in
    (pl, mv)

  let print_move (pl, mv) = 
    sprintf "pl: %s, mv: %s. " (G.print_fact pl) (G.print_fact mv)

  let print_goal (pl, i) =
    sprintf "%s : %d. " (G.print_fact pl) i

  let loop state set =
    let step = ref 0 in
    let result = ref [] in
    let continue = G.restart state result in
(*    G.print state;*)
    assert continue;
    let doeses = ref (combine_doeses (List.map rand (partition_legal !result))) in
    while G.make_move state !doeses result && !step < set.move_limit do
      incr step;
      doeses := combine_doeses (List.map rand (partition_legal !result))
    done;
    if !step <= set.move_limit then (!step, Some (partition_goals !result))
    else assert false

  let continue set start_time simus =
    match set.limit with
    | Time f -> Sys.time () -. start_time < f
    | Simus s -> simus < s


  let test set =
    let scores = Hashtbl.create 3 in
    let get_score sc = try Hashtbl.find scores sc with Not_found -> 0 in
    let add_score sc = Hashtbl.replace scores sc (get_score sc + 1) in
    let moves = ref 0 in
    let g = G.init () in
    let start_time = Sys.time () in
    let i = ref 0 in
    while continue set start_time !i do
      let (nb, result) = loop g set in
      moves := !moves + nb;
      incr i;
      (match result with
      | None -> (*eprintf "%d moves\n%s\n" nb (G.print g);*) assert false
      | Some scor -> add_score (List.map snd scor));
      if !i mod set.print_frequence = 0 then
        (printf "game %d\n" !i;
         (match result with
           | None -> printf "not finished before %d moves\n" nb
           | Some x -> printf "result after %d moves : %s\n" nb (String.concat " " (List.map print_goal x));
         );
         flush stdout);
    done;
    let scor = hashtbl_to_list scores in
    let scor = List.map (fun (sc, nb) -> sc, foi nb /. foi !i *. 100.) scor in
    let aggr = { playouts = foi !i, 0., 0.;
                 a_time = Sys.time () -. start_time;
                 a_moves = foi !moves /. foi !i;
                 a_results = List.sort compare scor } in
    eprintf "%s\n" (print_aggr aggr);
    flush_all ();
    aggr

  let rec range x y = if x >= y then [] else x :: range (x + 1) y

  let multiple sample set =
    let first = { move_limit = 50_000_000;
                  print_frequence = 200_000;
                  limit = Time 1. } in
    ignore (test first);
    let rang = range 0 sample in
    let samples = List.map (fun _ -> test set) rang in
    let aggr = aggregate2 samples in
    printf "%s %s\n" G.name (print_aggr2 aggr)
    
end
