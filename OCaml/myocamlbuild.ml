open Ocamlbuild_plugin
open Command

let _ = dispatch (function
  | After_rules ->
    flag ["ocaml"; "compile"; "native"; "keepcode"] (A "-S");
  | _ -> ())

