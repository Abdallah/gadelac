open Printf

let kwords x = x * 1024
let mwords x = x * 1024 * 1024
let gc_speed =
  { (Gc.get ()) with
    Gc.minor_heap_size = mwords 4; (* default: 32kw *)
    Gc.major_heap_increment = mwords 32; (* default: 124kw *)
(*    Gc.space_overhead = 120; *)
    Gc.space_overhead = 300; (* default: 80 *)
    Gc.verbose = 0x000; (* default: 0 *) }

let _ = Gc.set gc_speed
let foi = float_of_int
let debug = false
let moves = false

module E = Experimentation

module T = E.Tester (Game)

let quick_test = { E.move_limit = 200;
                   E.print_frequence = 200_000;
                   E.limit = E.Time 3. }

let blind_test = { E.move_limit = 200000000;
                   E.print_frequence = 300_000;
                   E.limit = E.Time 30. }

(*let _ = T.multiple 20 blind_test*)

let get_args () =
  try
    (int_of_string Sys.argv.(1), float_of_string Sys.argv.(2))
  with
    _ -> (20, 30.)

let _ =
  let runs, time = get_args () in
  T.multiple runs { E.move_limit = 20000000;
                    E.print_frequence = 300_000;
                    E.limit = E.Time time }
