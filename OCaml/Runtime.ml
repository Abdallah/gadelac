open Printf

module H = Hashtbl

type 'a fact = Fact of 'a * 'a fact array | Const of 'a
type 'a ofact = OFact of 'a * 'a ofact option array | OConst of 'a

let rec lift f = match f with
  | Fact (x, l) ->
(*    assert (l <> [||]);*)
    OFact (x, Array.map (fun x -> Some (lift x)) l)
  | Const x -> OConst x

let rec print_fact p f =
  match f with 
  | Fact (a, l) ->
(*    assert (l <> [||]);*)
    if l = [||] then p a
    else sprintf "(%s %s)" (p a) (String.concat " " (List.map (print_fact p) (Array.to_list l)))
  | Const a -> p a

let print_array p a = "[" ^ String.concat " " (List.map (print_fact p) (Array.to_list a)) ^ "]"

module type KB =
sig
  type 'a t
  type ('a, 'b) next

  val exists : 'a t -> 'a fact array -> bool
  val absent : 'a t -> 'a fact array -> bool
  val add : 'a t -> 'a fact array -> unit
  val next : ('a, 'b) next -> 'a -> 'b fact array -> unit
  val get : 'a t -> 'a fact array list
  val get_next : ('a, 'b) next -> ('a *'b fact array) list
  val clear : 'a t -> unit
  val clear_next : ('a, 'b) next -> unit
  val create : unit -> 'a t
  val create_next : unit -> ('a, 'b) next
  val copy : 'a t -> 'a t
  val copy_next : ('a, 'b) next -> ('a, 'b) next

(*  val unify : 'a t -> 'a ofact -> 'a fact array list*)

(*  val print_all : ('a -> string) -> 'a t -> string*)
end

(*
module type MYSET =
sig
  type 'a elt
  type 'a t
  val create : unit -> 'a t
  val add : 'a t -> 'a elt -> unit
  val mem : 'a t -> 'a elt -> bool
  val copy : 'a t -> 'a t
  val get_all : 'a t -> 'a elt list
end
  
module MySet : MYSET with type 'a elt = 'a =
struct
  type 'a elt = 'a
  type 'a t = ('a, unit) H.t
  let create () = H.create 10
  let add h x = H.replace h x ()
  let mem h x = H.mem h x
  let copy h = H.copy h

  let get_all h = H.fold (fun key () acc -> key :: acc) h []
end

module StockArray : MYSET with type 'a elt = 'a array =
struct
  type 'a elt = 'a array
  type 'a t = { non_empty : 'a array MySet.t;
                mutable empty : bool }

  let create () = { non_empty = MySet.create ();
                    empty = false }
(*  let find h head =
    try H.find h head with
      | Not_found -> let hh = MySet.create () in H.add h head hh; hh*)

  let add h l = 
    if l = [||] then h.empty <- true
    else MySet.add h.non_empty l

  let mem h l =
    if l = [||] then h.empty
    else MySet.mem h.non_empty l

  let copy h = 
    let non = MySet.copy h.non_empty in
    { non_empty = non;
      empty = h.empty }

  let get_all h =
    if h.empty then [[||]]
    else MySet.get_all h.non_empty
end


module Stock = MySet
(*module Stock = StockArray*)

module Naive =
struct
(*  type 'a t = ('a, 'a fact Stock.t) H.t*)
  type 'a t = ('a, 'a fact array Stock.t) H.t

  let h_find h (f : 'a) =
    try H.find h f with
    | Not_found ->
      let hh = Stock.create () in
      H.add h f hh;
      hh

  let exists (h : 'a t) fact =
    match fact with
    | Fact ((f : 'a), l) ->
      let hh = h_find h f in
      Stock.mem hh l
    | Const f ->
      let hh = h_find h f in
      Stock.mem hh [||]
  let absent h f = not (exists h f)

  let add h fact =
    match fact with 
    | Fact (f, l) ->
      let hh = h_find h f in
      Stock.add hh l
    | Const f ->
      let hh = h_find h f in
      Stock.add hh [||]

  let create () = H.create 10

  let start_with h f = 
    Stock.get_all (h_find h f)

  let clear h = H.clear h
  let clear_start h f = H.replace h f (Stock.create ())

  let get_all h =
    let get_all_one key hh accu = List.map (fun ll -> Fact (key, ll)) (Stock.get_all hh) @ accu in
    H.fold get_all_one h []

  let copy h =
    let cp = H.create 10 in
    let cp_sub key hh = H.add cp key (Stock.copy hh) in
    H.iter cp_sub h;
    cp

  let unify h ufact = match ufact with
    | Fact (None, _) -> assert false
    | Fact (Some x, _) -> start_with h x
    | Const _ -> assert false

  let print_all pr kb =
    let facts = List.sort compare (get_all kb) in
    String.concat ", " (List.map (print_fact pr) facts)
end*)
  

module HashList =
struct
  type 'a t = { hash  : ('a fact array, unit) H.t;
                mutable list : 'a fact array list }

  type ('a, 'b) next = ('a * 'b fact array) list ref
  let next l pred fact = if not (List.mem (pred, fact) !l) then l := (pred, fact) :: !l
  let create_next () = ref []
  let clear_next l = l := []
  let copy_next l = ref !l
  let get_next l = !l

  let exists kb fact = H.mem kb.hash fact

  let add kb fact =
    H.add kb.hash fact ();
    kb.list <- fact :: kb.list

  let absent kb f = not (exists kb f)

  let get kb = kb.list

  let clear kb =
    H.clear kb.hash;
    kb.list <- []

  let create () =
    { hash  = H.create 10;
      list = [] }

  let copy kb =
    { hash = H.copy kb.hash;
      list = kb.list }

(*  let print_array pr array = 
    let s = ref "[|" in
    Array.iter (fun e -> s := !s ^ "; " ^ pr e) array;
    !s ^ "|]"
  let print_all pr kb =
    let facts = List.sort compare (get kb) in
    String.concat ", " (List.map (print_array (print_fact pr)) facts)*)
end

(*
module Double =
struct
  type 'a t = ('a Naive.t * 'a HashList.t)

  let exists (kb, kb') f = 
    let ans  = Naive.exists kb f
    and ans' = HashList.exists kb' f in
    assert (ans = ans');
    ans

  let absent h f = not (exists h f)

  let add (kb, kb') fact = 
    Naive.add kb fact;
    HashList.add kb' fact

  let start_with (kb, kb') f =
    let ans  = List.sort compare (Naive.start_with kb f) in
    let ans' = List.sort compare (HashList.start_with kb' f) in
    assert (ans = ans');
    ans

  let clear (kb, kb') =
    Naive.clear kb;
    HashList.clear kb'

  let clear_start (kb, kb') f =
    Naive.clear_start kb f;
    HashList.clear_start kb' f

  let create () =
    (Naive.create (), HashList.create ())

  let get_all (kb, kb') =
    let ans  = List.sort compare (Naive.get_all kb) in
    let ans' = List.sort compare (HashList.get_all kb') in
    assert (ans = ans');
    ans

  let copy (kb, kb') =
    (Naive.copy kb, HashList.copy kb')

  let unify (kb, kb') ufact =
    let ans  = List.sort compare (Naive.unify kb ufact) in
    let ans' = List.sort compare (HashList.unify kb' ufact) in
    assert (ans = ans');
    ans

  let print_all pr (kb, kb') =
    sprintf "[\n\n%s\n\n%s]" (Naive.print_all pr kb) (HashList.print_all pr kb')

end
*)

(*include Naive*)
include HashList
(*include Double*)

module type Game =
sig
  type fact
  type t
  
  val name : string
  val init : unit -> t
  val restart : t -> fact array list ref -> bool
  val make_move : t -> fact array list -> fact array list ref -> bool
  val print_fact : fact -> string
  val print : t -> unit
end
