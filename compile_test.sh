#!/bin/bash
PATH=.:$PATH
OPTIONS_PATH="-i Games/Tests -o CGames/Tests"
Gadelac -e desugared $OPTIONS_PATH --name desugaring_distinct --noprint parsed
Gadelac -e grouped $OPTIONS_PATH --name grouping_order --noprint parsed --noprint desugared --noprint stratified --noprint normalized --noprint deunified --noprint inverted --noprint compiled
Gadelac -e grouped $OPTIONS_PATH --name nogrouping --noprint parsed --noprint desugared --noprint stratified --noprint normalized --noprint deunified --noprint inverted --noprint compiled --grouped false
Gadelac -e compiled $OPTIONS_PATH --name tictactoe --noprint parsed --noprint desugared --noprint stratified --noprint normalized --noprint deunified --noprint inverted --noprint instructed --print compiled --compiled c