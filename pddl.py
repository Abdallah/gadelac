#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import filecmp
import itertools
import os
import resource
import subprocess
import sys

import compile_all

#one_player = compile_all.one_player
#two_player = compile_all.two_player
problem_onep = ["coins"] #infinite loop?
problem_twop = ["guess"] #infinite loop?
one_player = ["8puzzle", "blocks", "buttons", "duplicatestatesmall", "duplicatestatemedium", "duplicatestatelarge", "ruledepthlinear", "ruledepthexponential", "hanoi", "firefighter", "knightstour", "max_knights", "pancakes6", "queens", "statespacesmall", "statespacemedium", "statespacelarge"]
two_player = ["battle", "beatmania", "bidding-tictactoe", "blocker", "brawl", "breakthrough", "bunk_t", "checkers", "chomp", "connect4", "crisscross",  "doubletictactoe", "knightthrough", "minichess", "nim1", "nim2", "nim3", "nim4", "numbertictactoe", "othellosuicide", "pawntoqueen", "pawn_whopping", "pentago_2008", "point_grab", "quarto", "quartosuicide", "Qyshinsu", "racer", "racetrackcorridor", "roshambo2","sheep_and_wolf", "skirmish", "sum15", "tictactoe", "tictactoex9", "tictactoeserial", "tictictoe", "wallmaze"]
test_games = compile_all.test_games
one_player_input = compile_all.one_player_input
two_player_input = compile_all.two_player_input
test_input = compile_all.test_input

def compile_game(directory, out_dir, game):
    pddl_noprint = ["--noprint", "parsed", "--noprint", "desugared"]
    input_file = directory + "/" + game + ".gdl"
    output_file = out_dir + "/" + game + ".base_full"
    nooptimizations = ["--reachability", "false", "--inlining", "-1", "--compute_rigids", "false", "--merge_rigids", "false"]
    pddl_options = ["--name", game] + ["-o", output_file] + pddl_noprint + nooptimizations + ["--frame", "true", "--backend", "prolog"] + [input_file]
    old_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    subprocess.check_output(["./Gadelac"] + pddl_options)
    g_info = resource.getrusage(resource.RUSAGE_CHILDREN)
    old_time = old_info.ru_utime + old_info.ru_stime
    pddl_time = g_info.ru_utime + g_info.ru_stime - old_time
    sys.stderr.write("Compilation time: ToPDDL {0:.3f}\n".format(pddl_time))

if __name__ == "__main__":
    good = ["blocks"]
    bad = ["pancakes6", "buttons", "knightstour"] # buttons: -> miss simplification opportunities # knightstour, overly complicated, shouldn't use dnf?
    twopl = ["tictactoe", "amazons", "chinesecheckers2", "othello-comp2007"]
    games = ["blocks", "chinesecheckers1"] #, "8puzzle"] + bad
    dresden_games = ["amazons", "breakthrough", "chinesecheckers1", "chinesecheckers1", "chinesecheckers2", "chinesecheckers3", "chinesecheckers4", "chinesecheckers6", "chinesecheckers6-simultaneous", "connect4", "othello-comp2007", "skirmish", "tictactoe"]
    games = [(one_player_input, game) for game in one_player] + [(two_player_input, game) for game in two_player] + [("Games/dresden", game) for game in dresden_games]
#    games = [("Games/dresden", game) for game in dresden_games]
#    games = [(two_player_input, "tictactoe")]
    for (directory, game) in games:
        sys.stderr.write(game + "    ")
        compile_game(directory, "NEW_FRAME", game)
